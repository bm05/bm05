import numpy as np # to import math functions and array functions
import sys         # needed for sleep macro (sys.stdout.flush)
import gevent      #in particular for sleeping

def wslits():
    #prints openings of primary, secondary and JJ-slits
    try:
       print("primary slits pshg x psvg:            %5.2f x %5.2f    (at %5.2f , %5.2f) " % (pshg.position,psvg.position,psho.position,psvo.position))
    except:
       print("primary slits not in current session")

    print("secondary slits sshg x ssvg:          %5.2f x %5.2f    (at %5.2f , %5.2f) " % (sshg.position,ssvg.position,ssho.position,ssvo.position))
    print("")
    print("JJ slits jjhg x jjvg:                 %5.2f x %5.2f    (at %5.2f , %5.2f) " % (jjhg.position,jjvg.position,jjho.position,jjvo.position))

def _changeGap(hgmotor,hgap,vgmotor,vgap):
    if hgmotor.name=="pshg":
       maxgap=2.5
       homotor=psho
       vomotor=psvo
    elif hgmotor.name=="sshg":
       maxgap=3
       homotor=ssho
       vomotor=ssvo
    elif hgmotor.name=="jjhg":
       maxgap=7
       homotor=jjho
       vomotor=jjvo
    else:
       print("I only know primar, secondary and JJ slits.")
       return
    if ((hgap==-9) and (vgap==-9)):
       print("slits %s x %s:          %5.2f x %5.2f    (at %5.2f , %5.2f) " \
              % (hgmotor.name,vgmotor.name,hgmotor.position,vgmotor.position,homotor.position,vomotor.position))
       return
    if vgap==-9:
       vgap=hgap
    if ((hgap<0.01) or (hgap>maxgap) or (vgap<0.01) or (vgap>maxgap)):
       print("Good gap values for %s, %s only between 0.01 and %.1f !" % hgmotor.name,vgmotor.name,maxgap)
       print("Current slits:")
       wslits()
       return
    umv(hgmotor,hgap,vgmotor,vgap)
    print("slits %s x %s:          %5.2f x %5.2f    (at %5.2f , %5.2f) " \
              % (hgmotor.name,vgmotor.name,hgmotor.position,vgmotor.position,homotor.position,vomotor.position))

def psg(gap1=-9,gap2=-9):
   _changeGap(pshg,gap1,psvg,gap2)
def ssg(gap1=-9,gap2=-9):
   _changeGap(sshg,gap1,ssvg,gap2)
def jsg(gap1=-9,gap2=-9):
   _changeGap(jjhg,gap1,jjvg,gap2)




########################################################################
##  small helper functions  ############################################
########################################################################

def sleep(st,endmsg="Done!"):
   pad_str= " " * len('%d' % st)
   for i in range(st,0,-1):
     print('sleeping %d seconds ... %s\r' % (i,pad_str), end="")
     sys.stdout.flush()
     gevent.sleep(1)
   print("%s" % endmsg)

def td(name=""):
   #ESRF telephone directory
   import os
   if name=="":
      os.system("td cook | grep Phil | awk \'{print $1, $2, $3}\'")
      os.system("td capasso | grep -v BL | awk \'{print $1, $2, $3}\'")
      os.system("td thu | grep CALISTE | awk \'{print $1, $2, $3, $4, $5}\'")
      os.system("td kuda | grep -v External | awk \'{print $1, $2, $3}\'")
      os.system("td clemence | grep muzelle |  awk \'{print $1, $2, $3}\'")
      os.system("td barrett | grep -v External | awk \'{print $1, $2, $3}\' ")
      os.system("td celestre | grep Rafael | awk \'{print $1, $2, $3}\' ")
      os.system("td roth | grep Thomas | grep -v NAWROTH | awk \'{print $1, $2, $3}\'")
   else:
      os.system("td %s" % name)

def u(command):
   #to execute Linux commnands like in a shell
   import os
   os.system(command)
  
def gopic():
   goto_peak()

def gocen():
   if fwhm()==0:
      goto_peak()
   else:
      goto_cen()

def gocom():
   goto_com()

def betterCursor():
   u("xset r on")

def sms(textStr,person="Thomas"):
   if len(textStr)>140:
      print("WARNING: text will be cropped to 140 characters:")
      textStr=textStr[0:140]
      print(len(textStr))
      print(textStr)
   key="73b7e58907bfde4ad51209253a7d19ca14d0d926ZEDRvriZ9sVodVqKo7f7SxeDM"
   if person in ["Thomas","TR","tr","troth","thomas"]:
      phone="+33695484681" # Thomas
   elif person in ["Gilles","GB","gb","gilles","berruyer"]:
      phone="+33649775197"
   elif person in ["Maxim","MB","mb","maxim","brendike"]:
      phone="+33760822284"
   elif person in ["Marine","MC","mc","marine","cotte"]:
      phone="+33681233346"
   else:
      if person.startswith("+"):
         try:
            number=int(person.lstrip("+"))
         except:
            print("Unknown person. Not a phone numnber. Given was %s" % person)
            return
         phone=person
      elif person.startswith("0"): 
         try:
            number=int(person.lstrip("0"))
         except:
            print("Unknown person. Not a phone numnber. Given was %s" % person)
            return
         phone="+33%s" % person.lstrip("0")
      else:
         print("Unknown person. Not a phone numnber. Given was %s" % person)
         return
            
   CLIstring="curl -X POST https://textbelt.com/text --data-urlencode phone='%s' --data-urlencode message='%s' -d key=%s" % (phone,textStr,key)
   print(CLIstring)
   u(CLIstring)
   print("")
