from bliss.scanning.chain import ChainPreset
from bliss.scanning.toolbox import ChainBuilder 
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

class BaslerShutterPreset(ChainPreset):
    
    def prepare(self, chain):
        
        basler_node = [node for node in chain.nodes_list if type(node) == LimaAcquisitionMaster][0]
        
        expo_time = basler_node.acq_params.get("acq_expo_time")

        lima_params = {
        "acq_expo_time": expo_time * 1.1
        }
        
        basler_node.acq_params.update(lima_params)
        

