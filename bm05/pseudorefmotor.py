from bliss.controllers.motor import CalcController

class PseudoRefMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def calc_from_real(self, positions_dict):
        yrot = positions_dict["yrot"]
        envy = positions_dict["envy"]
        
        calc_dict = dict()
        calc_dict["offset"] = (yrot + envy) / 2
        calc_dict["gap"] = yrot - envy
        return calc_dict

    def calc_to_real(self, positions_dict):
        real_dict = dict()
        real_dict["yrot"] = positions_dict["offset"] + positions_dict["gap"] / 2
        real_dict["envy"] = positions_dict["offset"] - positions_dict["gap"] / 2
        return real_dict
