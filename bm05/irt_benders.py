from bliss.controllers.motor import CalcController
import numpy
import math

class IRTBenderController(CalcController):
    def calc_from_real(self, positions_dict):
        table_angle = numpy.radians(self.config.get("table_angle", float))
        
        real_z = self._tagged["realz"][0]
        zsign = math.copysign(1, real_z.sign * real_z.steps_per_unit)
        real_z_pos = positions_dict["realz"]
        real_y_pos = positions_dict["realy"]

        return {"y": real_y_pos + zsign * (real_z_pos * numpy.sin(table_angle))/numpy.tan(table_angle) ,
                "z": real_z_pos * numpy.sin(table_angle) }

    def calc_to_real(self, positions_dict):
        table_angle = numpy.radians(self.config.get("table_angle", float))
        real_z = self._tagged["realz"][0]
        zsign = math.copysign(1, real_z.sign * real_z.steps_per_unit)
        z_pos = positions_dict["z"]
        y_pos = positions_dict["y"]

        return {"realz": z_pos / numpy.sin(table_angle),
                "realy": y_pos - zsign * (z_pos/numpy.tan(table_angle)) }
