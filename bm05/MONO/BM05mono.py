# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bm05.MONO.monochromator import MonochromatorBase
    
class BM05Mono(MonochromatorFixExitBase):
    def dxtal2xtal(self, dxtal):
        # gap between crystals planes    
        mono = self.motors['bragg_fe']  # bragg real motor is `mono`
        ztrans = self.motors['xtal']  # ztrans for 2nd crystal
        fixed_exit = 10.02
        # assume degrees for measuring bragg motor
        
        raise NotImplementedError
        
    def xtal2dxtal(self, xtal):
        raise NotImplementedError
