import numpy as np
import time
import datetime
import os
from bliss.common.standard import *
from bliss.common.axis import *
from bliss.common.cleanup import *
from bliss.setup_globals import *
from bliss.config.static import get_config
from bliss.common.axis import AxisOnLimitError

global short_calibration
short_calibration = False

def init_sx():

    sx.limits=(-100,100)

    ######### LIMIT
    print("init_sx --- Moving to hard limit - ")
    start = time.time()
    sx.hw_limit(-1,wait=False)


    time.sleep(1)
    string="init_sx --- Searching limit - ..."
    print(string)
    _status = sx.state
    while _status == "MOVING":
        _status = sx.state
        print(string)
        time.sleep(1)

    print("init_sx --- lim- found !\n")

    # hardware synchronization
    sx.sync_hard()

    _myposn = sx.position

    print("init_sx --- limit - value: %f" % (_myposn))

    if short_calibration:
        _myposp = 38.912699 + _myposn
        mid = 38.912699 / 2.0 + _myposn
    else:
        ######### LIMIT +
        print("init_sx --- Moving to hard limit +")

        sx.hw_limit(1,wait=False)

        time.sleep(1)
        string="init_sx --- Searching limit + ..."
        print(string)
        _status = sx.state
        while _status == "MOVING":
            _status = sx.state
            print(string)
            time.sleep(1)

        print("init_sx --- lim+ found !\n")

        # hardware synchronization
        sx.sync_hard()

        _myposp = sx.position

        print("init_sx --- Hard limit + value: %f" % (_myposp))

        mid = (_myposp - _myposn) / 2.0 + _myposn

    print("init_sx --- moving to middle position %f\n" % (mid))
    umvd(sx,mid)

    print("init_sx --- reseting sx to 0\n")
    sx.dial = 0
    sx.position = 0

    slp = (_myposp - _myposn) / 2.0 - 0.05

    print("init_sx --- reseting sx limits to hard limits +/ 0.05\n")
    sx.limits=(-slp,slp)

    if time.time()-start >= 60:
        print("init_sx --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sx --- Time end: "+str(time.time()-start)+ ' sec')


# search for SY pos and neg limits and reset dial to 0 in the middle of them.
def init_sy():

    sy.limits=(-100,100)

    ######### LIMIT
    print("init_sy --- Moving to hard limit - ")

    start = time.time()
    sy.hw_limit(-1,wait=False)

    time.sleep(1)
    string="init_sy --- Searching limit - ..."
    print(string)
    _status = sy.state
    while _status == "MOVING":
        _status = sy.state
        print(string)
        time.sleep(1)

    print("init_sy --- lim- found !\n")

    # hardware synchronization
    sy.sync_hard()

    _myposn = sy.position

    print("init_sy --- Hard limit - value: %f" % (_myposn))

    if short_calibration:
        _myposp = _myposn - 38.818497
        mid = -38.818497 / 2.0 + _myposn
    else:
        ######### LIMIT +
        print("init_sy --- Moving to hard limit +")
        sy.hw_limit(1,wait=False)

        time.sleep(1)
        string="init_sy --- Searching limit + ..."
        print(string)
        _status = sy.state
        while _status == "MOVING":
            _status = sy.state
            print(string)
            time.sleep(1)
        print("init_sy --- lim+ found !\n")


        # hardware synchronization
        sy.sync_hard()

        _myposp = sy.position

        print("init_sy --- Hard limit + value: %f" % (_myposp))

        mid = (_myposp - _myposn) / 2.0 + _myposn

    print("init_sy --- moving to middle position %f\n" % (mid))
    umvd(sy,mid)

    print("init_sy --- reseting sy to 0\n")

    sy.dial = 0
    sy.position = 0

    slp = (_myposp - _myposn) / 2.0 - 0.05

    print("init_sy --- reseting sy limits to hard limits +/ 0.05\n")
    sy.limits=(-slp,slp)

    if time.time()-start >= 60:
        print("init_sy --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sy --- Time end: "+str(time.time()-start)+ ' sec')



# search for YROT pos and neg limits and reset dial to 0 in the middle of them.
def init_yrot_hrtomo():

    yrot.limits=(-1000,1000)

    ######### LIMIT
    print("init_yrot --- Moving to hard limit - ")
    start = time.time()
    yrot.hw_limit(-1,wait=False)


    time.sleep(1)
    string="init_yrot --- Searching limit - ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)

    print("init_yrot --- lim- found !\n")

    # hardware synchronization
    yrot.sync_hard()

    _myposn = yrot.position

    print("init_yrot --- Hard limit - value: %f" % (_myposn))

    ######### LIMIT +
    print("init_yrot --- Moving to hard limit +")
    yrot.hw_limit(1,wait=False)


    time.sleep(1)
    string="init_yrot --- Searching limit + ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)


    print("init_yrot --- lim+ found !\n")

    # hardware synchronization
    yrot.sync_hard()

    _myposp = yrot.position

    print("init_yrot --- Hard limit + value: %f" % (_myposp))

    mid = (_myposp -  _myposn) / 2.0 + _myposn

    print("init_yrot --- moving to middle position %f\n" % (mid))
    umvd(yrot,mid)

    print("init_yrot --- reseting yrot to 120\n")

    umvd(yrot, 120)

    yrot.dial = 0
    yrot.position = 0

    slp = (_myposp - _myposn) / 2.0 - 0.05

    print("init_yrot --- reseting yrot limits to hard limits +/ 0.05\n")
    yrot.limits=(-slp,slp)

    if time.time()-start >= 60:
        print("init_yrot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_yrot --- Time end: "+str(time.time()-start)+ ' sec')


def init_yrot():

    yrot.limits=(-1000,1000)

    ######### LIMIT
    print("init_yrot --- Moving to hard limit - ")
    start = time.time()
    yrot.hw_limit(-1,wait=False)


    time.sleep(1)
    string="init_yrot --- Searching limit - ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)

    print("init_yrot --- lim- found !\n")

    # hardware synchronization
    yrot.sync_hard()

    _myposn = yrot.position

    print("init_yrot --- Hard limit - value: %f" % (_myposn))

    ######### LIMIT +
    print("init_yrot --- Moving to hard limit +")
    yrot.hw_limit(1,wait=False)


    time.sleep(1)
    string="init_yrot --- Searching limit + ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)


    print("init_yrot --- lim+ found !\n")

    # hardware synchronization
    yrot.sync_hard()

    _myposp = yrot.position

    print("init_yrot --- Hard limit + value: %f" % (_myposp))

    #zero = 108.5015
    mid = (_myposp -  _myposn) / 2.0 + _myposn

    #print("init_yrot --- moving to zero position %f\n" % (zero))
    print("init_yrot --- moving to middle position %f\n" % (mid))
    #umvd(yrot,zero)
    umvd(yrot,mid)

    slp = (_myposp - _myposn) / 2.0 - 0.05

    print("init_yrot --- reseting yrot limits to hard limits +/ 0.05\n")
    yrot.limits=(-slp,slp)


    print("init_yrot --- moving to beam position, dial value -108\n")

    umvd(yrot, -108) #Value updated by PKC 16-02-2023

    yrot.dial = 0
    yrot.position = 0

    if time.time()-start >= 60:
        print("init_yrot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_yrot --- Time end: "+str(time.time()-start)+ ' sec')


def init_sz():

    sz.limits=(-100000,100000)
    sz.dial = 0
    sz.position = 0

    ######### LIMIT
    print("init_sz --- Moving to hard limit +")
    start = time.time()
    while True:
        try:
            string="init_sz --- Searching limit + ..."
            print(string)
            umvd(sz,9999)
        except AxisOnLimitError:
            pass
        finally:
            if 'LIMPOS' in sz.state:
                break

    print("init_sz --- lim+ found !\n")

    # hardware synchronization
    sz.sync_hard()

    _myposp = sz.dial
    print("init_sz --- Hard limit + value: %f" % (_myposp))

    if short_calibration:
        zero = _myposp - 86
        print("init_sz --- moving to zero position %f\n" % (zero))
        umvd(sz, zero)
        zrange = 589
    else:
        ######### LIMIT +
        print("init_sz --- Moving to hard limit -")
        while True:
            try:
                string="init_sz --- Searching limit - ..."
                print(string)
                umvd(sz,-9999)
            except AxisOnLimitError:
                pass
            finally:
                if 'LIMNEG' in sz.state:
                    break

        print("init_sz --- lim- found !\n")

        # hardware synchronization
        sz.sync_hard()

        _myposn = sz.dial

        print("init_sz --- Hard limit - value: %f" % (_myposn))

        mid = (_myposp -  _myposn) / 2.0 + _myposn
        zrange = abs(_myposp -  _myposn)

        print("init_sz --- moving to 0 position %f\n" % (mid+209))
        umvd(sz, mid+209)

    print("init_sz --- reseting sz to 0\n")

    zero = sz.dial
    sz.dial = 0
    sz.position = 0

    slp = int(zrange - (_myposp - zero))-1

    print(f"init_sz --- reseting sz limits to {-slp},1\n")
    sz.limits=(-slp,1)

    if time.time()-start >= 60:
        print("init_sz --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sz --- Time end: "+str(time.time()-start)+ ' sec')


def init_sz_top():

    sz.limits=(-100000,100000)
    sz.dial = 0
    sz.position = 0

    ######### LIMIT
    print("init_sz --- Moving to hard limit +")
    start = time.time()
    while True:
        try:
            string="init_sz --- Searching limit + ..."
            print(string)
            umvd(sz,9999)
        except AxisOnLimitError:
            pass
        finally:
            if 'LIMPOS' in sz.state:
                break

    print("init_sz --- lim+ found !\n")

    # hardware synchronization
    sz.sync_hard()

    _myposp = sz.dial
    print("init_sz --- Hard limit + value: %f" % (_myposp))

    zero = _myposp - 85
    print("init_sz --- moving to zero position %f\n" % (zero))
    umvd(sz, zero)
    print("init_sz --- reseting sz to 0\n")
    sz.dial = 0
    sz.position = 0
    zrange = 588

    slp = int(zrange - (_myposp - zero))+1

    print("init_sz --- reseting sz limits to -502,1\n")
    sz.limits=(-502,1)

    if time.time()-start >= 60:
        print("init_sz --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sz --- Time end: "+str(time.time()-start)+ ' sec')

def init_sz_bottom():

    sz.limits=(-100000,100000)
    sz.dial = 0
    sz.position = 0

    ######### LIMIT
    print("init_sz --- Moving to hard limit -")
    start = time.time()
    while True:
        try:
            string="init_sz --- Searching limit - ..."
            print(string)
            umvd(sz,-9999)
        except AxisOnLimitError:
            pass
        finally:
            if 'LIMNEG' in sz.state:
                break

    print("init_sz --- lim- found !\n")

    # hardware synchronization
    sz.sync_hard()

    _myposn = sz.dial
    print("init_sz --- Hard limit - value: %f" % (_myposn))

    zero = _myposn + 503
    print("init_sz --- moving to zero position %f\n" % (zero))
    umvd(sz, zero)
    print("init_sz --- reseting sz to 0\n")
    sz.dial = 0
    sz.position = 0

    print("init_sz --- reseting sz limits to -502,1\n")
    sz.limits=(-502,1)

def init_sz_hrtomo():

    sz.limits=(-10000,10000)

    ######### LIMIT
    print("init_sz --- Moving to hard limit -")
    start = time.time()
    while True:
        try:
            string="init_sz --- Searching limit - ..."
            print(string)
            umvd(sz,-9999)
        except AxisOnLimitError:
            pass
        finally:
            if 'LIMNEG' in sz.state:
                break

    print("init_sz --- lim- found !\n")

    # hardware synchronization
    sz.sync_hard()

    _myposn = sz.dial

    print("init_sz --- Hard limit - value: %f" % (_myposn))

    ######### LIMIT +
    print("init_sz --- Moving to hard limit +")
    while True:
        try:
            string="init_sz --- Searching limit + ..."
            print(string)
            umvd(sz,9999)
        except AxisOnLimitError:
            pass
        finally:
            if 'LIMPOS' in sz.state:
                break

    print("init_sz --- lim+ found !\n")

    # hardware synchronization
    sz.sync_hard()

    _myposp = sz.dial

    print("init_sz --- Hard limit + value: %f" % (_myposp))

    mid = (_myposp -  _myposn) / 2.0 + _myposn

    print("init_sz --- moving to middle position %f\n" % (mid))
    umvd(sz,mid)

    while True:
        try:
            print("init_sz --- moving to middle position %f\n" % (mid))
            umvd(sz,mid)
        except AxisOnLimitError:
            pass
        finally:
            if "READY" in sz.state and not "LIMPOS" in sz.state:
                break

    print("init_sz --- reseting sz to 0\n")

    umvr(sz, 230)

    sz.dial = 0
    sz.position = 0

    slp = (_myposp - _myposn) / 2.0 - 0.05

    print("init_sz --- reseting sz limits to hard limits +/ 0.05\n")
    sz.limits=(-slp,sz.position+5)
    #sz.limits=(-slp,slp)

    if time.time()-start >= 60:
        print("init_sz --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sz --- Time end: "+str(time.time()-start)+ ' sec')


# search for SROT pos home switch, go to user position and reset dial to 0
def init_srot_hrtomo():

    srot.dial = 0
    srot.position = 0

    pos = 47
    start=time.time()
    srot.controller.home_search(srot,1,pos)

    srot.sync_hard()
    string="init_srot --- Searching home..."
    print(string)
    srot.controller.home_state(srot)
    while "READY" not in srot.state:
        srot.controller.home_state(srot)
        print(string)
        time.sleep(1)

    print("init_srot --- moving srot to 0\n")
    umv(srot,0)

    if time.time()-start >= 60:
        print("init_srot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_srot --- Time end: "+str(time.time()-start)+ ' sec')

# search for SROT pos home switch, go to user position and reset dial to 0
def init_srot():

    srot.dial = 0
    srot.position = 0

    string="init_srot --- Searching home..."
    print(string)
    start=time.time()
    srot.controller.home_search(srot,1)

    string="init_srot --- Searching home..."
    print(string)
    state = srot.controller.home_state(srot)
    while "READY" not in state:
        state = srot.controller.home_state(srot)
        print(string)
        time.sleep(1)

    srot.sync_hard()
    print("init_srot --- moving srot to 0\n")
    umv(srot,0)

    if time.time()-start >= 60:
        print("init_srot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_srot --- Time end: "+str(time.time()-start)+ ' sec')

def calib_motors(*motors,short_calib=True):

    all_calib = [init_sx,init_sy,init_sz,init_yrot,init_srot]
    all_motors = [sx,sy,sz,yrot,srot]

    global short_calibration
    short_calibration = short_calib
    if len(motors) > 0:
        calibrations = []
        for mot in motors:
            calibrations.append(all_calib[all_motors.index(mot)])
    else:
        calibrations = all_calib
    # context manager used to stop motors when an exception is raised
    with error_cleanup(*all_motors):
        t0 = time.time()
        cal=[gevent.spawn(calib) for calib in calibrations]
        try:
            # wait for all calibrations to be done
            gevent.joinall(cal)
            tend = time.time() - t0
            if tend >= 60:
                print(f"Calibration took {tend/60} min")
            else:
                print(f"Calibration took {tend} sec")
        except:
            gevent.killall(cal)
            print("\n")
            raise
