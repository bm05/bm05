"""
.name          = 'testjm2'
.comment       = 'test2'
.slits         = {'pslits': {'psho': 0.0, 'pshg': 2.0, 'psvo': 0.0, 'psvg': 1.0}, 'sslits': {'ssho': -0.13750000000000018, 'sshg': 1.2000000000000002, 'ssvo': -0.9937500000000001, 'ssvg': 1.1875000000000004}}
.motors        = {'sample_position': {'yrot': 0.0, 'sz': -173.4985}, 'detector_position': {'beny': 0.0, 'benz': 0.0, 'xc': -0.0037462593656414356}}
.attenuators   = {'all': {'att1': 0.0, 'att2': 0.0, 'att3': 0.0, 'att4': 0.0, 'att5': 0.0}}
.tomo          = {'reference': {'motor': 'yrot_eh2', 'in': 0.0, 'out': 0}, 'basic_pars': {'return_images_aligned_to_flats': False, 'save_flag': True, 'display_flag': True, 'return_to_start_pos': True, 'projection_groups': False, 'flat_on': 500, 'dark_at_start': True, 'flat_at_start': True, 'dark_at_end': False, 'flat_at_end': False, 'images_on_return': True, 'scan_type': 'CONTINUOUS', 'half_acquisition': True, 'full_frame_position': 0.0, 'acquisition_position': 1.44, 'shift_in_fov_factor': -0.4393320968741394, 'shift_in_mm': -0.3267, 'shift_in_pixels': -899.7521343982374, 'shift_type': 'Millimeters', 'return_to_full_frame_position': False, 'move_to_acquisition_position': False, 'beam_check': True, 'wait_for_beam': True, 'refill_check': True}, 'config_pars': {'start_pos': 0, 'range': 10, 'flat_n': 41, 'dark_n': 40, 'tomo_n': 10, 'energy': 115.5, 'exposure_time': 0.1, 'comment': 'Mo 1.93+0.8 mm', 'latency_time': 0.004}, 'sequence_pars': {}}
.detector      = {'optics': {}, 'counters': {'enabled': []}}
.creation_date = '2022-12-20 10:37:08'
.last_accessed = '2022-12-20 10:42:16'
"""


from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./library.db'

db = SQLAlchemy(app)
db.init_app(app)

class MultiRes(db.Model):
    # Initialize the Column
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    comment = db.Column(db.String(100), nullable=True)
    creation_date = db.Column(db.Date(), nullable=False)
    last_accessed = db.Column(db.Date(), nullable=False)

    # For displaying our database record rather than just numbers
    def __repr__(self):
        return f'{self.name} : {self.comment}'


class Slits(db.Model):
    # Initialize the Column
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    multires_id = db.Column(db.Integer, db.ForeignKey(MultiRes.id), nullable=False)
    number = db.Column(db.String(100), nullable=False)
    sho = db.Column(db.Float, nullable=False)
    shg = db.Column(db.Float, nullable=False)
    svo = db.Column(db.Float, nullable=False)
    svg = db.Column(db.Float, nullable=False)

    # Initialize the relationship
    multires = db.relationship('MultiRes', backref=db.backref('slits', lazy=True))

    # For displaying our database record rather than just numbers
    def __repr__(self):
        return f"<{self.number} Slits > {self.sho} {self.shg} {self.svo} {self.svg}"


#class Tomo(db.Model):
#    id = db.Column(db.Integer, nullable=False, primary_key=True)
#    multires_id = db.Column(db.Integer, db.ForeignKey(MultiRes.id), nullable=False)

db.create_all()

date1 = datetime.strptime('2022-12-20 10:37:08', '%Y-%m-%d %H:%M:%S')
date2 = datetime.strptime('2022-12-20 10:42:16', '%Y-%m-%d %H:%M:%S')

test1 = MultiRes(name='Test 1', comment= "test", creation_date=date1 , last_accessed=date2 )
test2 = MultiRes(name='Tes 2 ', creation_date=date1 , last_accessed=date2)

slits = Slits(number="Primary", sho=0.0, shg=2.0, svo=0.0, svg=1.0, multires=test1)
slits = Slits(number="Secondary", sho=-0.13750000000000018, shg=1.2000000000000002, svo=-0.9937500000000001, svg=1.1875000000000004, multires=test1)
slits = Slits(number="Primary", sho=0.0, shg=2.0, svo=0.0, svg=1.0, multires=test2)
slits = Slits(number="Secondary", sho=-0.13750000000000018, shg=1.2000000000000002, svo=-0.9937500000000001, svg=1.1875000000000004, multires=test2)

db.session.add(test1)
db.session.add(test2)
db.session.add(slits)


db.session.commit()


Slits.query.all()
