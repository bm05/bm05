#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from marshmallow import fields, validate

from bliss.config.static import get_config
from tomo.tiling import tiling

from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
)

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class TomotilingscanSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)

    # FIXME: Units have to be read from the motors
    # FIXME: Motor names have to be read from the motor
    start_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="Start",
        dump_default=-250,
        unit="mm",
    )
    end_x = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="End",
        dump_default=250,
        unit="mm",
    )
    start_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="Start",
        dump_default=-250,
        unit="mm",
    )
    end_y = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="End",
        dump_default=250,
        unit="mm",
    )
    start_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="Start",
        dump_default=550,
        unit="mm",
    )
    end_z = fields.Float(
        validate=validate.Range(min=-2000, max=2000),
        title="End",
        dump_default=-550,
        unit="mm",
    )
    expo_time = fields.Float(
        validate=validate.Range(min=0.001, max=10),
        title="Exposure time",
        dump_default=1,
        unit="s",
    )
    enqueue = fields.Bool(title="Queue Scan", dump_default=True)

    class Meta:
        uiorder = [
            "sampleid",
            "expo_time",
            "start_x",
            "end_x",
            "start_y",
            "end_y",
            "start_z",
            "end_z",
            "enqueue",
        ]
        uigroups = [
            "sampleid",
            {"X-motor": ["start_x", "end_x"], "ui:minwidth": 12,},
            {"Y-motor": ["start_y", "end_y"], "ui:minwidth": 12,},
            {"Z-motor": ["start_z", "end_z"], "ui:minwidth": 12,},
            {"Options": ["expo_time"], "ui:minwidth": 12},
            "enqueue",
        ]
        uischema = {
            "sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"},
            "enqueue": {"classNames": "hidden-row", "ui:widget": "hidden"},
        }


class TomotilingscanActor(ComponentActor):
    schema = TomotilingscanSchema
    name = "[tomo] tiling scan"
    saving_args = {"dataset": "{sampleid.name}_scan"}

    metatype = "tomo"

    def method(
        self,
        start_x,
        end_x,
        start_y,
        end_y,
        start_z,
        end_z,
        expo_time,
        before_scan_starts,
        update_datacollection,
        **kwargs
    ):
        config = get_config()
        tomo_config = config.get("mrtomo_config")
        tomo_det = tomo_config.detectors.active_detector
        if tomo_det is not None:
            tomo_cam = tomo_det.detector
        else:
            tomo_cam = None

        _scan = tiling(
            start_x,
            end_x,
            start_y,
            end_y,
            start_z,
            end_z,
            tomo_cam,
            expo_time=expo_time,
            tomoconfig=tomo_config,
        )

