#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gevent
from marshmallow import fields, validate
import mmh3

from bliss.config.static import get_config
from bliss.scanning.scan import ScanState
from bliss.common.scans import dscan

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
    ComponentActorKilled,
)
from daiquiri.core.hardware.bliss.session import *


class Mtilt2detuningSchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    energy = fields.Float(required=True, title="Energy", unit='keV')
    detuning = fields.Float(required=True, title="detuning", unit='%')
    
    counter = OneOf(["pico2", "pico3", ], required=True,title="Counter")
    enqueue = fields.Bool(title="Queue Scan", default=False)

    class Meta:
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class Mtilt2DetuningActor(ComponentActor):
    schema = Mtilt2detuningSchema
    name = "mtilt2 detuning"

    def method(self, **kwargs):
        cfg = get_config()

        kwargs["before_scan_starts"](self)
        ### before scan
        
        daiquiri_getE(kwargs['energy'], kwargs['detuning'],
             counter=cfg.get(kwargs['counter']), motor=mtilt2,
             energy_motor=bm05_energy)
