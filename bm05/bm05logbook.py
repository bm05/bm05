#Assorted functions for recording things in the elogbook more easily

def lb(comment, prefix="", preview = False):
    from bliss.common.logtools import elog_print
    import click
    
    if preview:
        print("Comment preview:")
        print(prefix + comment)
        add_comment = click.confirm("Add this to the logbook?", default=True)
        if add_comment:
            elog_print(prefix + comment)
            print("Comment logged.")        
    else:
        print("Comment added:")
        print(prefix + comment)
        elog_print(prefix + comment)
         
def lbi(intention="", preview = False):
    comment = intention
    prefix = "### Intention: "
    lb(comment, prefix, preview)
    
def lbo(observation="", preview = False):
    prefix = "@@@ Observation: "
    comment = observation
    lb(comment, prefix, preview)
        
def lba(attention="", preview = False):
    prefix = "!!! Attention: "
    comment = attention
    lb(comment, prefix, preview)
    
def lbs(sample="", preview = False):
    prefix = "$$$ Sample info: "
    comment = sample
    lb(comment, prefix, preview)
    
def lbc(config_info="", preview = False):
    prefix = "%%% Config information: "
    comment = config_info
    lb(comment, prefix, preview)


