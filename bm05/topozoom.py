#from bliss.common.utils import umv

def camsel(cameraname):
    #config.get("topozoomcamsel")
    if cameraname.lower() == "marana".lower() :
        print("Moving to Marana!")
        umv(topozoomcamsel, -120)
    elif cameraname.lower() == "pcogold".lower() :
        print("Moving to PCO Gold!")
        umv(topozoomcamsel, -2)        
    else :
        print("Unknown camera. Valid selections are \'Marana\' or \'PCOGold\'")
