from bliss.config.settings import SimpleSetting
from bliss.setup_globals import wcd05b
import numpy
import time

CHOP_STARTED = SimpleSetting("chopper", default_value=False)
NPOINTS_PER_VOLT = 7.5

def chopon():
  wcd05b.set("ohrelay", 1)
  CHOP_STARTED.set(True)

def chopoff():
  try:
      chopstop()
  except BaseException:
      raise
  else:
      # only deactivate relay if stop went ok
      wcd05b.set("ohrelay", 0)

def chopstart():
  v0 = wcd05b.get("ohdac")
  vmax = 2
  npoints = int((vmax-v0)*NPOINTS_PER_VOLT)
  for v in numpy.linspace(v0, vmax, npoints):
    wcd05b.set("ohdac", v)
    time.sleep(1)

def chopstop():
  v0 = wcd05b.get("ohdac")
  npoints = int(v0*NPOINTS_PER_VOLT)
  for v in numpy.linspace(v0, 0, npoints):
    wcd05b.set("ohdac", v)
    time.sleep(1)
  CHOP_STARTED.set(False)

def chopstate():
  return f"Chopper is {'MOVING' if CHOP_STARTED.get() else 'STOPPED'}; dac voltage: {wcd05b.get('ohdac')}"

