# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# Single source of truth for the version number and the like


# Original code comes from ID31, id31.git/id31/id31musst.py
# L.Claustre 3/09/2020


from bliss.controllers.musst import musst, MusstIntegratingCounterController
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.chain import AcquisitionMaster
from bliss.common.auto_filter.chain_patching_inplace import AutoFilterLimaAcquisitionMasterPatch
from bliss.scanning.chain import ChainNode
import time

class StepScanAccMusstAcquisitionMaster(MusstAcquisitionMaster):
    def __init__(self, musst_device):
        super().__init__(musst_device, program='acc_step_scan.mprg',  program_start_name='MAIN', program_abort_name='CLEAN')
        self._AcquisitionObject__trigger_type = AcquisitionMaster.SOFTWARE
        self.musst.CLEAR

    def prepare(self):
        
        for slave in self.slaves:
            assert isinstance(slave, LimaAcquisitionMaster) or isinstance(slave, AutoFilterLimaAcquisitionMasterPatch)

        self.wait_slaves_prepare()

        
        max_latency = 0
        npulses = None
        max_acc_expo_time = 0
        timer_factor = self.musst.get_timer_factor()
        for lima_slave in self.slaves:
            if isinstance(lima_slave, AutoFilterLimaAcquisitionMasterPatch): device = lima_slave.device.device
            else: device = lima_slave.device

            if npulses is None:
                npulses = int(device.accumulation.nb_frames)
            else:
                assert npulses == device.accumulation.nb_frames
                
            max_latency = max(max_latency,device.proxy.latency_time)

            max_acc_expo_time = max(max_acc_expo_time,device.accumulation.expo_time)

        
        deadtime = int(max_latency * timer_factor)
        expotime = int(max_acc_expo_time * timer_factor)
        trigdelta = expotime+deadtime

        self.next_vars = {"NPULSES": npulses, "TRIGDELTA": trigdelta, "EXPOTIME": expotime}

        return MusstAcquisitionMaster.prepare(self)
        
    def start(self):
        pass
    
    def trigger(self):
        self.musst.run(self.program_abort_name)
        while self.musst.STATE == self.musst.RUN_STATE:
            gevent.idle()
        self.musst.run(self.program_start_name)
        self._start_epoch = time.time()
        self._running_state = True
        self._event.set()


class SelectiveAcqObjIntegratingCounterController(MusstIntegratingCounterController):
    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params):
        return StepScanAccMusstAcquisitionMaster(self.musst)

class musstacc(musst):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._counter_controllers["icc"] = SelectiveAcqObjIntegratingCounterController(self)
