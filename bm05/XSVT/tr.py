import numpy as np # to import math functions and array functions
import sys         # needed for sleep macro (sys.stdout.flush)
import gevent      #in particular for sleeping

def wslits():
    #prints openings of primary, secondary and JJ-slits
    try:
       print("primary slits pshg x psvg:            %5.2f x %5.2f    (at %5.2f , %5.2f) " % (pshg.position,psvg.position,psho.position,psvo.position))
    except:
       print("primary slits not in current session")

    print("secondary slits sshg x ssvg:          %5.2f x %5.2f    (at %5.2f , %5.2f) " % (sshg.position,ssvg.position,ssho.position,ssvo.position))
    print("")
    try:
       print("JJ slits jjhg x jjvg:                 %5.2f x %5.2f    (at %5.2f , %5.2f) " % (jjhg.position,jjvg.position,jjho.position,jjvo.position))
    except:
        print("no jjhg/jjvg slits in current session")
       
def _changeGap(hgmotor,hgap,vgmotor,vgap):
    if hgmotor.name=="pshg":
       maxgap=2.5
       homotor=psho
       vomotor=psvo
    elif hgmotor.name=="sshg":
       maxgap=3
       homotor=ssho
       vomotor=ssvo
    elif hgmotor.name=="jjhg":
       maxgap=7
       homotor=jjho
       vomotor=jjvo
    else:
       print("I only know primar, secondary and JJ slits.")
       return
    if ((hgap==-9) and (vgap==-9)):
       print("slits %s x %s:          %5.2f x %5.2f    (at %5.2f , %5.2f) " \
              % (hgmotor.name,vgmotor.name,hgmotor.position,vgmotor.position,homotor.position,vomotor.position))
       return
    if vgap==-9:
       vgap=hgap
    if ((hgap<0.01) or (hgap>maxgap) or (vgap<0.01) or (vgap>maxgap)):
       print("Good gap values for %s, %s only between 0.01 and %.1f !" % hgmotor.name,vgmotor.name,maxgap)
       print("Current slits:")
       wslits()
       return
    umv(hgmotor,hgap,vgmotor,vgap)
    print("slits %s x %s:          %5.2f x %5.2f    (at %5.2f , %5.2f) " \
              % (hgmotor.name,vgmotor.name,hgmotor.position,vgmotor.position,homotor.position,vomotor.position))

def psg(gap1=-9,gap2=-9):
   _changeGap(pshg,gap1,psvg,gap2)
def ssg(gap1=-9,gap2=-9):
   _changeGap(sshg,gap1,ssvg,gap2)
def jsg(gap1=-9,gap2=-9):
   _changeGap(jjhg,gap1,jjvg,gap2)




########################################################################
##  small helper functions  ############################################
########################################################################

def sleep(st,endmsg="Done!                  "):
   if st<2:
      time.sleep(st)
      return
   pad_str= " " * len('%d' % st)
   for i in range(int(st+0.5),0,-1):
     print('sleeping %d seconds ... %s\r' % (i,pad_str), end="")
     sys.stdout.flush()
     gevent.sleep(1)
   print("%s" % endmsg)

def td(name=""):
   #ESRF telephone directory
   import os
   if name=="":
      os.system("td cook | grep Phil | awk \'{print $1, $2, $3}\'")
      os.system("td capasso | grep -v BL | awk \'{print $1, $2, $3}\'")
      os.system("td thu | grep CALISTE | awk \'{print $1, $2, $3, $4, $5}\'")
      os.system("td kuda | grep -v External | awk \'{print $1, $2, $3}\'")
      os.system("td clemence | grep muzelle |  awk \'{print $1, $2, $3}\'")
      os.system("td barrett | grep -v External | awk \'{print $1, $2, $3}\' ")
      os.system("td celestre | grep Rafael | awk \'{print $1, $2, $3}\' ")
      os.system("td roth | grep Thomas | grep -v NAWROTH | awk \'{print $1, $2, $3}\'")
   else:
      os.system("td %s" % name)

def u(command):
   #to execute Linux commnands like in a shell
   import os
   os.system(command)

def betterCursor():
   u("xset r on")

def sms(textStr,person="Thomas"):
   if len(textStr)>140:
      print("WARNING: text will be cropped to 140 characters:")
      textStr=textStr[0:140]
      print(len(textStr))
      print(textStr)
   key="73b7e58907bfde4ad51209253a7d19ca14d0d926ZEDRvriZ9sVodVqKo7f7SxeDM"
   if person in ["Thomas","TR","tr","troth","thomas"]:
      phone="+33695484681" # Thomas
   elif person in ["Gilles","GB","gb","gilles","berruyer"]:
      phone="+33649775197"
   elif person in ["Maxim","MB","mb","maxim","brendike"]:
      phone="+33760822284"
   elif person in ["Marine","MC","mc","marine","cotte"]:
      phone="+33681233346"
   elif person in ["Rafael","RC","rc","rafael","celestre"]:
      phone="+33769969174"
   else:
      if person.startswith("+"):
         try:
            number=int(person.lstrip("+"))
         except:
            print("Unknown person. Not a phone numnber. Given was %s" % person)
            return
         phone=person
      elif person.startswith("0"): 
         try:
            number=int(person.lstrip("0"))
         except:
            print("Unknown person. Not a phone numnber. Given was %s" % person)
            return
         phone="+33%s" % person.lstrip("0")
      else:
         print("Unknown person. Not a phone numnber. Given was %s" % person)
         return
            
   CLIstring="curl -X POST https://textbelt.com/text --data-urlencode phone='%s' --data-urlencode message='%s' -d key=%s" % (phone,textStr,key)
   print(CLIstring)
   u(CLIstring)
   print("")


def status(detailed=False):
   from bliss.common.utils import BOLD
   allGood=True
    
   #SR current and user mode
   mysrcur=srcur.value
   if mysrcur<5:
       allGood=False
       print("Not enough beam in Storage Ring: %.3f mA" % mysrcur)
   else:
       print(BOLD("SR current:  %.3f mA" % mysrcur))
   
   #FE
   if fe.is_open:
       if allGood:
          print(BOLD("FE is open."))
       else:
          print("FE is open.")
   elif fe.is_closed:
       allGood=False
       print("FE is closed!!!")
   else:
       allGood=False
       print("FE status unknown!!!")
       
   #primary slits
   if allGood:
      print(BOLD("primary slits:  %.3f (h) x %.3f (v) mm2     offsets   %.3f (h) / %.3f (v)" % (pshg.position,psvg.position,psho.position,psvo.position)))
   else:
      print("primary slits:  %.3f (h) x %.3f (v) mm2     offsets   %.3f (h) / %.3f (v)" % (pshg.position,psvg.position,psho.position,psvo.position))
   
   #mono
   myBragg=mono.position
   monoE.update_position()
   myE=monoE.position
   if np.isnan(myE):
       #no idea which energy we are. Calculate via n*l=2dsin(theta)
       d=3.13321196 #from ID21
       mylambda= 2*d*np.sin(myBragg/180.0*np.pi)
       myE=12.3984/mylambda
   if allGood:
      print(BOLD("Mono Bragg angle is %.3f deg " % myBragg))
      print(BOLD("Mono energy is %.3f keV" % monoE.position))
   else:
      print("Mono Bragg angle is %.3f deg " % myBragg)
      print("Mono energy is %.3f keV" % monoE.position)

   #attenuators
   att1pos=att1.position
   att2pos=att2.position
   att3pos=att3.position
   if att1pos==0 and att2pos==0 and att3pos==0:
       if allGood:
           print(BOLD("attenuators OUT of the beam"))
       else:
           print("attenuators OUT of the beam")
   else:
       for attmot in [att1,att2,att3]:
           if attmot.position==0:
                 if allGood:
                    print(BOLD("attenuator %s OUT of the beam" % attmot.name))
                 else:
                    print("attenuator %s OUT of the beam" % attmot.name)
           else:
                allGood=False
                print("attenuator %s = %.3f" % (attmot.name,attmot.position))

   #eh1 beamshutter
   if eh1.is_open:
        if allGood:
           print(BOLD("EH1 shutter is open."))
        else:
           print("EH1 shutter is open.")
   elif eh1.is_closed:
       allGood=False
       print("EH1 shutter is closed!!!")
   else:
       allGood=False
       print("EH1 shutter status unknown!!!")
       print(eh1.state_string)

   #secondary slits
   if allGood:
      print(BOLD("secondary slits:  %.3f (h) x %.3f (v) mm2     offsets   %.3f (h) / %.3f (v)" % (sshg.position,ssvg.position,ssho.position,ssvo.position)))
   else:
      print("secondary slits:  %.3f (h) x %.3f (v) mm2     offsets   %.3f (h) / %.3f (v)" % (sshg.position,ssvg.position,ssho.position,ssvo.position))

   #yinsert
   if np.abs(yinsert.position-0)<2:
      if allGood:
         print(BOLD("yinsert:         yinsert=%.3f" % yinsert.position))
         print(BOLD("not obstructing the beam"))
      else:
         print("yinsert:         yinsert=%.3f" % yinsert.position)
         print("not obstructing the beam")
   elif np.abs(yinsert.position-70)<2:
      print("yinsert:         yinsert=%.3f" % yinsert.position)
      print("Laser In !!!")
   else:
      print("yinsert:         yinsert=%.3f" % yinsert.position)
      print("UNKNOWN yinsert situation")

   #membrane
   print("Membrane:        y1=%.3f " % y1.position)

   #sample stage
   print("Lens stage      @ x2=%.3f   (from membrane)" % x2.position)
   print("Lens tower:       z2=%.3f     y2=%.3f " % (z2.position,y2.position))

   #detector
   print("Detector        @ x3=%.3f   (from membrane)" % x3.position)
   print("Detector tower:   frelz=%.3f  y3=%.3f " % (frelz.position,y3.position))

   
