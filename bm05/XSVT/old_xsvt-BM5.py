import numpy as np
import os
import time
from bliss.config.settings import ParametersWardrobe    #get equivalent to global variables
from bliss import current_session                       #to enable custom file naming
import pylab
import contextlib                                       #trying to redirect error message to /dev/null. Not working
from bliss.common.shutter import BaseShutter, BaseShutterState
from enum import Enum

###################################################################

global lensPosMot
global lensParasiticMot
global parasiticSlope
global parasiticY0
lensPosMot=y2
lensParasiticMot=z2
parasiticSlope=0
parasiticY0=lensParasiticMot.position
   
###################################################################
###################################################################

def help():
   print("Available commands")
   os.system("cat /data/bm05/inhouse/xog-refropt/macros/xsvt-BM5.py | grep '^def '")
   print("Global variables")
   show()
   print("required global variables")
   required()

###################################################################
###################################################################

def getRootDir(trDir="/data/bm05/inhouse/xog-refropt",nameExt="lenses"):
   now=time.localtime()
   year,month,day=map(int,time.strftime("%Y %m %d").split())
   yesterday=float(time.time())-60*60*24
   yesterdayMonth=time.localtime(yesterday)[1]
   yesterdayDay=time.localtime(yesterday)[2]
   todayDir="%02d%02d%02d%s" % (year-2000,month,day,nameExt)
   yesterdayDir="%02d%02d%02d%s" % (year-2000,yesterdayMonth,yesterdayDay,nameExt)
   rootDir=trDir+"/"+todayDir
   if os.path.isdir(rootDir)==1:
      print("%s directory exists. We use it." % rootDir)
   else:
      alternativeDir=trDir+"/"+yesterdayDir
      if os.path.isdir(alternativeDir)==1:
         print("%s directory (yesterday) exists. We use it." % alternativeDir)
         rootDir=alternativeDir
      else:
         print("Neither %s\n    nor %s exist. Create folder for today." % (rootDir,alternativeDir))
         try:
            os.makedirs(rootDir)
         except:
            rootDir="/data/bm05/inhouse/xog-refropt/"
            print("Problem creating directory %s" % rootDir)
            print("Check permissions.")
            print("###################################")
            print("###################################")
            print("###  USE %s !!!" % rootDir)
            print("###################################")
            print("###################################")
   return rootDir

###################################################################

def setMembraneIn(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      membraneInPos=[(y1.name,y1.position)]   #tuple of motor and motor position
   else:
      membraneInPos=[]
      for motor in args:
         membraneInPos.append((motor.name,motor.position))
   glob.add('membraneInPos',membraneInPos)
   showMembraneIn()             

def showMembraneIn():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.membraneInPos
   except AttributeError:
      print("glob.membraneInPos not known yet.")
      print("Use setMembraneIn() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Membrane In at:")
   for myMotor in glob.membraneInPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def membraneIn():
   glob=ParametersWardrobe('xsvt-globals')
   showMembraneIn()
   print("Moving membrane into the beam.")
   for motor in glob.membraneInPos:
      umv(config.get(motor[0]),motor[1])  

def setMembraneOut(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      membraneOutPos=[(y1.name,y1.position)]   #tuple of motor and motor position
   else:
      membraneOutPos=[]
      for motor in args:
         membraneOutPos.append((motor.name,motor.position))
   glob.add('membraneOutPos',membraneOutPos)
   showMembraneOut()            

def showMembraneOut():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.membraneOutPos
   except AttributeError:
      print("glob.membraneOutPos not known yet.")
      print("Use setMembraneOut() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Membrane Out at:")
   for myMotor in glob.membraneOutPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def membraneOut():
   glob=ParametersWardrobe('xsvt-globals')
   showMembraneOut()
   print("Moving membrane out of the beam.")
   for motor in glob.membraneOutPos:
      umv(config.get(motor[0]),motor[1])  

###################################################################

def setLensIn(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      lensInPos=[(y2.name,y2.position)]   #tuple of motor and motor position
   else:
      lensInPos=[]
      for motor in args:
         lensInPos.append((motor.name,motor.position))
   glob.add('lensInPos',lensInPos)
   showLensIn()

def showLensIn():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.lensInPos
   except AttributeError:
      print("xsvtglob.lensInPos not known yet.")
      print("Use setLensIn() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Lens In at:")
   for myMotor in glob.lensInPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def lensIn():
   glob=ParametersWardrobe('xsvt-globals')
   showLensIn()
   print("Moving lens into the beam.")
   for motor in glob.lensInPos:
      umv(config.get(motor[0]),motor[1])  

def setLensOut(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      lensOutPos=[(y2.name,y2.position)]   #tuple of motor and motor position
   else:
      lensOutPos=[]
      for motor in args:
         lensOutPos.append((motor.name,motor.position))
   glob.add('lensOutPos',lensOutPos)
   showLensOut()            

def showLensOut():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.lensOutPos
   except AttributeError:
      print("glob.lensOutPos not known yet.")
      print("Use setLensOut() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Lens Out at:")
   for myMotor in glob.lensOutPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def lensOut():
   glob=ParametersWardrobe('xsvt-globals')
   showLensOut()
   print("Moving lens out of the beam.")
   for motor in glob.lensOutPos:
      umv(config.get(motor[0]),motor[1])  

###################################################################
###################################################################
###################################################################
###################################################################
###################################################################
###################################################################

def showLensSet():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.lensTagStr
      glob.lensPosStr
      glob.lensSupport
      glob.lensSubfolder
   except AttributeError:
      print("At least one of") 
      print("glob.lensTagStr")
      print("glob.lensPosStr")
      print("glob.lensSupport")
      print("glob.lensSubfolder")
      print("not known yet.")
      print("Use setLensSet() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt

   print("Currently defined lens support plate:")
   print("   subfolder, i.e. global tag : %s" % glob.lensSubfolder)
   print("   support type:                %s" % glob.lensSupport)
   print("   position | lens tag          | material")
   print("   ---------+-------------------+------------")
   for ii in range(max(len(glob.lensPosStr.split()),len(glob.lensTagStr.split()))):
      try:
         print("   %8d | %17s | %s" % (int(glob.lensPosStr.split()[ii]),glob.lensTagStr.split()[ii],glob.materialStr.split()[ii]))
      except:
         if  len(glob.lensPosStr.split())>len(glob.lensTagStr.split()):
            print("   %8d | %17s | %s" % (int(glob.lensPosStr.split()[ii]),"---",glob.materialStr.split()[ii]))
         else:
            print("      --    | %17s | %s" % (glob.lensTagStr.split()[ii],glob.materialStr.split()[ii]))
   if len(glob.lensPosStr.split())!=len(glob.lensTagStr.split()):
      print("ATTENTION: different number of defined positions and defined tags !!!!!")

def generatePosStr(inStr):
   try:
      commaparts=inStr.split(",")
      outArr=[]
      ictr=0
      for idx in range(len(commaparts)):
          colonparts=commaparts[idx].split("-");
          if len(colonparts)==1:
            colonparts=commaparts[idx].split(":");
          if len(colonparts)==1:
            imstart=int(commaparts[idx]);
            imend=imstart;
          else:
            imstart=int(colonparts[0])
            imend=int(colonparts[1])

          for ii in range(imend-imstart+1):
            outArr.append(imstart+ii)
            ictr=ictr+1
      outStr=' '.join(str(p) for p in outArr)
   except:
      print("Incomprehensive position list. Try 0:3,5,7:8 or similar")
      outStr=""
   return outStr

def setLensSet(newDir,materialStr,newTagStr,newPosStr="0 1 2 3 4 5 6 7 8 9",plateType="2D"):
   glob=ParametersWardrobe('xsvt-globals')
   if len(materialStr.split())==1:
      #only 1 material given
      singleElement=materialStr
      materialStr=""
      for ii in range(len(newTagStr.split())):
         materialStr=materialStr+" "+singleElement
         materialStr=materialStr.lstrip(" ")
   else:
      #many materials given
      if len(materialStr.split())!=len(newTagStr.split()):
         print("Not the same number of lens tags as there are in list of materials")
         print("Please redo setLensSet() !")
         return         
   if "-" in newPosStr or ":" in newPosStr or "," in newPosStr:
      newPosStr=generatePosStr(newPosStr)
   if newPosStr=="":
      print("empty newPosStr is not allowed.")
      print("Please redo setLensSet() !")
      return
   if plateType not in ["2D","1D","pinholes","perpendicular"]:
      print("Known plate types only 1D, 2D, and pinholes")
      print("Please redo setLensSet() !")
      return
   if len(newPosStr.split())!=len(newTagStr.split()):
      print("Not the same number of lens tags as there are lens positions")
      print("Please redo setLensSet() !")
      return
   glob.add('lensSupport',plateType)
   glob.add('lensSubfolder',newDir)
   glob.add('lensPosStr',newPosStr)
   glob.add('lensTagStr',newTagStr)
   glob.add('materialStr',materialStr)
   showLensSet()

def setLensMotors(newLensPosMot=y2,newLensParasiticMot=None,p1=-1,h1=0,p2=1,h2=0):
   glob=ParametersWardrobe('xsvt-globals')
   glob.add('lensPosMot',newLensPosMot.name)
   if newLensParasiticMot!=None:
     glob.add('lensParasiticMot',newLensParasiticMot.name)
     glob.add('parasiticSlope', (h2-h1)/(p2-p1) )
     glob.add('parasiticY0',  h1+parasiticSlope*(0-p1) )
   else:
     glob.add('lensParasiticMot',None)
     glob.add('parasiticSlope', 0 )
     glob.add('parasiticY0',  0 )
   showLensMotors()

def showLensMotors():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.lensPosMot
      glob.lensParasiticMot
      glob.parasiticSlope
      glob.parasiticY0
   except AttributeError:
      print("At least one of") 
      print("glob.lensPosMot")
      print("glob.lensParasiticMot")
      print("glob.parasiticSlope")
      print("glob.parasiticY0")
      print("not known yet.")
      print("Use setLensMotors() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt

   print("Translation to change lenses: %s" % glob.lensPosMot)
   if glob.lensParasiticMot!=None:
      print("Translation perpendicular to it: %s" % glob.lensParasiticMot)
      if glob.parasiticSlope==0:
         print("   Moves to fixed height parasiticY0 = %.3f" % glob.parasiticY0)
      else:
         print("   Follows linear curve given by y= %.3f *x + %.3f " % (glob.parasiticSlope,glob.parasiticY0) )
         print("   parasiticSlope is %.4f" % glob.parasiticSlope)
         print("   parasiticY0 is %.4f" % glob.parasiticY0)


def lensPosPrint(lp=False):
   outstr ="motors |   y2       z2      phi2   psi2   th2       x2\n"
   outstr+="-------+------------------------------------------------\n"
   outstr+="user   | %8.3f %8.3f %6.3f %6.3f %7.3f %8.2f \n" % (y2.position,z2.position,phi2.position,psi2.position,th2.position,x2.position)
   outstr+="dial   | %8.3f %8.3f %6.3f %6.3f %7.3f %8.2f \n" % (y2.dial,z2.dial,phi2.dial,psi2.dial,th2.dial,x2.dial)
   print(outstr)
   if lp:
      import os
      cmdStr="echo -e %s | lp -d lj1bm05" 
      os.system(cmdStr)
         
def showLensNin(nr):
    lensNin(nr,move=False)

def lensNin(nr,move=True,offset=0):
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.lensPosMot
      glob.lensParasiticMot
      glob.parasiticSlope
      glob.parasiticY0
   except AttributeError:
      print("At least one of") 
      print("glob.lensPosMot")
      print("glob.lensParasiticMot")
      print("glob.parasiticSlope")
      print("glob.parasiticY0")
      print("not known yet.")
      print("Use setLensMotors() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   try:
      glob.lensTagStr
      glob.lensPosStr
      glob.lensSupport
      glob.lensSubfolder
   except AttributeError:
      print("At least one of") 
      print("glob.lensTagStr")
      print("glob.lensPosStr")
      print("glob.lensSupport")
      print("glob.lensSubfolder")
      print("not known yet.")
      print("Use setLensSet() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt

   if glob.lensSupport=="2D":
      spacing=15
      pos0=-4.5*spacing
   elif glob.lensSupport=="1D":
      spacing=24
      pos0=-4.5*spacing
   elif glob.lensSupport=="pinholes":
      spacing=-1
      pos0=-4.5*24
   elif glob.lensSupport=="perpendicular":
      spacing=-1.2
      pos0=0
   else:
      print("Unknown support plate of type %s" % glob.lensSupport)
      print("Known are only 1D, 2D and pinholes")
      raise KeyboardInterrupt
   if glob.lensSupport=="pinholes":
      positions=np.arange(10)*15-4.5*15+offset
      positions=np.append(positions,np.arange(10)*24-4.5*24+offset)
      positions=np.sort(positions)
   if glob.lensSupport=="perpendicular":
      positions= np.arange(24)*spacing+pos0+offset
   else:
      positions= np.arange(10)*spacing+pos0+offset
   if move:
      umv(config.get(glob.lensPosMot),positions[nr])
   else:
      print("umv(%s,%.4f)" % (glob.lensPosMot,positions[nr]))
   if glob.lensParasiticMot!=None:
      if move:
         umv(config.get(glob.lensParasiticMot),glob.parasiticSlope*positions[nr]+glob.parasiticY0)
      else:
         print("umv(%s,%.4f)" % (glob.lensParasiticMot,glob.parasiticSlope*positions[nr]+glob.parasiticY0))
   if move:
      print("Moved in lens on position %d" % nr)
      arrayElement=getIndex(glob.lensPosStr,nr)
      print("called %s" % glob.lensTagStr.split()[arrayElement])

def getIndex(posStr,nr):
   for ii in range(len(posStr.split())):
      if nr==int(posStr.split()[ii]):
         return ii

def mySave(dirname,scanname):
   myDir(dirname)
   myPointerFile(dirname)
   SCAN_SAVING.images_path_template=scanname
   SCAN_SAVING.images_prefix=scanname+"_"
   
      
###################################################################
###################################################################

def pinhole_align(posstr="-67.5 -52.5 -37.5 -22.5 -7.5 7.5 22.5 37.5 52.5 67.5",exptime=.1,repeats=2,angleMotor=phi2,y2opt=True,resetPos=True):
   #angleMotor=psi2
   #angleMotor=phi2
   glob=ParametersWardrobe('xsvt-globals')
   myDir("pinholeAlign")
   myPointerFile("pinholeAlign")
   SCAN_SAVING.images_path_template="pinholeAlign"
   SCAN_SAVING.images_prefix="pinholeAlign_"
   
   #align phi2 and th2
   plotselect(glob.camera.counters.total_sum)
   positions=posstr.split()
   ctr=0
   if posstr=="all":
      positions=np.arange(10)*15-4.5*15
      positions=np.append(positions,np.arange(10)*24-4.5*24)
      positions=np.sort(positions)
   yi=np.zeros(len(positions))
   psii=np.copy(yi)
   thi=np.copy(yi)
   for pos in positions:
      yi[ctr]=float(pos)
      umv(y2,yi[ctr])
      if y2opt:
         targetPos=y2.position
         dscan(y2,-1.5,1.5,20,exptime)
         print(fwhm())
         if inside(fwhm(),1.2,1.4):
            goto_cen()
            y2.position=targetPos
         else:
            goto_peak()

      if ctr==0: #first position has one more repeat
         ct(exptime)
         dscan(angleMotor,-.2,.2,20,exptime)
         goto_peak()
         ct(exptime)
         dscan(th2,-.2,.2,20,exptime)
         goto_peak()
      for ii in range(repeats):
         ct(exptime)
         dscan(angleMotor,-.2,.2,20,exptime)
         try:
            goto_cen()
         except:
            goto_peak()
         ct(exptime)
         dscan(th2,-.2,.2,20,exptime)
         try:
            goto_cen()
         except:
            goto_peak()
      yi[ctr]=y2.position
      thi[ctr]=th2.position
      psii[ctr]=angleMotor.position
      ctr+=1
   print("Results:")
   print("y2 positions")
   print(yi)
   print("th2 positions")
   print(thi)
   print("%s positions" % angleMotor.name)
   print(psii)
   print("")
   print("Average:")
   print("th2 mean:")
   print(np.mean(thi))
   print("%s mean" % angleMotor.name)
   print(np.mean(psii))

   umv(th2,np.mean(thi))
   print("!!! Moved to average best theta angle %.3f !!!" % np.mean(thi))
   if np.std(thi)<0.05 and resetPos:
      th2.position=0
      print("!!! SET THETA ANGLE BACK TO zero/ZERO/0 !!!")
      
   umv(angleMotor,np.mean(psii))
   print("!!! Moved to average best %s angle %.3f !!!" % (angleMotor.name,np.mean(psii)))
   if np.std(psii)<0.05 and resetPos:
      angleMotor.position=0
      print("!!! SET %s ANGLE BACK TO zero/ZERO/0 !!!" % angleMotor.name)
   beep(10)

   fig,ax=pylab.subplots()
   ax.plot(yi,psii,"--ro")
   ax.set_xlabel("y2-position [mm]")
   ax.set_ylabel(("best %s [deg]" % angleMotor.name),color="red")
   ax2=ax.twinx()
   ax2.plot(yi,thi,"--bo")
   ax2.set_ylabel("best th2 [deg]",color="blue")
   timestamp="%sh%smin" % (time.localtime().tm_hour,time.localtime().tm_min)
   rootfolder=glob.rootDir
   pylab.savefig("%s/pinholeAlign/pinholeAlign_%s.png" % (rootfolder,timestamp))
   pylab.show()   

   endSaving()

###################################################################
###################################################################

def speckle(exptime=3,startRefs=False,endRefs=True):
   glob=ParametersWardrobe('xsvt-globals')
   lensPosMot=glob.lensPosMot
   lensParasiticMot=glob.lensParasiticMot

   oldMot1=config.get(lensPosMot).position
   if lensParasiticMot!=None: 
      oldMot2=config.get(lensParasiticMot).position

   membraneIn()
   if startRefs:
      lensOut()
      refs(exptime,"refs","startRefs_")
      #come back to saved lens position
      umv(config.get(lensPosMot),oldMot1)
      if lensParasiticMot!=None: 
         umv(config.get(lensParasiticMot),oldMot2)

   SCAN_SAVING.images_path_template="speckles"
   SCAN_SAVING.images_prefix="speckles_"
   vectscan()

   if endRefs:
      lensOut()
      refs(exptime,"refs","endRefs_")
      #come back to saved lens position
      umv(config.get(lensPosMot),oldMot1)
      if lensParasiticMot!=None: 
         umv(config.get(lensParasiticMot),oldMot2)


def radios(exptime=3,folder="radios",prefix="radios_",repeats=10):
   print("Taking radios. ")
   membraneOut()
   SCAN_SAVING.images_path_template=folder.rstrip("_")
   SCAN_SAVING.images_prefix=prefix.rstrip("_")+"_"
   print("Saving to %s/%s/%s/%s_number.h5" % (SCAN_SAVING.base_path,SCAN_SAVING.template,folder.rstrip("_"),prefix.rstrip("_")+"_") )
   loopscan(repeats,exptime,sleep_time=0.1)

def flats(exptime=3,flatdir="flats",tag="flats_",repeats=10):
   print("Taking flats. ")
   membraneOut()
   lensOut()
   SCAN_SAVING.images_path_template=flatdir.rstrip("_")
   SCAN_SAVING.images_prefix=tag.rstrip("_")+"_"
   print("Saving to %s/%s/%s/%s_number.h5" % (SCAN_SAVING.base_path,SCAN_SAVING.template,flatdir.rstrip("_"),tag.rstrip("_")+"_") )
   loopscan(repeats,exptime,sleep_time=0.1)

def darks(exptime=3,darkdir="darks",tag="darks_",repeats=10):
   print("Taking darks.")
   eh1.close()
   SCAN_SAVING.images_path_template=darkdir.rstrip("_")
   SCAN_SAVING.images_prefix=tag.rstrip("_")+"_"
   print("Saving to %s/%s/%s/%s_number.h5" % (SCAN_SAVING.base_path,SCAN_SAVING.template,darkdir.rstrip("_"),tag.rstrip("_")+"_") )
   loopscan(repeats,exptime,sleep_time=0.1)
   eh1.open()

###################################################################
def refs(exptime=3,refdir="refs",refstag="refs_",scanType="vectscan"):
   membraneIn()
   lensOut()
   #waitForBeam(neededT=300)
   SCAN_SAVING.images_path_template=refdir.rstrip("_")
   SCAN_SAVING.images_prefix=refstag.rstrip("_")+"_"
   if scanType=="both":
      vectscan(exptime=exptime,scanType="vectscan")
      vectscan(exptime=exptime,scanType="spiral")
   else:
      vectscan(exptime=exptime,scanType=scanType) 

def speckleSeries(exptime=3,subset="all",startRefs=True,endRefs=True,makeRadios=True,postAlign=True,scanType="vectscan"):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if startRefs:
      darks(exptime,"darks","darkStart",10)
      flats(exptime,"flats","flatStart",10)
      print(scanType)
      refs(exptime,"refs","refStart",scanType=scanType)

   for ii in range(len(lensposstr.split())):
      wantedPos=int(lensposstr.split()[ii])
      print("Do we need position %d ?" % wantedPos)
      if subset!="all":
         subsetarr=generatePosStr(subset).split()
         print(subsetarr) 
         if str(wantedPos) not in subsetarr:
            print("%d  is not wanted " % wantedPos)
            continue
      membraneIn()
      lensNin(wantedPos)   
      if postAlign:
            SCAN_SAVING.images_path_template=("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix=("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            dscan(y2,-3,3,20,.1)
            goto_peak()
      SCAN_SAVING.images_path_template=("%s" % lenstagarr[ii])
      SCAN_SAVING.images_prefix=("%s_speckles_" % lenstagarr[ii])
      if scanType=="both":
         vectscan(exptime=exptime,scanType="vectscan")
         vectscan(exptime=exptime,scanType="spiral")
      else:
         vectscan(exptime=exptime,scanType=scanType)

   if endRefs:
      refs(exptime,"refs","refEnd",scanType=scanType)
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   endSaving()
   beep(20)   




def xss1DSeries(Nsteps=101,dx=1,exptime=3,subset="all",startRefs=True,endRefs=True,makeRadios=True,postAlign=True,scanType="vert"):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if startRefs:
      darks(exptime,"darks","darkStart",10)
      flats(exptime,"flats","flatStart",10)
      print(scanType)
      xss1Dref(Nsteps,dx,exptime,"refs","refStart",scanType=scanType)

   for ii in range(len(lensposstr.split())):
      wantedPos=int(lensposstr.split()[ii])
      print("Do we need position %d ?" % wantedPos)
      if subset!="all":
         subsetarr=generatePosStr(subset).split()
         print(subsetarr) 
         if str(wantedPos) not in subsetarr:
            print("%d  is not wanted " % wantedPos)
            continue
      membraneIn()
      lensNin(wantedPos)   
      if postAlign:
            SCAN_SAVING.images_path_template=("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix=("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            dscan(y2,-3,3,20,.1)
            goto_peak()
      SCAN_SAVING.images_path_template=("%s" % lenstagarr[ii])
      SCAN_SAVING.images_prefix=("%s_speckles_" % lenstagarr[ii])
      if scanType=="both":
         xss1D(Nsteps,dx,exptime=exptime,scanType="horiz")
         xss1D(Nsteps,dx,exptime=exptime,scanType="vert")
      else:
         xss1D(Nsteps,dx,exptime=exptime,scanType=scanType)

   if endRefs:
      xss1Dref(Nsteps,dx,exptime,"refs","refEnd",scanType=scanType)
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   endSaving()
   beep(20)   


def xss1Dsingle(mydir,mylenstag,Nsteps=101,dx=1,exptime=3,startRefs=True,endRefs=True,makeRadios=True,scanType="vert"):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera

   SCAN_SAVING.template=mydir
   SCAN_SAVING.data_filename=mydir

   if makeRadios:
      lensIn()
      membraneOut()
      radios(exptime,"radios","radios",10)

   SCAN_SAVING.template=mydir
   SCAN_SAVING.data_filename=mydir

   
   
   if startRefs:
      darks(exptime,"darks","darkStart",10)
      flats(exptime,"flats","flatStart",10)
      print(scanType)
      membraneIn()
      xss1Dref(Nsteps,dx,exptime,"refs","refStart",scanType=scanType)

   if 1: #for ii in range(len(lensposstr.split())):
      #wantedPos=int(lensposstr.split()[ii])
      #print("Do we need position %d ?" % wantedPos)
      #if subset!="all":
      #   subsetarr=generatePosStr(subset).split()
      #   print(subsetarr) 
      #   if str(wantedPos) not in subsetarr:
      #      print("%d  is not wanted " % wantedPos)
      #      continue
      membraneIn()
      lensIn()

      SCAN_SAVING.images_path_template=("%s" % mylenstag)
      SCAN_SAVING.images_prefix=("%s_speckles_" % mylenstag)
      
      if scanType=="both":
         xss1D(Nsteps,dx,exptime=exptime,scanType="horiz")
         xss1D(Nsteps,dx,exptime=exptime,scanType="vert")
      else:
         xss1D(Nsteps,dx,exptime=exptime,scanType=scanType)

   if endRefs:
      xss1Dref(Nsteps,dx,exptime,"refs","refEnd",scanType=scanType)
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   endSaving()
   beep(20)   




def speckleSingle(lensfolder,lenstag,exptime=3,startRefs=True,endRefs=False,makeRadios=True,scanType='vect',removePhaseplate=False):

   SCAN_SAVING.template=lensfolder
   SCAN_SAVING.data_filename=lensfolder

   if startRefs:
      darks(exptime,"darks","darkStart",5)
      flats(exptime,"flats","flatStart",5)
      refs(exptime,"refs","refStart")

   if 1:
      membraneIn()
      lensIn()   

      SCAN_SAVING.images_path_template=("%s" % lenstag)
      SCAN_SAVING.images_prefix=("%s_speckles_" % lenstag)

   if scanType=='vect':
      vectscan(exptime=exptime)
   elif scanType=='spiral':
      spiralscan(exptime=exptime)
   else:
      dmeshscan(exptime=exptime)

   if removePhaseplate:
      umv(zpx,9)
      SCAN_SAVING.images_path_template=("%s" % lenstag)
      SCAN_SAVING.images_prefix=("%s_specklesNoPP_" % lenstag)
      if scanType=='vect':
         vectscan(exptime=exptime)
      elif scanType=='spiral':
         spiralscan(exptime=exptime)
      else:
         dmeshscan(exptime=exptime)

   if endRefs:
      refs(exptime,"refs","refEnd")
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   if makeRadios:
      lensIn()
      radios(exptime,"radios","radios",10)

   endSaving()
   beep(20)



def radioSeries(exptime=3,repeats=10,subset="all",makeDarks=True,makeFlats=True,useLensFolder=False,subdir="radios"):
   glob=ParametersWardrobe('xsvt-globals')
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()

   if makeDarks:
      print("taking darks....")
      darks(exptime,"darks","darks",repeats)
   if makeFlats:
      print("taking flats....")
      flats(exptime,"flats","flats",repeats)

   if glob.lensSupport=="perpendicular":
      offset=y2.position
   else:
      offset=0

   for ii in  range(len(lensposstr.split())):
      wantedPos=int(lensposstr.split()[ii])
      if subset!="all":
         subsetarr=generatePosStr(subset)
         if wantedPos not in lensposstr.split():
            continue
      membraneOut()
      lensNin(wantedPos,offset=offset)
      print("%s" % lenstagarr[ii])
      sleep(1)

      if useLensFolder:
         radios(exptime,folder=("%s" % lenstagarr[ii]),prefix=("%s_radios" % lenstagarr[ii]),repeats=repeats)
      else:
         radios(exptime,folder=subdir,prefix=("%s_radios" % lenstagarr[ii]),repeats=repeats)

   if  glob.lensSupport=="perpendicular":
      umv(y2,offset) #returning to first lens

   endSaving()


def multiDist(distList=[600,800,1000],lensNr=0,subdir="multiDist",exptime=3,postAlign=True):
   glob=ParametersWardrobe('xsvt-globals')
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()
   membraneIn()
   for ii in range(10):
      if int(lensposstr.split()[ii])==lensNr:
         mytag=lenstagarr[ii]
 
   print("Using lens called %s " % mytag)

   SCAN_SAVING.template=subdir
   SCAN_SAVING.data_filename="%s_%ddist" % (mytag,len(distList))
   ctr=0
   for dist in distList:
      umv(x3,dist)
      lensOut()
      refs(exptime,"refs",("refs_%02d" % ctr))
      lensNin(lensNr)
      SCAN_SAVING.images_path_template=("%s" % lenstagarr[lensNr])
      SCAN_SAVING.images_prefix=("%s_speckles_d%04d_" % (lenstagarr[lensNr],dist))
      if postAlign:
            plotselect(camera.counters.total_avg)
            dscan(y2,-4,4,20,.1)
            goto_peak()
      vectscan()
      ctr+=1
   lensOut()
   refs(exptime,"refs",("refs_%02d" % ctr))

   endSaving()

def speckleSeries_multiAngle(angles=[0,1,2,3,4,6,8,10,12],exptime=3,subset="all",startRefs=True,endRefs=True,makeRadios=True,postAlign=True):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   umv(th2,0)
   if startRefs:
      darks(exptime,"darks","darkStart",10)
      flats(exptime,"flats","flatStart",10)
      refs(exptime,"refs","refStart")

   for myth in angles:
      umv(th2,myth)      
      
      for ii in range(len(lensposstr.split())):
         wantedPos=int(lensposstr.split()[ii])
         print("Do we need position %d ?" % wantedPos)
         if subset!="all":
            subsetarr=generatePosStr(subset).split()
            print(subsetarr) 
            if str(wantedPos) not in subsetarr:
               print("%d  is not wanted " % wantedPos)
               continue
         membraneIn()
         lensNin(wantedPos)
         if postAlign:
            SCAN_SAVING.images_path_template=("%s%ddeg_align" % (lenstagarr[ii],int(myth)))
            SCAN_SAVING.images_prefix=("%s%ddeg_align_" % (lenstagarr[ii],int(myth)))
            plotselect(camera.counters.total_avg)
            dscan(y2,-5,5,20,.1)
            goto_com()

         SCAN_SAVING.images_path_template=("%s%ddeg" % (lenstagarr[ii],int(myth)))
         SCAN_SAVING.images_prefix=("%s%ddeg_speckles_" % (lenstagarr[ii],int(myth)))
         vectscan(exptime=exptime)

   if makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   umv(th2,0)
   if endRefs:
      refs(exptime,"refs","refEnd")
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   endSaving()



def speckleSeries_ppAndL(ppyPos=[0,0.01,0,-0.02,0,0.01,0,-0.02,-12],ppzPos=[0,0,0.015,-0.02,0,0,0.015,-0.02,0],lyPos=[0,0,0,0,10,10,10,10,0],ppLtag=["sum0_0","sum10_0","sum0_15","sum-20_-20","PP0_0","PP10_0","PP0_15","PP-20_-20","L"],exptime=3,subset="all",startRefs=True,endRefs=True,makeRadios=False,postAlign=False):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   lensposstr=glob.lensPosStr
   lenstagstr=glob.lensTagStr
   lenstagarr=lenstagstr.split()

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if len(ppyPos)!=len(ppzPos) or len(ppyPos)!=len(lyPos) or len(ppyPos)!=len(ppLtag):
      print("get arrays right!")
      return
   
   if 0 and makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   SCAN_SAVING.template=glob.lensSubfolder
   SCAN_SAVING.data_filename=glob.lensSubfolder

   if startRefs:
      darks(exptime,"darks","darkStart",10)
      flats(exptime,"flats","flatStart",10)
      refs(exptime,"refs","refStart")

   lensIn()   
   for ctr in range(len(ppyPos)):
      umv(ppy,ppyPos[ctr])
      umv(ppz,ppzPos[ctr])
      umv(towz,lyPos[ctr])
      
      
      if 1:
         membraneIn()
         SCAN_SAVING.images_path_template=("CrossCor_%s" % ppLtag[ctr])
         SCAN_SAVING.images_prefix=("CrossCor_%s_speckles_" % ppLtag[ctr])
         vectscan(exptime=exptime)

   if 0 and makeRadios:
      radioSeries(exptime=exptime,subset=subset,makeDarks=False,makeFlats=False,useLensFolder=True)

   lensOut()
      
   if endRefs:
      refs(exptime,"refs","refEnd")
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   endSaving()
 
   
def phasePlateAlign(exptime=3,ppaDir="phasePlateAlign",ppaTag="ppA_1_",mode="dmesh",dmeshwidth=0.051,dmeshpoints=35):
   lensOut()
   membraneIn()
   sleep(1)    
   SCAN_SAVING.template=ppaDir
   SCAN_SAVING.data_filename="%s_" % ppaTag

   #taking the single image (but 10 times)
   SCAN_SAVING.images_path_template=ppaDir.rstrip("_")+"_refS"
   SCAN_SAVING.images_prefix=ppaTag.rstrip("_")+"_refS"
   print("Saving to %s/%s/%s/%s_number.h5" % (SCAN_SAVING.base_path,SCAN_SAVING.template,ppaDir.rstrip("_"),ppaTag.rstrip("_")+"_") )
   loopscan(10,exptime)

   #moving in the phaseplate:
   lensIn()
   if mode=="dmesh":
      dmpts=dmeshpoints
      dmwhalf=dmeshwidth*0.5
      SCAN_SAVING.images_path_template=ppaDir.rstrip("_")+"_dmesh"
      SCAN_SAVING.images_prefix=ppaTag.rstrip("_")+"_dmesh"
      dmesh(zpx,-dmwhalf,dmwhalf,dmpts,zpy,-dmwhalf,dmwhalf,dmpts,exptime,srcur,glob.camera)

   if mode=="squareSpiral":
      dmpts=dmeshpoints
      dmwhalf=dmeshwidth*0.5
      SCAN_SAVING.images_path_template=ppaDir.rstrip("_")+"_dmesh"
      SCAN_SAVING.images_prefix=ppaTag.rstrip("_")+"_dmesh"
      xarr,yarr=spiralMesh(-dmwhalf,dmwhalf,dmpts,-dmwhalf,dmwhalf,dmpts)
      lookupscan([(zpx,xarr),(zpy,yarr)],exptime,srcur,glob.camera)

   #taking the single image (but 10 times)
   lensOut()
   SCAN_SAVING.images_path_template=ppaDir.rstrip("_")+"_refE"
   SCAN_SAVING.images_prefix=ppaTag.rstrip("_")+"_refE"
   print("Saving to %s/%s/%s/%s_number.h5" % (SCAN_SAVING.base_path,SCAN_SAVING.template,ppaDir.rstrip("_"),ppaTag.rstrip("_")+"_") )
   loopscan(10,exptime)

   endSaving()


###################################################################
###################################################################
###################################################################

def xss1D(Nsteps,dx,exptime,scanType):
   glob=ParametersWardrobe('xsvt-globals')
   cenPos=250
   xmin=cenPos-(Nsteps*dx/2.)
   xmax=cenPos+(Nsteps*dx/2.)
   pzv.enable()
   sleep(0.2)
   pzh.enable()
   sleep(0.2)
   pzv.sync_hard()
   while 'READY' not in pzv.state:
      pzv.sync_hard()
   sleep(0.5)
   pzh.sync_hard()
   while 'READY' not in pzh.state:
      pzh.sync_hard()
   sleep(1)
   if scanType=="both" or scanType.startswith("horiz"):
      print("Now we do a horizontal 1D xss scan.")
      umv(pzh,xmin,pzv,250)
      ascan(pzh,xmin,xmax,Nsteps,exptime,srcur,glob.camera)
   if scanType=="both" or scanType.startswith("vert"):
      print("Now we do a vertical 1D xss scan.")
      umv(pzv,xmin,pzh,250)
      ascan(pzv,xmin,xmax,Nsteps,exptime,srcur,glob.camera)
      
      
def xss1Dref(Nsteps,dx,exptime,refdir="refs",refstag="refs_",scanType="both"):
   membraneIn()
   lensOut()
   #waitForBeam(neededT=300)
   if scanType=="both" or scanType=="horiz":
      SCAN_SAVING.images_path_template=refdir.rstrip("_")+"hor"
      SCAN_SAVING.images_prefix=refstag.rstrip("_")+"hor_"
      xss1D(Nsteps,dx,exptime,scanType="horiz")
   if scanType=="both" or scanType=="vert":
      SCAN_SAVING.images_path_template=refdir.rstrip("_")+"ver"
      SCAN_SAVING.images_prefix=refstag.rstrip("_")+"ver_"
      xss1D(Nsteps,dx,exptime,scanType="vert")

def xssSingleUnfinished(lensfolder, lenstag, exptime=3, steps=33, rsteps=13, dr=0.5, scanType='both', makeRadios=True, k=3):
   glob=ParametersWardrobe('xsvt-globals')
   membraneIn()
   lensOut()

   SCAN_SAVING.template = lensfolder
   SCAN_SAVING.data_filename = lensfolder

   darks(exptime, "darks", "darkStart", 5)
   flats(exptime, "flats", "flatStart", 5)

   xmin = 245
   ymin = 245

   if scanType == 'both':
      steps1D = k*steps
      steps2D = steps
   if scanType == '2D':
      steps2D = steps
   if scanType == '1D':
      steps1D = k*steps

   # refs
   membraneIn()
   if scanType == '2D' or scanType == 'both':

       # XSS 2D refs
       refdir = "xss_2D_refs"
       refstag = "xss_2D_refs_"

       SCAN_SAVING.images_path_template = refdir.rstrip("_")
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       ameshscan(xstart=xmin, xend=xmin+(steps2D-1)*dr, xsteps=steps2D, 
                 ystart=ymin, yend=ymin+(steps2D-1)*dr, ysteps=steps2D, exptime=exptime)

       # debug
       # x = np.linspace(100, 100+(xsteps-1)*dx, xsteps)
       # rs = int((xsteps-rxsteps)/2)
       # rx = np.linspace(100+rs*dx, 100+(xsteps-1-rs)*dx, (xsteps-2*rs))
       #
       # y = np.linspace(100, 100+(ysteps-1)*dy, ysteps)
       # rs = int((ysteps-rysteps)/2)
       # ry = np.linspace(100+rs*dx, 100+(ysteps-1-rs)*dx, (ysteps-2*rs))

   #making refernce images
   xmax=xmin+(steps1D-1)*dr/k
   if scanType == '1D' or scanType == 'both':
       refdir = "xss_1D_refs"
       refstag = "xss_1D_refs_hor_"
       SCAN_SAVING.images_path_template = refdir.rstrip("_")
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       ascan(pzh, xmin, xmax, steps1D, exptime, srcur, glob.camera)
       refstag = "xss_1D_refs_ver_"
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       ascan(pzv, xmin, xmax, steps1D, exptime, srcur, glob.camera)

   lensIn()

   if scanType == '2D' or scanType == 'both':
       refdir = "xss_2D"
       refstag = "xss_2D_"
       SCAN_SAVING.images_path_template = refdir.rstrip("_")
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       rs = int((steps2D - rsteps) / 2)
       ameshscan(xstart=xmin+rs*dr, xend=xmin+(steps2D-1-rs)*dr, xsteps=steps2D-2*rs, 
                 ystart=ymin+rs*dr, yend=ymin+(steps2D-1-rs)*dr, ysteps=steps2D-2*rs, exptime=exptime)

   if scanType == '1D' or scanType == 'both':
       refdir = "xss_1D"
       refstag = "xss_1D_hor_"
       SCAN_SAVING.images_path_template = refdir.rstrip("_")
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       rs = int((steps1D - rsteps) / 2)
       ascan(pzh, xmin, xmax, steps1D, exptime, srcur, glob.camera)
       refstag = "xss_1D_ver_"
       SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
       ascan(pzv, xmin, xmax, steps1D, exptime, srcur, glob.camera)

   if makeRadios:
       membraneOut()
       radios(exptime, "radios", "radios", 10)

   endSaving()
   beep(20)

def ameshscan(xstart=245, xend=254, xsteps=9, ystart=245, yend=254, ysteps=9, exptime=3):
    glob=ParametersWardrobe('xsvt-globals')
    sync()
    pzv.enable()
    sleep(0.2)
    pzh.enable()
    sleep(0.2)
    pzv.sync_hard()
    sleep(0.5)
    pzh.sync_hard()
    sleep(1)
    print("Starting the amesh now ...")
    amesh(pzh, xstart, xend, xsteps, pzv, ystart, yend, ysteps, exptime, srcur, glob.camera) #sleep_time=0.1)
    
def xsvtSingle(lensfolder,lenstag,exptime=3,startRefs=True,endRefs=False,makeRadios=True,scanType='vect'):

   SCAN_SAVING.template=lensfolder
   SCAN_SAVING.data_filename=lensfolder

   if startRefs:
      darks(exptime,"darks","darkStart",5)
      flats(exptime,"flats","flatStart",5)
      refs(exptime,"refs_xsvt","refStart")

   if 1:
      membraneIn()
      lensIn()   

      SCAN_SAVING.images_path_template=("%s" % lenstag)
      SCAN_SAVING.images_prefix=("%s_speckles_" % lenstag)

   if scanType=='vect':
      vectscan(exptime=exptime)
   elif scanType=='spiral':
      spiralscan(exptime=exptime)
   else:
      dmeshscan(exptime=exptime)

   if endRefs:
      refs(exptime,"refs_xsvt","refEnd")
      flats(exptime,"flats","flatEnd",10)
      darks(exptime,"darks","darkEnd",10)

   if makeRadios:
      lensIn()
      radios(exptime,"radios","radios",10)

   endSaving()
   beep(20)

    
def ST(lensfolder,lenstag,exptime=1):
    xssSingle(lensfolder, lenstag, exptime=exptime,  steps=30, rsteps=30, dr=0.35, scanType='2D', makeRadios=False, k=2)
    xssSingle(lensfolder, lenstag, exptime=exptime,  steps=35, rsteps=35, dr=0.35, scanType='1D', makeRadios=False, k=2)
    xsvtSingle(lensfolder,lenstag, exptime=exptime, startRefs=True, endRefs=False, makeRadios=True, scanType='vect')

    endSaving()
    beep(10)
    sleep(1)
    beep(10)
    
###################################################################
###################################################################
###################################################################

def spiralMeshPoints(x1=-.4,x2=.4,xpts=20,y1=-.4,y2=.4,ypts=20,initialDirection="W",clockwise=True):
   dx=1.0*(x2-x1)/xpts
   dy=1.0*(y2-y1)/ypts
   direction=initialDirection
   reps=1
   if clockwise:
      sign=1
   else:
      sign=-1
   if initialDirection=="E":
      xsign=1
      ysign=0
      nextysign=sign
      x0=(x2+x1)/2.0-0.5*dx
      y0=(y2+y1)/2.0+0.5*dy*sign
   elif initialDirection=="W":
      xsign=-1
      ysign=0
      nextysign=(-1)*sign
      x0=(x2+x1)/2.0+0.5*dx
      y0=(y2+y1)/2.0-0.5*dy*sign
   elif initialDirection=="N":
      xsign=0
      ysign=1
      nextxsign=sign
      y0=(y2+y1)/2.0-0.5*dy
      x0=(x2+x1)/2.0-0.5*dx*sign
   else: # "S"
      xsign=0
      ysign=-1
      nextxsign=(-1)*sign
      y0=(y2+y1)/2.0+0.5*dy
      x0=(x2+x1)/2.0+0.5*dx*sign
   ctr=0
   if (xpts+1) & 0x1:
      x0=(x2+x1)/2.0
   if (ypts+1) & 0x1:
      y0=(y2+y1)/2.0
   xi=x0
   xarr=np.asarray(xi)
   yi=y0
   yarr=np.asarray(yi)
   while xi>=x1 and xi<=x2 and yi>=y1 and yi<=y2:
      for ii in range(reps):
         xi=xi+dx*xsign
         yi=yi+dy*ysign
         xarr=np.append(xarr,xi)
         yarr=np.append(yarr,yi)
      #change direction
      oldxsign=xsign
      if xsign!=0:
         xsign=0
      else:
         xsign=1*sign*ysign
      if ysign!=0:
         ysign=0
      else:
         ysign=1*sign*oldxsign*(-1)
      ctr+=1
      if (ctr-1) & 0x1:
         reps+=1
   xarr=xarr[:-1]
   yarr=yarr[:-1]

   pylab.plot(xarr,yarr,"--ro")
   pylab.show()
   return xarr,yarr


def spiral(x0=250,y0=250,d=30,rmax=150,initialDirection="W",clockwise=True,showSpiral=False):
   getlength=1
   for nn in range(int(1.0*rmax/d)):  #number of required rings
      r=(nn+1)*d
      u=2*np.pi*r
      nu=int(u/d+0.5)  #number of points on ring
      getlength+=nu
   print("points on spiral: %d " % getlength)
   xarr=np.zeros(getlength)
   yarr=np.zeros(getlength)
   xarr[0]=x0
   yarr[0]=y0
   ctr=1
   initDir={"E":0,"S":np.pi/2,"W":np.pi,"N":3*np.pi/2}
   alpha=initDir[initialDirection]
   if clockwise:
      sign=1
   else:
      sign=-1
   for nn in range(int(1.0*rmax/d)):  #number of required rings
      r=(nn+1)*d
      u=2*np.pi*r
      nu=int(u/d+0.5)  #number of points on ring
      for ii in range(nu):
         xarr[ctr]=x0+r*np.sin(alpha+2*np.pi/nu*ii*sign)
         yarr[ctr]=y0+r*np.cos(alpha+2*np.pi/nu*ii*sign)
         ctr+=1
      alpha=alpha+2*np.pi/nu*(nu-1)*sign
   if showSpiral:
      pylab.plot(xarr,yarr,"--ro")
      pylab.show()
   return xarr,yarr

def spiralscan(exptime=3,silent=True):
   glob=ParametersWardrobe('xsvt-globals')

   pzv.enable()
   sleep(0.2)
   pzh.enable()
   sleep(0.2)

   pzv.sync_hard()
   while 'READY' not in pzv.state:
      pzv.sync_hard()
   sleep(0.5)
   pzh.sync_hard()
   while 'READY' not in pzh.state:
      pzh.sync_hard()
   sleep(1)

   harr,varr=spiral(x0=250,y0=250,d=35,rmax=200)

   print("Moving piezos to the first point ...")
   umv(pzh,harr[0],pzv,varr[0])
   sleep(1)
   print("Starting the lookupscan now ...")
   lookupscan([(pzh,harr),(pzv,varr)],exptime,srcur,glob.camera)


def vectscan(xmin=20,xmax=480,xsteps=10,ymin=20,ymax=480,ysteps=10,exptime=3,silent=True,scanType="vectscan"):
   glob=ParametersWardrobe('xsvt-globals')

   pzv.enable()
   sleep(0.2)
   pzh.enable()
   sleep(0.2)

   pzv.sync_hard()
   while 'READY' not in pzv.state:
      pzv.sync_hard()
   sleep(0.5)
   pzh.sync_hard()
   while 'READY' not in pzh.state:
      pzh.sync_hard()
   sleep(1)

   if scanType=="grating":
      print("Now we do a horizontal scan, a vertical scan and a small - 5x5 -  2d mesh:")
      umv(pzh,19,pzv,19)
      #ascan(pzh,20,39,10,exptime,srcur,glob.camera)
      #ascan(pzv,20,39,10,exptime,srcur,glob.camera)
      ascan(pzh,20,59,49,exptime,srcur,glob.camera)
      ascan(pzv,20,59,49,exptime,srcur,glob.camera)
      umv(pzh,19,pzv,19)
      #amesh(pzh,20,29,4,pzv,20,29,4,exptime,srcur,glob.camera)           
      amesh(pzh,20,39,9,pzv,20,39,9,exptime,srcur,glob.camera)           
      #amesh(pzh,20,59,17,pzv,20,59,17,exptime,srcur,glob.camera)           
   else:   
      if scanType=="vectscan":
         harr,varr=piezoList(xmin,xmax,xsteps,ymin,ymax,ysteps,silent=silent)
      elif scanType=="spiral":
         harr,varr=spiral(x0=250,y0=250,d=35,rmax=200)
      else:
         print("Scantype %s is not known" % scanType)
         return
      print("Moving piezos to the first point ...")
      umv(pzh,harr[0],pzv,varr[0])
      sleep(1)
      print("Starting the lookupscan now ...")
      lookupscan([(pzh,harr),(pzv,varr)],exptime,srcur,glob.camera)
      #lookupscan([(pzh,harr),(pzv,varr)],exptime,srcur,pcogold)
 
def dmeshscan(xmin=245,xmax=254,xsteps=9,ymin=245,ymax=254,ysteps=9,exptime=3,silent=True):
   pzv.enable()
   sleep(0.2)
   pzh.enable()
   sleep(0.2)
   pzv.sync_hard()
   sleep(0.5)
   pzh.sync_hard()
   sleep(1)
   print("Moving piezos to the first point 250,250...")
   umv(pzh,xmin,pzv,xmax)
   sleep(1)
   print("Starting the dmesh now ...")
   dmesh(pzh,xmin,xmax,xsteps,pzv,ymin,ymax,ysteps,exptime,srcur,glob.camera)

def piezoList(xmin=20,xmax=480,xsteps=10,ymin=20,ymax=480,ysteps=10,silent=False):
   if xmin<0 or ymin<0:
      print("Piezos can not go below 0. Better stay above 20")
      raise KeyboardInterrupt
   if xmax>500 or ymax>500:
      print("Piezos can not go above 500. Better stay below 480")
      raise KeyboardInterrupt
   if xsteps<3 or xsteps>50 or ysteps<3 or xsteps>50:
      print("Reasonable number of intervals between 4 and 20!")
      raise KeyboardInterrupt
   xarr=vectArray(xmin,xmax,xsteps)
   yarr=vectArray(ymin,ymax,ysteps)
   harr=np.tile(xarr,ysteps)
   varr=np.sort(np.tile(yarr,xsteps))
   if not silent:
      print(harr)
      print(varr)
   return harr,varr

def vectArray(mymin,mymax,N):
  arr=np.arange(N)
  arr=mymin+(np.exp2(arr)-1)*(mymax-mymin)/(np.exp2(N-1)-1)
  print(arr)
  return arr

def vectArrayOld(mymin,mymax,N):
  arr=[0]
  arr=np.append(arr,np.exp2(np.arange(N-1)))
  arr=mymin+arr*(mymax-mymin)/(np.exp2(N-2))
  print(arr)
  return arr

def causticSetPos1(m1=y3,m2=frelz):
   glob=ParametersWardrobe('xsvt-globals')
   glob.add("causticX1",x3.position)
   glob.add("causticY1",m1.position)
   glob.add("causticZ1",m2.position)
   glob.add("caustic1Date",date())
   
def causticSetPos2(m1=y3,m2=frelz):
   glob=ParametersWardrobe('xsvt-globals')
   glob.add("causticX2",x3.position)
   glob.add("causticY2",m1.position)
   glob.add("causticZ2",m2.position)
   glob.add("caustic2Date",date())
   

def causticWaypoints():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      print("Caustic waypoint 1 defined on %s :" %  glob.caustic1Date)
      print("      x3= %7.2f " % glob.causticX1)
      print("      y3= %7.2f " % glob.causticY1)
      print("   frelz= %7.2f " % glob.causticZ1)
      print()
      print("Caustic waypoint 2 defined on %s :" %  glob.caustic2Date)
      print("      x3= %7.2f " % glob.causticX2)
      print("      y3= %7.2f " % glob.causticY2)
      print("   frelz= %7.2f " % glob.causticZ2)
   except:
      print("No waypoints defined. Use causticSetPos1 and  causticSetPos2")
      
def causticY(dist):
   glob=ParametersWardrobe('xsvt-globals')
   cx1=glob.causticX1
   cy1=glob.causticY1
   cx2=glob.causticX2
   cy2=glob.causticY2
   cm=(cy2-cy1)/(cx2-cx1)
   cy0=cy1-cm*cx1
   return cy0+cm*dist

def causticZ(dist):
   glob=ParametersWardrobe('xsvt-globals')
   cx1=glob.causticX1
   cz1=glob.causticZ1
   cx2=glob.causticX2
   cz2=glob.causticZ2
   cm=(cz2-cz1)/(cx2-cx1)
   cz0=cz1-cm*cx1
   return cz0+cm*dist

def image_meanmin_meanmax(arr,percentage):
   s_arr=np.sort(arr,axis=None, kind='quicksort', order=None)
   size_vec=int(percent*s_arr.size)
   mean_min=np.mean(s_arr[0:size_vect])
   mean_max=np.mean(s_arr[-size_vect:-1])   
   return mean_min, mean_max

def caustic(newdir,dist1,dist2,steps,exptime,correctLateral=True,correctExptime=False,comeBack=False,settleT=2):
   startX=x3.position
   startY=y3.position
   startZ=frelz.position
   glob=ParametersWardrobe('xsvt-globals')

   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="causticScan"
   SCAN_SAVING.images_prefix="causticScan_"

   if correctExptime:
      cttime=exptime
      for xx in np.linspace(dist1,dist2,steps+1):
         if correctLateral:
            if glob.causticX1==glob.causticX2:
               print("define two waypoints on x3_1,y3_1,frelz_1  and x3_2,y3_2,frelz_2")
               return
            umv(x3,xx,y3,causticY(xx),frelz,causticZ(xx))
         else:
            umv(x3,xx)
            
         #moved, now count:
         myscan=sct(cttime,glob.camera,glob.camera.counters)
         mydata=myscan.get_data()
         arr=mydata["image"].as_array()
         arrLowMean,arrHighMean=image_meanmin_meanmax(arr,0.01)
         arrMax=nparr.max()
         
         #adjust exptime
         if (arrMax > 10000):
            cttime=cttime*0.5
         elif (arrMax < 2000) and cttime*2<exptime:
            cttime=cttime*2
   
   else:
      if correctLateral:
         if glob.causticX1==glob.causticX2:
            print("define two waypoints on x3_1,y3_1,frelz_1  and x3_2,y3_2,frelz_2")
            return
         a3scan(x3,dist1,dist2,y3,causticY(diset1),causticY(dist2),frelz,causticZ(dist1),causticZ(dist2),steps,exptime,sleep_time=settleT)
      else:
         ascan(x3,dist1,dist2,steps,exptime,sleep_time=settleT)      

   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(x3,startX,y3,startY,z3,startZ)

   endSaving()

def caustics(exptime=3,stack_tag='stack',xi=-30,xf=30,npts=30):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   SCAN_SAVING.template='caustics_scan'
   SCAN_SAVING.data_filename='caustics_scan'
   SCAN_SAVING.images_path_template=stack_tag
   SCAN_SAVING.images_prefix=stack_tag
   dscan(x2, xi, xf, npts, exptime)
   umv(y1,30)
   SCAN_SAVING.images_path_template=stack_tag+'_flats'
   SCAN_SAVING.images_prefix=stack_tag+'_flats'
   dscan(x2, xi, xf, npts, exptime)
   umv(y1, 0)
   endSaving()
   beep(20)     
   
   
def test_piezo(total_time=2, exptime=0.1, stack_tag='piezo_scan', pi=20, pf=480, reset=False):
   glob=ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   SCAN_SAVING.template='test_piezo'
   SCAN_SAVING.data_filename='test_piezo'
   SCAN_SAVING.images_path_template=stack_tag
   SCAN_SAVING.images_prefix=stack_tag
   
   
   if reset:
      pzv.enable()
      sleep(0.2)
      pzh.enable()
      sleep(0.2)

      pzv.sync_hard()
      while 'READY' not in pzv.state:
          pzv.sync_hard()
      sleep(0.5)
      pzh.sync_hard()
      while 'READY' not in pzh.state:
           pzh.sync_hard()
      sleep(0.5)
      
   n_imgs = int(total_time/exptime)
   
   SCAN_SAVING.images_path_template=stack_tag+'loopscan'
   SCAN_SAVING.images_prefix=stack_tag+'loopscan'
   

   umv(pzh, pi, pzv, pi)
   sleep(5)
   loopscan(n_imgs, exptime)
   umv(pzh, pf)
   loopscan(n_imgs, exptime)
   umv(pzv, pf)
   loopscan(n_imgs, exptime)
   umv(pzh, pi, pzv, pi)
   loopscan(n_imgs, exptime)
   
   endSaving()
   beep(20)   
    


def wirescansZ(newdir,Xdist1,Xdist2,Xsteps,wire1,wire2,wiresteps,exptime=0.2,pico=pico1,comeBack=False):
   startX3=x3.position
   startWireZ=wirez.position
   glob=ParametersWardrobe('xsvt-globals')

   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="wireZScan"
   SCAN_SAVING.images_prefix="wireZScan_"


   umv(y3,-227.85,wirey,0.5)
   
   for xx in np.linspace(Xdist1,Xdist2,Xsteps):
      umv(x3,xx)
      sleep(1)
      ascan(wirez,wire1,wire2,wiresteps,exptime,pico)
      
   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(x3,startX3,wirez,startWireZ)

   endSaving()


   
def wirescansY(newdir,Xdist1,Xdist2,Xsteps,wire1,wire2,wiresteps,exptime=0.2,pico=pico1,comeBack=False):
   startX3=x3.position
   startWireY=wirey.position
   glob=ParametersWardrobe('xsvt-globals')

   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="wireYScan"
   SCAN_SAVING.images_prefix="wireYScan_"


   umv(y3,-227.85,wirez,0.5)
   
   for xx in np.linspace(Xdist1,Xdist2,Xsteps):
      umv(x3,xx)
      sleep(1)
      ascan(wirey,wire1,wire2,wiresteps,exptime,pico)
      
   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(x3,startX3,wirey,startWireY)

   endSaving()

###################################################################
###################################################################

def Emovie(E1,E2,Esteps,exptime):
   for EE in np.linspace(E1,E2,Esteps):
       kev.moveE(EE,do_dm=True,counter=pcogold.counters.reflected_avg)
       #create folder
       newdir="PSI_phase_steps/%06.3f" % EE
       fulldir=SCAN_SAVING.base_path+"/"+newdir
       unixStr="mkdir %s" % fulldir
       print(unixStr)
       tr.u(unixStr)
       SCAN_SAVING.template=newdir
       SCAN_SAVING.data_filename="Emovie"
       
       SCAN_SAVING.images_path_template="Efine"
       SCAN_SAVING.images_prefix="Efine_"
       umv(y2,-2.30)
       radios(exptime)

       SCAN_SAVING.images_path_template="Ecoarse"
       SCAN_SAVING.images_prefix="Ecoarse_"
       umv(y2,2.30)
       radios(exptime)

       lensOut()
       flats(exptime)
       
       endSaving()

###################################################################
###################################################################

def piInfo():
   print("PI E-727 mac address:   01:d8:80:39:d8:48:04")
   print("IP address on BM5:      160.103.125.175")
   print("alias:                  e727pitroth")

def recover():
   print("sync()")
   sync()
   sleep(1)
   print("pzv.enable()")
   pzv.enable()
   sleep(1)
   print("pzh.enable()")
   pzh.enable()
   sleep(1)
   print("pzv.sync_hard()")
   pzv.sync_hard()
   sleep(1)
   print("pzh.sync_hard()")
   pzh.sync_hard()
   sleep(1)
   print("ct(1)")
   ct(1)
   print("umv(pzv,20)")
   umv(pzv,20)
   print("umv(pzh,20)")
   umv(pzh,20)

###################################################################

def myDir(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)   
   if Nargs==1:
      SCAN_SAVING.template=args[0]
   if Nargs>1:
      print("No or one arguments only allowed!")
   print("currently saving to folder %s " % SCAN_SAVING.template)
   try:
      print("(current lens set path is  %s )" % glob.lensSubfolder)
   except:
      pass
      
def myPointerFile(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)   
   if Nargs==1:
      SCAN_SAVING.data_filename=args[0]
   if Nargs>1:
      print("No or one arguments only allowed!")
   print("currently h5 pointer file %s.h5 " % SCAN_SAVING.data_filename)

def myScan(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)   
   if Nargs==1:
      SCAN_SAVING.images_path_template=("scans")
      SCAN_SAVING.images_prefix=("scan_")
   if Nargs>1:
      print("No or one arguments only allowed!")
   print("currently saving scans to folder %s " % SCAN_SAVING.images_path_template)
   print("currently saving scans to folder %s " % SCAN_SAVING.images_prefix)
 
def endSaving():
   myDir("align")
   myPointerFile("align")
   SCAN_SAVING.images_path_template=("align_scans")
   SCAN_SAVING.images_prefix=("align_scan_")
   
def saveIn(h5dir,imagesubdir,imageprefix):
   myDir(h5dir)          #name of folder where *.h5 file goes
   myPointerFile(h5dir)  #name of *.h5 file
   #myPointerFile(h5file)
   SCAN_SAVING.images_path_template=imagesubdir
   SCAN_SAVING.images_prefix=imageprefix

def mySaving(subfolder,prefix,ccdn,saveIt=True):
   pass

def wavefront(distance,folder,exptime=3,imageName=""):
   myDir(folder)
   myPointerFile(folder)
   if type(distance)!=list:
      distance=[distance]
   for dist in distance:
      umv(x3,dist)
      SCAN_SAVING.images_path_template="%s%dmm" % (imageName,x3.position)
      SCAN_SAVING.images_prefix="%s%dmm_" % (imageName,x3.position)
      membraneIn()
      vectscan(exptime)
      membraneOut()
      SCAN_SAVING.images_path_template="%s%dmm_flats" % (imageName,x3.position)
      SCAN_SAVING.images_prefix="%s%dmm_flats_" % (imageName,x3.position)
      loopscan(10,exptime)
   

###################################################################

def detDistort(expTime=3,settleTime=3,mode="both",objective="10x",horizMot=y3,vertMot=frelz):
   #mode="detector" varies detector position on a mesh-grid
   #mode="piezo" varies piezo position on a mesh-grid
   #mode="both" does both of the two meshs
   membraneIn()
   lensOut()
   myDir("det_dist_"+objective)
   myPointerFile("det_dist_pointer")
   if mode=="both" or mode=="detector":
      horizPos0=horizMot.position
      vertPos0=vertMot.position
      SCAN_SAVING.images_path_template="det_dist_nfc"
      SCAN_SAVING.images_prefix="det_dist_nfc_"
      if objective=="20x":
         dmesh(horizMot,-0.025,0.025,5,vertMot,-0.025,0.025,5,expTime,sleep_time=settleTime)
      else:
         dmesh(horizMot,-0.05,0.05,5,vertMot,-0.05,0.05,5,expTime,sleep_time=settleTime)
      umv(horizMot,horizPos0,vertMot,vertPos0)
   if mode=="both" or mode=="piezo":
      recover()
      SCAN_SAVING.images_path_template="det_dist_piezo"
      SCAN_SAVING.images_prefix="det_dist_piezo_"
      if objective=="20x":
         amesh(pzh,50,100,5,pzv,50,100,5,expTime,sleep_time=.1)
      else:
         amesh(pzh,50,150,5,pzv,50,150,5,expTime,sleep_time=.1)
   #taking flats
   membraneOut()
   SCAN_SAVING.images_path_template="det_dist_flats"
   SCAN_SAVING.images_prefix="det_dist_flats_"
   loopscan(10,expTime)
   endSaving()
   beep(10)

###################################################################

def bm5Distances():
   print("Still a syntax error to get removed...")
   x1tom=20
   cx1=43000
   myx2=x2.position
   myx2min=x2.low_limit
   myx3=x3.position
   myx3min=x3.low_limit
   myx3max=x3.high_limit
   cx1ToLens=(cx1+x1tom+myx2)
   
   print("Useful distances at BM5:")
   print("                distance-to-source    distance-to-lens     ")
   print("source                     0                %6.2f m " % ((0-(cx1+cx1ToLens))*1e-3)  )
   print("primary slits vert.       26.22 m           %6.2f m " % ((26220-(cx1+cx1ToLens))*1e-3)  )
   print("primary slits horiz.      26.32 m           %6.2f m " % ((26320-(cx1+cx1ToLens))*1e-3)  )
   print("DCM                       29.70 m           %6.2f m " % ((29700-(cx1+cx1ToLens))*1e-3)  )
   print("secondary slits vert.     35.85 m           %6.2f m " % ((35850-(cx1+cx1ToLens))*1e-3)  )
   print("secondary slits horiz.    36.09 m           %6.2f m " % ((36090-(cx1+cx1ToLens))*1e-3)  )
   print("end of UHV beampipe EH1    -.-  m           %6.2f m " % (-99.99)  )
   print("triple axis granite start  -.-  m           %6.2f m " % (-99.99)  )  
   print("center x1 tower           %5.2f m           %6.2f m " % (cx1*1e-3                     , -1e-3*cx1ToLens))
   print("membrane                  %5.2f m           %6.2f m " % ((cx1+x1tom)*1e-3             , myx2*1e-3))
   print("sample @ x2_min           %5.2f m               0 m " % ((cx1+x1tom+myx2min)*1e-3))
   print("sample                    %5.2f m               0 m " % ((cx1+x1tom+myx2)*1e-3))
   print("detector @ x3_min         %5.2f m               ---"  % ((cx1+x1tom+myx3min)*1e-3))
   print("detector                  %5.2f m           %6.2f m " % ((cx1+x1tom+myx3)*1e-3        , (myx3-myx2)*1e-3))
   print("detector @ x3_max         %5.2f m           %6.2f m " % ((cx1+x1tom+myx3max)*1e-3     , (myx3max-myx2min)*1e-3))
   print("triple axis granite end   45.18 m           %6.2f m " % ((45180-(cx1+x1tom+myx3))*1e-3) )

def show():
   missing=0
   glob = ParametersWardrobe('xsvt-globals')
   try:
      print("detector:   ",glob.camera.name)
   except:
      print("WHAT CAMERA DO WE USE?")
      missing+=1
   try:
      print("Membrane out:   ",glob.membraneOutPos)
   except:
      missing+=1
   try:
      print("Membrane in:    ",glob.membraneInPos)
   except:
      missing+=1
   try:
      print("Lens out:       ",glob.lensOutPos)      
   except:
      missing+=1
   try:
      print("Lens in:        ",glob.lensInPos)
   except:
      missing+=1
   try:
      print("Type of lens support:",glob.lensSupport)
   except:
      missing+=1
   try:
      print("Lens tags:    ",glob.lensTagStr)
   except:
      missing+=1
   try:
      print("Lens positions:    ",glob.lensPosStr)
   except:
      missing+=1
   try:
      print("Lens material:    ",glob.materialStr)
   except:
      missing+=1
   try:
      print("Subfolder:         ",glob.lensSubfolder)
   except:
      missing+=1
   try:
      print("Energy: ",glob.energy)
   except:
      missing+=1

def initGlobals():
   glob = ParametersWardrobe('xsvt-globals')
   #This gets from Redis a set of variables called xsvt-globals.
   #There might be many defined, there might be none.
   rootDir=getRootDir("/data/bm05/inhouse/xog-refropt","lenses")
   glob.add('rootDir',rootDir)
   try:
      print("Camera I hope to use is: ",glob.camera.name)
   except:
      glob.add('camera',pcogold)
      #glob.add('camera',edgehs)
      print("Camera I will try to use is: ",glob.camera.name)

def changeDetector(detector):
   glob= ParametersWardrobe('xsvt-globals')
   glob.camera=detector
      
def deleteGlobals(keepDet=True):
   glob = ParametersWardrobe('xsvt-globals')
   if keepDet:
      oldDet=glob.camera
   glob.purge()
   if keepDet:
      glob.add('camera',oldDet)
   
def required():
   missing=0
   glob = ParametersWardrobe('xsvt-globals')
   try:
      glob.membraneOutPos
   except AttributeError:
      missing+=1
      print("glob.membraneOutPos not known yet.")
      print("   Move membrane out of the beam and type 'setMembraneOut()' !")
   try:
      glob.membraneInPos
   except AttributeError:
      missing+=1
      print("glob.membraneInPos not known yet.")
      print("   Move membrane into the beam and type 'setMembraneIn()' !")
   try:
      glob.lensOutPos
   except AttributeError:
      missing+=1
      print("glob.lensOutPos not known yet.")
      print("   Move lens out of the beam and type 'setLensOut()' !")
   try:
      glob.lensInPos
   except AttributeError:
      missing+=1
      print("glob.lensInPos not known yet.")
      print("   Move lens into the beam and type 'setLensIn()' !")

   definition=True
   try:
      glob.lenstagstr
   except AttributeError:
      missing+=1
      definition=False
      print("glob.lenstagstr not known yet.")
   try:
      glob.lensposstr
   except AttributeError:
      missing+=1
      definition=False
      print("glob.lensposstr not known yet.")
   try:
      glob.support
   except AttributeError:
      missing+=1
      definition=False
      print("glob.support plate type not known yet.")
   try:
      glob.subfolder
   except AttributeError:
      missing+=1
      definition=False
      print("glob.subfolder plate type not known yet.")
   if definition==False:
      print("   Define  setLensSet(newDir,newTagStr,newPosStr='0 1 2 3 4 5 6 7 8 9',plateType='2D')' !")

   try:
      glob.membraneLensDist
   except AttributeError:
      missing+=1
      print("glob.membraneLensDist not known yet.")
      print("   type 'setMembraneLensDist(<mm>)' !")
   try:
      glob.lensDetectorDist
   except AttributeError:
      missing+=1
      print("glob.lensDetectorDist not known yet.")
      print("   type 'setLensDetectorDist(<mm>)' !")
   try:
      glob.energy
   except AttributeError:
      missing+=1
      print("glob.energy not known yet.")
      print("   type 'setEnergy(<keV>)' !")
   try:
      glob.material
   except AttributeError:
      missing+=1
      print("glob.material not known yet.")
      print("   type 'setMaterial(Al/C/diamond/Be/SU8/other)' !")
   
   return missing

###################################################################
###################################################################

def laserIn():
   try:
      ibt.laser()
   except:
      umv(yinsert, 70)
      print("Laser is in the beam!")

def laserOut():
   try:
      ibt.free()
   except:
      umv(yinsert, -10)
      print("Neither laser nor diode in the beam!")

def diodeIn():
   try:
      ibt.diode()
   except:
      umv(yinsert, 95)
      print("Diode is in the beam!")

def diodeOut():
   try:
      ibt.free()
   except:
      umv(yinsert, -10)
      print("Neither laser nor diode in the beam!")

def myeh1state():
   return str(eh1.state).split(".")[-1]

def waitForBeam(neededT):
   pass

def myeh1open2(timeout=2,silent=True):
    TangoShutterState = Enum(
       "TangoShutterState",
       dict(
           {
               "MOVING": "Moving",
               "DISABLE": "Hutch not searched",
               "STANDBY": "Wait for permission",
               "RUNNING": "Automatic opening",
           },
           **{item.name: item.value for item in BaseShutterState},
       ),
    )   
    state = eh1.state
    if not silent:
       print(str(state))
    if state.name in  ("OPEN", "RUNNING"):
        log_warning(self, f"{eh1.name} already open, command ignored")                                                            
    elif state == TangoShutterState.DISABLE:
        pass
        #log_warning(self, f"{eh1.name} disabled, command ignored")                                                                
    elif state == TangoShutterState.CLOSED:                                                                                        
        eh1.__control.open()                                                                                                      
        eh1._wait(TangoShutterState.OPEN,timeout)                                                                                
        user_print(f"{eh1.name} was {state.name} and is now {eh1.state.name}")                                                   
    else:
        raise RuntimeError(                                                                                                        
            f"Cannot open {self.name}, current state is: {state.value}"                                                            
        )                                                                                                                          

def myeh1open():
   TangoShutterState = Enum(
       "TangoShutterState",
       dict(
           {
               "MOVING": "Moving",
               "DISABLE": "Hutch not searched",
               "STANDBY": "Wait for permission",
               "RUNNING": "Automatic opening",
           },
           **{item.name: item.value for item in BaseShutterState},
       ),
   )   
   state = eh1.state
   if state.name in ("OPEN", "RUNNING"):
       pass
   elif state == TangoShutterState.DISABLE:
       pass
   elif state == TangoShutterState.CLOSED:
       try:
           eh1.__control.open()
           time.sleep(.2)
           newstate=eh1.state
           #eh1._wait(TangoShutterState.OPEN, timeout)
           user_print(f"eh1 was {state.name} and is now {newstate.name}")
       except RuntimeError as err:
           pass
   else:
       raise RuntimeError(
           f"Cannot open eh1 shutter, current state is: {state.value}"
       )


def beam():
   if fe.is_closed:
      fe.open()
      print("Front End status is ",fe.state)
   elif fe.is_open:
      print("Front End is open")
   else:
      print("CHECK FRONT END STATUS")
   laserOut()
   #DISABLE / CLOSED / OPEN
   shstate=str(eh1.state).split(".")[-1]
   print(shstate)
   if shstate=="OPEN":
      print("Shutter for eh1 is already open")
      return
   if shstate in ["DISABLED","DISABLE"]:
      print("Currently disabled. Waiting for end of search ...")
      while shstate=="DISABLED":
         sleep(.1)
         shstate=str(eh1.state).split(".")[-1]
   print("Trying to open the shutter...")
   ctr=0
   with contextlib.redirect_stdout(open(os.devnull,"w")):
      while shstate!="OPEN":
         eh1.open()#   /  myeh1open()
         if (ctr>2):
            sleep(.25)
            shstate=str(eh1.state).split(".")[-1]
         else:
            shstate=str(eh1.state).split(".")[-1]
         ctr+=1
   print("Shutter open.")

def helpPco():
   print("open a terminal on loka or lbm05loka.")
   print("type there \'rd_pcousb\' to connect remotely to the rackable PC running the device server.")
   print("double click on the \'cond1.9pcousb3\' icon on the Desktop.")
   print("close the remote desktop window with a mouse click")
   print("")
   print("inside the xsvt bliss session, type the following:")
   print("  activate_pcoEdge()")
   print("  umvactivate_hard_triggering()")
   
def wss():
   print("secondary slits gap: %.2f x %.2f at offset %.1f / %.1f" % (sshg.position,ssvg.position,ssho.position,ssvo.position))  
    
def ss(*args):
   if len(args)==0:
      wss()
   elif len(args)==1:
      gapsize=args[0]
      if (gapsize>-.1) and (gapsize<10):
         umv(sshg,gapsize,ssvg,gapsize)
      else:
         print("Resonable gapsize between -.1 and 10")
   elif len(args)==2:
      hgap=args[0]
      vgap=args[1]
      if (hgap>-.1) and (hgap<10) and (vgap>-.1) and (vgap<10):
         umv(sshg,hgap,ssvg,vgap)
      else:
         print("Resonable gapsize between -.1 and 10")
   else:
      print("Usage is:  ss(<gapSize>) or ss(<horizGap>,<vertGap>) ")
      print("           in order to get a square slit or horiz/vert slit")
      print("     e.g.  ss(2) or ss(2,1) in order to get a square slit or 2x1 horiz/vert slit")

def imstat(counter,maxtime=10):
   cttimes=np.asarray([0.04,0.07,0.1,0.2,0.4,0.7,1,2,4,7,10])
   cttimes=cttimes[np.where(cttimes<= maxtime)]
   if cttimes[-1]!=maxtime:
      cttimes.append(maxtime)
   print("Counting with %s the following times:" % counter.name)
   print(cttimes)
   min=np.zeros(len(cttimes))
   mean=np.zeros(len(cttimes))
   max=np.zeros(len(cttimes))
   std=np.zeros(len(cttimes))
   michContrast=np.zeros(len(cttimes))
   histmap=np.zeros((len(cttimes),100))
   ctr=-1   
   for cttime in cttimes:
       ctr+=1
       myct=sct(cttime,counter)
       print(myct)
       print(myct.get_data())
       img=myct.get_data()["%s:image" % counter.name].as_array()

       mean[ctr]=img.mean()
       min[ctr]=img.min()
       max[ctr]=img.max()
       std[ctr]=img.std()
       michContrast[ctr]=(img.max()-img.min())/(img.max()+img.min())
       histo,binedges=np.histogram(img,100)
       bincenters=binedges[:-2]+0.5*(binedges[1]-binedges[0])
       histmap[ctr,:]=histo
   #done with acquisition
   pylab.figure("Michelson contrast")
   pylab.plot(cttimes,michContrast,"b+")        
   pylab.figure("standard deviation")
   pylab.plot(cttimes,std,"r+")
   #pylab.figure("min, mean, max")
   #pylab.plot(cttimes,min,"b+")        
   #pylab.plot(cttimes,mean,"g+")        
   #pylab.plot(cttimes,max,"r+")        
   pylab.figure("min, mean, max / cttime")
   pylab.plot(cttimes,min/cttimes,"b+")        
   pylab.plot(cttimes,mean/cttimes,"g+")        
   pylab.plot(cttimes,max/cttimes,"r+")        
   pylab.figure("Histograms")
   for ii in range(len(cttimes)):
      pylab.plot(np.arange(100),histmap[ii,:],"-")
   pylab.figure("Histogram map")
   pylab.imshow(histmap,interpolation=None) 
   pylab.show()

def dist(*args):
   if len(args)==1:
      target=args[0]
      print("Lens to detector distance was %.1f mm" % (x3.position-x2.position))
      print("Moving detector...")
      umv(x3,x2.position+target)

   elif len(args)==2:
      print("Membrane to lens distance was %.1f mm" % (x2.position))
      print("Lens to detector distance was %.1f mm" % (x3.position-x2.position))
      print("Moving membrane and detector...")
      umv(x2,args[0],x3,args[0]+args[1])

   print(" Membrane to lens distance is %6.1f mm" % (x2.position))
   print(" Lens to detector distance is %6.1f mm" % (x3.position-x2.position))
   print("(Membrane to detector dist is %6.1f mm)" % (x3.position))
      
def focusScan(expTime=0.1):
   glob = ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   plotselect(camera.counters.total_std)
   membraneIn()
   dscan(focus01,-10,10,20,expTime,camera)
   goto_peak()
   dscan(focus01,-1,1,40,expTime,camera)
   goto_cen()
   endSaving()
   beep(1)   

def beep(repeats=1):
   for ii in range(repeats*6):
      print("\a",end="\r")
      sleep(.05)

      
def deccelerate_y2Axis():
   y2.velocity=4
   y2.acctime=0.5
   y2.backlash=1

def y2Test(nMoves=100):
   glob = ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   myDir("y2Test")
   myPointerFile("y2Test_pointer")
   SCAN_SAVING.images_path_template="y2Test_"
   SCAN_SAVING.images_prefix="y2Test_"
   pos1=np.zeros(nMoves)
   pos2=np.zeros(nMoves)
   plotselect(camera.counters.x)
   for ii in range(nMoves):
      umv(y2,-22.5)
      result=sct(1)
      pos1[ii]=result.get_data()["x"][0]
      umv(y2,22.5)
      result=sct(1)
      pos2[ii]=result.get_data()["x"][0]

   endSaving()

   fig,ax=pylab.subplots()
   ax.plot(np.arange(nMoves),pos1,"--ro")
   ax.set_ylabel("y2-position [mm]")
   ax.set_xlabel("steps number")
   ax2=ax.twinx()
   ax2.plot(np.arange(nMoves),pos2,"--bo")
   pylab.show()   

def y2TestB(nMoves=100):
   glob = ParametersWardrobe('xsvt-globals')
   camera=glob.camera
   myDir("y2Test")
   myPointerFile("y2Test_pointer")
   SCAN_SAVING.images_path_template="y2Test_"
   SCAN_SAVING.images_prefix="y2Test_"
   pos1=np.zeros((nMoves,10+1))
   pos1[:,-1]=np.arange(nMoves)
   plotselect(camera.counters.x)
   for ii in range(nMoves):
      lensOut()
      for jj in range(10):
         lensNin(jj)
         result=sct(1)
         pos1[ii,jj]=result.get_data()["x"][0]
         np.savetxt(SCAN_SAVING.base_path+"/y2Test.txt",pos1,"%.3f")
   endSaving()
   fig,ax=pylab.subplots()
   for ii in range(5):
      ax.plot(np.arange(nMoves),pos1[:,ii],"--")
   ax.set_ylabel("y2-position [mm]")
   ax.set_xlabel("steps number")
   ax2=ax.twinx()
   for ii in range(5):
      ax2.plot(np.arange(nMoves),pos1[:,ii+5],"--")
   pylab.show()

def inside(myvalue,lowlim,highlim):
   if (myvalue>=lowlim) and (myvalue<=highlim):
      return True
   else:
      return False

def outside(myvalue,lowlim,highlim):
   if (myvalue<lowlim) or (myvalue>highlim):
      return True
   else:
      return False

def attenuatorsOut():
   if outside(att1.position,-1.,1.) or outside(att2.position,-1.,1.) or outside(att3.position,-1.,1.):
      print("Removing attenuators!!!")
      reopen=False
      if fe.is_open:
         fe.close()
         reopen=True
      umv(att1,0)
      umv(att2,0)
      umv(att3,0)
      if reopen:
         fe.open()
   




         
      
###################################################################
###################################################################
initGlobals()
glob = ParametersWardrobe('xsvt-globals')
gevent.sleep(1)
print("Lenses_BM05.py loaded")
print("Root directory is %s" % glob.rootDir)
print("Believe we use camera %s" % glob.camera.name)
print("change with xsvt.changeDetector(pcogold) or similar")

attenuatorsOut()

current_session.disable_esrf_data_policy()
SCAN_SAVING.base_path=glob.rootDir

deccelerate_y2Axis()

try:
   glob.lensSubfolder
except AttributeError:
   print("No subfolder, no prefix defined yet.") 
   print("Please define a lensset you are working with.")
   print("Run xsvt.setLensSet()")
   endSaving()
else:
   pass





