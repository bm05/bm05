import numpy as np
from numpy.polynomial import Polynomial
from scipy.optimize import curve_fit

def E_from_th(x, thE, d):
    return 12.3982/(2*d*np.sin(x + thE))

best_vals = [dspacing,  0]  # 12.3982/(2*d*np.sin(x + thE))
best_vals, covar = curve_fit(E_from_th, np.asarray(th_array)*np.pi/180, np.asarray(e_array), p0=best_vals)


print('fit coefficients for: E = 12.3982/(2*d*np.sin(thB + thE))')
print('>>> E = 12.3982/(2*.4f*np.sin(thB + .4f))'%(best_vals[0], best_vals[1]*180/np.pi))

perr = np.sqrt(np.diag(covar))

print(perr)

