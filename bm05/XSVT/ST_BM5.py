# ############################################################################
# Speckle tracking macros for BM05 using BLISS
# Copyright (c) 2016-2022 ESRF - the European Synchrotron
# ############################################################################

__author__ = ['Thomas Roth', 'Rafael Celestre']
__contact__ = 'rafael.celestre@esrf.eu'
__license__ = 'MIT'
__copyright__ = 'ESRF - the European Synchrotron, Grenoble, France'
__date__ = '18/06/2022'

###################################################################
# imports & co

import numpy as np
import os
import pylab
import time
import glob
from scipy.optimize import curve_fit


from bliss.config.settings import ParametersWardrobe  # get equivalent to global variables
from bliss import current_session  # to enable custom file naming

global lensPosMot
global lensParasiticMot
global parasiticSlope
global parasiticY0
lensPosMot = y2         # auxiliary variables for lens alignment
lensParasiticMot = z2   # auxiliary variables for lens alignment
parasiticSlope = 0
parasiticY0 = lensParasiticMot.position

###################################################################


def monochromatic():
    fe.close()
    moveMONO("mono")
    moveEH1("mono")
    #remove attenuator:
    umv(att1,0)
    #reasonable primary slits
    umv(pshg,2,psvg,2)
    if np.abs(psvo.position)>0.5:
        umv(psvo,0)
    if np.abs(psho.position)>0.5:
        umv(psho,0)
    #open FE
    fe.open()

def moveMONO(position: str):
    """Align the monochromator to whitebeam or monochromatic beam"""
    mono_align = config.get('mono_align')
    mono_align.move(position)

def moveEH1(position: str):
    """Align EH1/OH to whitebeam or monochromatic beam"""
    eh1_oh_align = config.get("eh1_oh_align")
    if position == "mono":
        # ibtz is always moved by people, need to re-sync
        ibtz = eh1_oh_align.motors["ibtz"]
        ibtz.sync_hard()
    eh1_oh_align.move(position)






# ======================================================================================================================
# measurement routines
# ======================================================================================================================

def flats(exptime=3, subfolder="flats", prefix="flats_", repeats=10, lensfolder=None, keepLens=False):
    print("Taking flats.")
    membraneOut()
    if keepLens is False:
        lensOut()
    if lensfolder is not None:
        SCAN_SAVING.template = lensfolder
    SCAN_SAVING.images_path_template = subfolder.rstrip("_")
    SCAN_SAVING.images_prefix = prefix.rstrip("_") + "_"
    print("Saving to %s/%s/%s/%s_number.h5" % (
    SCAN_SAVING.base_path, SCAN_SAVING.template, subfolder.rstrip("_"), prefix.rstrip("_") + "_"))
    loopscan(repeats, exptime, sleep_time=0.1)


def darks(exptime=3, subfolder="darks", prefix="darks_", repeats=10, lensfolder=None):
    print("Taking darks.")
    eh1.close()
    if lensfolder is not None:
        SCAN_SAVING.template = lensfolder
    SCAN_SAVING.images_path_template = subfolder.rstrip("_")
    SCAN_SAVING.images_prefix = prefix.rstrip("_") + "_"
    print("Saving to %s/%s/%s/%s_number.h5" % (
    SCAN_SAVING.base_path, SCAN_SAVING.template, subfolder.rstrip("_"), prefix.rstrip("_") + "_"))
    loopscan(repeats, exptime, sleep_time=0.1)
    eh1.open()


def radios(exptime=3, subfolder="radios", prefix="radios_", repeats=10, lensfolder=None):
    print("Taking radios.")
    membraneOut()
    if lensfolder is not None:
    #     print('debug radios - changing folder')
        SCAN_SAVING.template = lensfolder

    SCAN_SAVING.images_path_template = subfolder.rstrip("_")
    SCAN_SAVING.images_prefix = prefix.rstrip("_") + "_"
    print("Saving to %s/%s/%s/%s_number.h5" % (
    SCAN_SAVING.base_path, SCAN_SAVING.template, subfolder.rstrip("_"), prefix.rstrip("_") + "_"))
    loopscan(repeats, exptime, sleep_time=0.1)


def radioSeries(exptime=3, repeats=10, subset="all", makeDarks=True, makeFlats=True, useLensFolder=False, subdir="radios",startY2=0,perpAlign=False):
    pwGlob = ParametersWardrobe('xsvt-globals')
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    lenstagarr = lenstagstr.split()
    camera = pwGlob.camera
    
    startPhi2=phi2.position
    startPsi2=psi2.position
    
    if makeDarks:
        print("taking darks....")
        darks(exptime, "darks", "darks", repeats)
    if makeFlats:
        print("taking flats....")
        flats(exptime, "flats", "flats", repeats)

    if pwGlob.lensSupport == "perpendicular":
        offset = startY2
    else:
        offset = 0

    for ii in range(len(lensposstr.split())):
        wantedPos = int(lensposstr.split()[ii])
        if subset != "all":
            subsetarr = generatePosStr(subset)
            if str(wantedPos) not in subsetarr:
                continue
        membraneOut()
        lensNin(wantedPos, offset=offset)
        print("%s" % lenstagarr[ii])
        sleep(1)

        if perpAlign:
           dscan(y2,-0.6,0.6,12,.1,camera.counters.total_avg)
           goto_com()
           ascan(phi2,-2,2,20,.1,camera.counters.total_avg)
           goto_peak()
           ascan(phi2,-2,2,20,.1,camera.counters.total_avg)
           goto_peak()


        if useLensFolder:
            radios(exptime, subfolder=("%s" % lenstagarr[ii]), prefix=("%s_radios" % lenstagarr[ii]), repeats=repeats)
        else:
            radios(exptime, subfolder=subdir, prefix=("%s_radios" % lenstagarr[ii]), repeats=repeats)

    if pwGlob.lensSupport == "perpendicular":
        umv(y2, offset)  # returning to first lens
        if perpAlign:
           umv(phi2,startPhi2,psi2,startPsi2)


    endSaving()


def perpSeries(exptime=2, repeats=10, subset="all", makeDarks=True, makeFlats=True, subdir="perpRadios", startRadiosAtY2=11, perpAlign=True):
    pwGlob = ParametersWardrobe('xsvt-globals')
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    lenstagarr = lenstagstr.split()
    camera = pwGlob.camera

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder
    
    startPhi2=phi2.position
    startPsi2=psi2.position
    startTh2=th2.position
    startZ2=z2.position
    startY2=y2.position

    umv(y2,40);
    setLensOut()
    
    if makeDarks:
        print("taking darks....")
        darks(exptime, "darks", "darks", repeats)
    if makeFlats:
        print("taking flats....")
        flats(exptime, "flats", "flats", repeats)

    if pwGlob.lensSupport == "perpendicular":
        offset = startRadiosAtY2-11
    else:
        print("Only works for lens support for perpendicular radius")
        return

    firstAlign=True
    for ii in range(len(lensposstr.split())):
        wantedPos = int(lensposstr.split()[ii])
        if subset != "all":
            subsetarr = generatePosStr(subset)
            if str(wantedPos) not in subsetarr:
                continue
        membraneOut()
        lensNin(wantedPos, offset=offset)
        currentY2=y2.position
        print("%s" % lenstagarr[ii])
        sleep(1)

        plotselect(camera.counters.total_avg)
        if perpAlign:
           ascan(phi2,-2,2,20,.1,camera.counters.total_avg)
           goto_peak();sleep(0.5);ct()
           dscan(y2,-0.6,0.6,12,.1,camera.counters.total_avg)
           goto_com();sleep(0.5);ct()
           if firstAlign:
              dscan(z2,-0.6,0.6,12,.1,camera.counters.total_avg)
              goto_com();sleep(0.5);ct()
              ascan(th2,-2,2,20,.1,camera.counters.total_avg)
              goto_peak();sleep(0.5);ct()
              firstAlign=False
           dscan(phi2,-0.5,0.5,20,.1,camera.counters.total_avg)
           goto_peak();sleep(0.5);ct()
           dscan(y2,-0.5,0.5,12,.1,camera.counters.total_avg)
           goto_com();sleep(0.5);ct()
           #tweak offset for next lensNin
           offset=offset-currentY2+y2.position
           dscan(th2,-0.5,0.5,20,.1,camera.counters.total_avg)
           goto_peak();sleep(0.5);ct()
           dscan(z2,-0.5,0.5,12,.1,camera.counters.total_avg)
           goto_com();sleep(0.5);ct()
           

        #radios(exptime, subfolder=("%s" % lenstagarr[ii]), prefix=("%s_radios" % lenstagarr[ii]), repeats=repeats)
        radios(exptime, subfolder=subdir, prefix=("%s_radios" % lenstagarr[ii]), repeats=repeats)

    #returning to initial position    
    umv(y2, startY2,  z2, startZ2,  th2,startTh2, phi2, startPhi2)  # returning to first lens

    endSaving()


# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------
def xsvtRefs(exptime=3, refdir="refs", refstag="refs_", scanType="vectscan", keepLens=False):
    membraneIn()
    if keepLens is False:
        lensOut()
    # waitForBeam(neededT=300)
    SCAN_SAVING.images_path_template = refdir.rstrip("_")
    SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_"
    if scanType == "both":
        SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_vectscan_"
        scan_membrane_2D(exptime=exptime, scanType="vectscan")
        SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_spiral_"
        scan_membrane_2D(exptime=exptime, scanType="spiral")
    else:
        scan_membrane_2D(exptime=exptime, scanType=scanType)


def xsvtSingle(lensfolder, lenstag, exptime=3, startRefs=True, endRefs=False, makeRadios=True, scanType='vectscan', n_rep=10, beeping=True,material="unknown",radius=-1):

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder


    if startRefs:
        membraneOut()
        lensOut()
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        xsvtRefs(exptime, "refs", "refStart", scanType=scanType)

    membraneIn()
    lensIn()

    SCAN_SAVING.images_path_template = ("%s" % lenstag)
    SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstag)

    scan_membrane_2D(exptime=exptime, scanType=scanType)

    if endRefs:
        xsvtRefs(exptime, "refs", "refEnd", scanType=scanType)
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)

    if makeRadios:
        lensIn()
        radios(exptime, "radios", "radios", n_rep)

    endSaving()
    write_ini_file("xsvt",lensfolder,lenstag,material,radius,step=1)
    if beeping:
        beep(10)


def xsvtSeries(exptime=3, subset="all", startRefs=True, endRefs=True, makeRadios=True, postAlign=True, scanType="vectscan", n_rep=10):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    materialstr= pwGlob.materialStr
    radiusStr = pwGlob.radiusStr
    radiusarr = radiusStr.split()
    lenstagarr = lenstagstr.split()
    materialarr= materialstr.split()

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if makeRadios:
        radioSeries(exptime=exptime, repeats=n_rep,  subset=subset, makeDarks=False, makeFlats=False)

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if startRefs:
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        print(scanType)
        xsvtRefs(exptime, "refs", "refStart", scanType=scanType)

    for ii in range(len(lensposstr.split())):
        wantedPos = int(lensposstr.split()[ii])
        print("Do we need position %d ?" % wantedPos)
        if subset != "all":
            subsetarr = generatePosStr(subset).split()
            print(subsetarr)
            if str(wantedPos) not in subsetarr:
                print("%d  is not wanted " % wantedPos)
                continue
        membraneIn()
        lensNin(wantedPos)
        if postAlign:
            SCAN_SAVING.images_path_template = ("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix = ("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            umvr(y2,-3);ct(.1)
            dscan(y2, 0, 6, 20, .1)
            goto_com()
        SCAN_SAVING.images_path_template = ("%s" % lenstagarr[ii])
        SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstagarr[ii])
        if scanType == "both":
            scan_membrane_2D(exptime=exptime, scanType="vectscan")
            scan_membrane_2D(exptime=exptime, scanType="spiral")
        else:
            scan_membrane_2D(exptime=exptime, scanType=scanType)
        create2DxsvtIni(materialarr[ii],radiusarr[ii],lenstagarr[ii])
                
            
    if endRefs:
        xsvtRefs(exptime, "refs", "refEnd", scanType=scanType)
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)

    endSaving()
    beep(10)


def create2DxsvtIni(material,radius,tag):
    pwGlob = ParametersWardrobe('xsvt-globals')
    DDD=pwGlob.rootDir.split("/")[-1].split("lens")[0]
    outdir="/data/optics/refractive_optics/fromBM5"
       
    outFileName="/data/optics/refractive_optics/fromBM5/%s_2D_R%sum_%s_%s.ini" % (material,radius,tag,DDD)

    templateFile=open("/users/blissadm/local/bm05.git/bm05/XSVT/2Dxsvt.ini")
    templateLines=templateFile.readlines()

    outf=open(outFileName,"w")
    for line in templateLines:
        line=line.replace("___METHOD___","xsvt")
        line=line.replace("___MAT___","")
        line=line.replace("___SAMPLETAG___",tag)
        line=line.replace("___EEE___","%f" %  monoE.position)
        if pwGlob.magnification.rstrip("xX")=="4":
            pixsize=1.588
        elif  pwGlob.magnification.rstrip("xX")=="10":
            pixsize=0.6333
        else:
            pixsize=0.317
        line=line.replace("___PIX___","%s" % pixsize)
        line=line.replace("___DIST1___","%.3f" % x2.position)
        line=line.replace("___DIST2___","%.3f" % (x3.position-x2.position))
        line=line.replace("DDMMYY","%s" % DDD)
        line=line.replace("___MAG___","%s" % pwGlob.magnification.rstrip("xX"))
        line=line.replace("___Al_2D_RRRum___","%s_2D_%sum" % (material,radius))
        line=line.replace("___DIROUT___","%s_%s" % (tag,DDD))
        line=line.replace("DDDlenses","%slenses" % DDD)
        line=line.replace("___ROOT___","%s" % pwGlob.rootDir.split("/")[-1])
        line=line.replace("___FILENAME___","%s" % pwGlob.lensSubfolder)
        line=line.replace("___NUMBER___","0000")
        line=line.replace("___SEARCH___","50")
        try:
           line=line.replace("___DISTO___","%s" % pwGlob.detDistortName)  # piezo_20x_10um_disto without h.tif or v.tif
        except:
            print("Implement detector distorion global variable pwGlob.detDistortName")
        line=line.replace("BM05_DDD","BM05_%s" % DDD)

        outf.write(line)        
    outf.close()
    print("Ini file %s generated" % outFileName)
    

def xsvtSlowSeries(exptime=3, subset="all", postAlign=True, scanType="vectscan", n_rep=10, singleLens=False):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    materialstr= pwGlob.materialStr
    radiusStr = pwGlob.radiusStr
    radiusarr = radiusStr.split()
    lenstagarr = lenstagstr.split()
    materialarr= materialstr.split()
    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    for ii in range(len(lensposstr.split())):

        wantedPos = int(lensposstr.split()[ii])
        print("Do we need position %d ?" % wantedPos)
        if subset != "all":
            subsetarr = generatePosStr(subset).split()
            print(subsetarr)
            if str(wantedPos) not in subsetarr:
                print("%d  is not wanted " % wantedPos)
                continue

        eh1.close(); sleep(1)    
        darks(exptime, "darks", "dark_%s" % lenstagarr[ii], n_rep)

        lensOut()
        membraneOut()
        eh1.open(); sleep(1)
        flats(exptime, "flats", "flat_%s" % lenstagarr[ii], n_rep)

        if singleLens:
            lensIn()
        else:
            lensNin(wantedPos)
        if postAlign:
            SCAN_SAVING.images_path_template = ("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix = ("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            umvr(y2,-3);ct(.1)
            dscan(y2, 0, 6, 20, .1)
            goto_com()
            SCAN_SAVING.template = pwGlob.lensSubfolder
            SCAN_SAVING.data_filename = pwGlob.lensSubfolder
        radios(exptime, "radios", "radio_%s" % lenstagarr[ii], repeats=n_rep)

        membraneIn()
        SCAN_SAVING.images_path_template = ("%s" % lenstagarr[ii])
        SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstagarr[ii])
        if scanType == "both":
            scan_membrane_2D(exptime=exptime, scanType="vectscan")
            scan_membrane_2D(exptime=exptime, scanType="spiral")
        else:
            scan_membrane_2D(exptime=exptime, scanType=scanType)
            #write_ini_file("xsvt",pwGlob.lensSubfolder,lenstagarr[ii],materialarr[ii],radiusarr[ii],step=1)

        lensOut()    
        xsvtRefs(exptime, "refs", "refs_%s" % lenstagarr[ii], scanType=scanType)
            
    endSaving()
    beep(10)


def xsvtMultiDist(distList, lensfolder, lenstag=None, exptime=3, abs_dist=True, postAlign=True, scanType="vectscan", n_rep=10, settleTime=3, keepLens=False, keepMembrane=False):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    
    if keepLens is False:
        lensOut()

    for distance in distList:
        if abs_dist is True:
            umv(x3, distance)
        else:
            umvr(x3, distance)
        sleep(settleTime)

        # RC220618: this only works for a SS of about the FOV on the CCD
        if postAlign:
            if keepMembrane==False:
               membraneOut()
            SCAN_SAVING.images_path_template = "post_align"
            SCAN_SAVING.images_prefix = "post_align_"
            plotselect(camera.counters.total_avg)
            umvr(y3,-1.5); ct(0.1); umvr(y3, 1.5)
            dscan(y3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            # umv(y3, (cen()+peak())/2)
            sleep(settleTime)

            plotselect(camera.counters.total_avg)
            umvr(z3,-1.5); ct(0.1); umvr(z3, 1.5)
            dscan(z3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            # umv(z3, (cen()+peak())/2)
            sleep(settleTime)
        SCAN_SAVING.template = lensfolder
        SCAN_SAVING.data_filename = lensfolder

        if lenstag is None:
            file_tag = 'd_%04dmm'% int(distance)
        else:
            file_tag = lenstag + '_d_%04dmm'% int(distance)
        xsvtRefs(exptime, "speckles", file_tag, scanType=scanType, keepLens=keepLens)
        if not keepMembrane:
           flats(exptime, "flats", "flat_"+file_tag, n_rep, keepLens=keepLens)

    darks(exptime, "darks", "darkStart", n_rep)

    endSaving()
    beep(10)


def xsvtMultiAngle(angleList, lensfolder, lenstag=None, exptime=3, abs_angle=True, postAlign=False, startRefs=True, endRefs=True, makeRadios=True, scanType="vectscan", n_rep=10, settleTime=3):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    if startRefs:
        flats(exptime, "flats", "flatStart", n_rep)
        darks(exptime, "darks", "darkStart", n_rep)
        print(scanType)
        xsvtRefs(exptime, "refs", "refStart", scanType=scanType)
    
    for angle in angleList:

        if abs_angle is True:
            umv(th2, angle)
        else:
            umvr(th2, angle)
        sleep(settleTime)
        
        # RC220618: this only works for a SS of about the FOV on the CCD
        if postAlign:
            SCAN_SAVING.images_path_template = "post_align"
            SCAN_SAVING.images_prefix = "post_align_"
            plotselect(camera.counters.total_avg)
            dscan(y3, -1.5, 1.5, 50, .1)
            goto_peak()
            # umv(y3, (cen()+peak())/2)
            sleep(settleTime)

            plotselect(camera.counters.total_avg)
            dscan(z3, -1.5, 1.5, 50, .1)
            goto_peak()
            # umv(z3, (cen()+peak())/2)
            sleep(settleTime)
            
        SCAN_SAVING.template = lensfolder
        SCAN_SAVING.data_filename = lensfolder

        if lenstag is None:
            file_tag = 'th_%03ddeg'% int(angle)
        else:
            file_tag = lenstag + '_th_%03ddeg'% int(angle)
            
        xsvtSingle(lensfolder, file_tag, exptime=3, startRefs=False, endRefs=False, makeRadios=False, scanType=scanType, n_rep=n_rep, beeping=False)
        
        if makeRadios:
            SCAN_SAVING.template = lensfolder
            SCAN_SAVING.data_filename = lensfolder
            membraneOut()
            lensIn()
            if lenstag is None:
                file_tag = 'radio_th_%03ddeg'% int(angle)
            else:
                file_tag = lenstag + 'radio_th_%03ddeg'% int(angle)
            radios(exptime, "radios", file_tag, n_rep)

    if startRefs:
        SCAN_SAVING.template = lensfolder
        SCAN_SAVING.data_filename = lensfolder
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)
        print(scanType)
        xsvtRefs(exptime, "refs", "refEnd", scanType=scanType)

    endSaving()
    beep(10)

# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------
def xss1DRefs(Nsteps, dx, exptime=3, refdir="refs", refstag="refs_", scanType="both", keepLens=False):
    membraneIn()
    if keepLens is False:
        lensOut()
    # waitForBeam(neededT=300)
    if scanType == "both" or scanType.startswith("h"):
        SCAN_SAVING.images_path_template = refdir
        SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_hor_"
        scan_membrane_1D(Nsteps, dx, exptime, scanType="hor")
    if scanType == "both" or scanType.startswith("v"):
        SCAN_SAVING.images_path_template = refdir
        SCAN_SAVING.images_prefix = refstag.rstrip("_") + "_ver_"
        scan_membrane_1D(Nsteps, dx, exptime, scanType="ver")


def xss1DSingle(lensfolder, lenstag, Nsteps=101, dx=1., exptime=3, startRefs=True, endRefs=True, makeRadios=True,
                scanType="both", n_rep=10):

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    membraneOut()
    lensOut()

    if startRefs:
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        xss1DRefs(Nsteps, dx, exptime, "refs", "refStart", scanType=scanType)

    membraneIn()
    lensIn()

    SCAN_SAVING.images_path_template = ("%s" % lenstag)

    if scanType == "both":
        SCAN_SAVING.images_prefix = ("%s_speckles_hor_" % lenstag)
        scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType="hor")
        SCAN_SAVING.images_prefix = ("%s_speckles_ver_" % lenstag)
        scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType="ver")
    else:
        if scanType.startswith("h"):
            SCAN_SAVING.images_prefix = ("%s_speckles_hor_" % lenstag)
        else:
            SCAN_SAVING.images_prefix = ("%s_speckles_ver_" % lenstag)
        scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType=scanType)

    if endRefs:
        darks(exptime, "darks", "darkEnd", n_rep)
        flats(exptime, "flats", "flatEnd", n_rep)
        xss1DRefs(Nsteps, dx, exptime, "refs", "refEnd", scanType=scanType)

    if makeRadios:
        lensIn()
        radios(exptime, "radios", "radios", n_rep)

    endSaving()
    beep(10)


def xss1DSeries(Nsteps=101, dx=1., exptime=3, subset="all", startRefs=True, endRefs=True, makeRadios=True,
                postAlign=True, scanType="both", n_rep=10):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    lenstagarr = lenstagstr.split()

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if makeRadios:
        print('debug xss1DSeries - makeRadios is True')
        radioSeries(exptime=exptime, repeats=n_rep, subset=subset, makeDarks=False, makeFlats=False)

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if startRefs:
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        print(scanType)
        xss1DRefs(Nsteps, dx, exptime, "refs", "refStart", scanType=scanType)

    for ii in range(len(lensposstr.split())):
        wantedPos = int(lensposstr.split()[ii])
        print("Do we need position %d ?" % wantedPos)
        if subset != "all":
            subsetarr = generatePosStr(subset).split()
            print(subsetarr)
            if str(wantedPos) not in subsetarr:
                print("%d  is not wanted " % wantedPos)
                continue
        membraneIn()
        lensNin(wantedPos)
        if postAlign:
            SCAN_SAVING.images_path_template = ("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix = ("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            dscan(y2, -3, 3, 20, .1)
            goto_peak()

        SCAN_SAVING.images_path_template = ("%s" % lenstagarr[ii])
        if scanType == "both":
            SCAN_SAVING.images_prefix = ("%s_speckles_hor_" % lenstagarr[ii])
            scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType="hor")
            SCAN_SAVING.images_prefix = ("%s_speckles_ver_" % lenstagarr[ii])
            scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType="ver")
        else:
            if scanType.startswith("h"):
                SCAN_SAVING.images_prefix = ("%s_speckles_hor_" % lenstagarr[ii])
            else:
                SCAN_SAVING.images_prefix = ("%s_speckles_ver_" % lenstagarr[ii])
            scan_membrane_1D(Nsteps, dx, exptime=exptime, scanType=scanType)

    if endRefs:
        xss1DRefs(Nsteps, dx, exptime, "refs", "refEnd", scanType=scanType)
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)

    endSaving()
    beep(10)


def xss1DMultiDist(distList, lensfolder, lenstag=None, Nsteps=101, dx=1., exptime=3, abs_dist=True, postAlign=True,
                   scanType="both", n_rep=10, settleTime=3, keepLens=False):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder


    if keepLens is False:
        lensOut()

    for distance in distList:
        if abs_dist is True:
            umv(x3, distance)
        else:
            umvr(x3, distance)
        sleep(settleTime)

        # RC220618: this only works for a SS of about the FOV on the CCD
        if postAlign:
            membraneOut()
            SCAN_SAVING.images_path_template = "post_align"
            SCAN_SAVING.images_prefix = "post_align_"
            plotselect(camera.counters.total_avg)
            umvr(y3,-1.5); ct(0.1); umvr(y3, 1.5)
            dscan(y3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            sleep(settleTime)

            plotselect(camera.counters.total_avg)
            umvr(z3,-1.5); ct(0.1); umvr(z3, 1.5)
            dscan(z3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            sleep(settleTime)
        SCAN_SAVING.template = lensfolder
        SCAN_SAVING.data_filename = lensfolder

        if lenstag is None:
            file_tag = 'd_%04dmm' % int(distance)
        else:
            file_tag = lenstag + '_d_%04dmm' % int(distance)
        xss1DRefs(Nsteps, dx, exptime, "speckles", file_tag, scanType=scanType, keepLens=keepLens)
        flats(exptime, "flats", "flat_" + file_tag, n_rep, keepLens=keepLens)

    darks(exptime, "darks", "darkStart", n_rep)

    endSaving()
    beep(10)

# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------
def xss2DRefs(Nsteps=35, dx=1., exptime=3, refdir="refs", refstag="refs_", keepLens=False):
    membraneIn()
    if keepLens is False:
        lensOut()
    SCAN_SAVING.images_path_template = refdir
    SCAN_SAVING.images_prefix = refstag.rstrip("_")
    cenPos = 250
    xmin = cenPos - (Nsteps * dx / 2.)
    xmax = cenPos + (Nsteps * dx / 2.)
    scan_membrane_2D(xmin, xmax, Nsteps, xmin, xmax, Nsteps, exptime, silent=True, scanType="xss")


def xss2DSingle(lensfolder, lenstag, Nsteps=35, NstepsSample=None, dx=1., exptime=3, startRefs=True, endRefs=True, makeRadios=True, n_rep=10):

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    membraneOut()
    lensOut()

    if NstepsSample is None:
        NstepsSample = Nsteps

    cenPos = 250
    xmin = cenPos - (NstepsSample * dx / 2.)
    xmax = cenPos + (NstepsSample * dx / 2.)

    if startRefs:
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        xss2DRefs(Nsteps, dx, exptime, refdir="refs", refstag="refsStart_")

    membraneIn()
    lensIn()

    SCAN_SAVING.images_path_template = ("%s" % lenstag)
    SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstag)
    scan_membrane_2D(xmin, xmax, NstepsSample, xmin, xmax, NstepsSample, exptime, silent=True, scanType="xss")

    if endRefs:
        darks(exptime, "darks", "darkEnd", n_rep)
        flats(exptime, "flats", "flatEnd", n_rep)
        xss2DRefs(Nsteps, dx, exptime, refdir="refs", refstag="refEnd")

    if makeRadios:
        lensIn()
        radios(exptime, "radios", "radios", n_rep)

    endSaving()
    beep(10)


def xss2DSeries(exptime=3, subset="all", Nsteps=35, NstepsSample=None, dx=1., startRefs=True, endRefs=True, makeRadios=True, postAlign=True, n_rep=10):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    lenstagarr = lenstagstr.split()

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if makeRadios:
        radioSeries(exptime=exptime, repeats=n_rep,  subset=subset, makeDarks=False, makeFlats=False)

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    if NstepsSample is None:
        NstepsSample = Nsteps

    cenPos = 250
    xmin = cenPos - (NstepsSample * dx / 2.)
    xmax = cenPos + (NstepsSample * dx / 2.)

    if startRefs:
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        xss2DRefs(Nsteps, dx, exptime, refdir="refs", refstag="refsStart_")

    for ii in range(len(lensposstr.split())):
        wantedPos = int(lensposstr.split()[ii])
        print("Do we need position %d ?" % wantedPos)
        if subset != "all":
            subsetarr = generatePosStr(subset).split()
            if str(wantedPos) not in subsetarr:
                print("%d  is not wanted " % wantedPos)
                continue
        membraneIn()
        lensNin(wantedPos)
        if postAlign:
            SCAN_SAVING.images_path_template = ("%s_align" % lenstagarr[ii])
            SCAN_SAVING.images_prefix = ("%s_align_" % lenstagarr[ii])
            plotselect(camera.counters.total_avg)
            dscan(y2, -3, 3, 20, .1)
            goto_peak()
        SCAN_SAVING.images_path_template = ("%s" % lenstagarr[ii])
        SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstagarr[ii])
        scan_membrane_2D(xmin, xmax, NstepsSample, xmin, xmax, NstepsSample, exptime, silent=True, scanType="xss")

    if endRefs:
        xss2DRefs(Nsteps, dx, exptime, refdir="refs", refstag="refEnd")
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)

    endSaving()
    beep(10)


def xss2DMultiDist(distList, lensfolder, lenstag=None, Nsteps=35, dx=1., exptime=3, abs_dist=True, postAlign=True,
                   n_rep=10, settleTime=3, keepLens=False):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    if keepLens is False:
        lensOut()

    for distance in distList:
        if abs_dist is True:
            umv(x3, distance)
        else:
            umvr(x3, distance)
        sleep(settleTime)

        # RC220618: this only works for a SS of about the FOV on the CCD
        if postAlign:
            membraneOut()
            SCAN_SAVING.images_path_template = "post_align"
            SCAN_SAVING.images_prefix = "post_align_"
            plotselect(camera.counters.total_avg)
            umvr(y3,-1.5); ct(0.1); umvr(y3, 1.5)
            dscan(y3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            # umv(y3, (cen()+peak())/2)
            sleep(settleTime)

            plotselect(camera.counters.total_avg)
            umvr(z3,-1.5); ct(0.1); umvr(z3, 1.5)
            dscan(z3, -1.5, 1.5, 50, exptime/5)
            goto_cen()
            # umv(z3, (cen()+peak())/2)
            sleep(settleTime)
        SCAN_SAVING.template = lensfolder
        SCAN_SAVING.data_filename = lensfolder
        
        if lenstag is None:
            file_tag = 'd_%04dmm' % int(distance)
        else:
            file_tag = lenstag + '_d_%04dmm' % int(distance)
        xss2DRefs(Nsteps, dx, exptime, "speckles", file_tag, keepLens=keepLens)
        flats(exptime, "flats", "flat_" + file_tag, n_rep, keepLens=keepLens)

    darks(exptime, "darks", "darkStart", n_rep)

    endSaving()
    beep(10)


# ======================================================================================================================
# Scans functions
# ======================================================================================================================
def scan_membrane_1D(Nsteps, dx, exptime, scanType):
    pwGlob = ParametersWardrobe('xsvt-globals')
    prepare_piezos()

    cenPos = 250
    xmin = cenPos - (Nsteps * dx / 2.)
    xmax = cenPos + (Nsteps * dx / 2.)

    if scanType.startswith("h"):
        print("Now we do a horizontal 1D xss scan.")
        umv(pzh, xmin, pzv, 250)
        sleep(1)
        ascan(pzh, xmin, xmax, Nsteps, exptime, srcur, pwGlob.camera)
    if scanType.startswith("v"):
        print("Now we do a vertical 1D xss scan.")
        umv(pzv, xmin, pzh, 250)
        sleep(1)
        ascan(pzv, xmin, xmax, Nsteps, exptime, srcur, pwGlob.camera)


def scan_membrane_2D(xmin=20, xmax=480, xsteps=10, ymin=20, ymax=480, ysteps=10, exptime=3, silent=True, scanType="vectscan"):
    # scanType = 'vectscan' (XSVT), 'spiral' (UMPA), 'xss' (2D xss)
    pwGlob = ParametersWardrobe('xsvt-globals')

    prepare_piezos()

    if scanType != 'xss':
        if scanType == "vectscan":
            harr, varr = vectscan(xmin, xmax, xsteps, ymin, ymax, ysteps, silent=silent)
        elif scanType == "spiral":
            harr, varr = spiral(x0=250, y0=250, d=35, rmax=200)
        else:
            print("Scantype %s is not known" % scanType)
            return
        print("Moving piezos to the first point ...")
        umv(pzh, harr[0], pzv, varr[0])
        sleep(1)
        print("Starting the lookupscan now ...")
        lookupscan([(pzh, harr), (pzv, varr)], exptime, srcur, pwGlob.camera)
    else:
        print("Moving piezos to the first point ...")
        umv(pzh, xmin, pzv, ymin)
        sleep(1)
        amesh(pzh, xmin, xmax, xsteps, pzv, ymin, ymax, ysteps, exptime, srcur, pwGlob.camera)


def vectscan(xmin=20, xmax=480, xsteps=10, ymin=20, ymax=480, ysteps=10, silent=False):

    def _calc_pos(mymin, mymax, N):
        arr = np.arange(N)
        arr = mymin + (np.exp2(arr) - 1) * (mymax - mymin) / (np.exp2(N - 1) - 1)
        return arr

    if xmin < 0 or ymin < 0:
        print("Piezos can not go below 0. Better stay above 20")
        raise KeyboardInterrupt
    if xmax > 500 or ymax > 500:
        print("Piezos can not go above 500. Better stay below 480")
        raise KeyboardInterrupt
    if xsteps < 3 or xsteps > 50 or ysteps < 3 or xsteps > 50:
        print("Reasonable number of intervals between 4 and 20!")
        raise KeyboardInterrupt
    xarr = _calc_pos(xmin, xmax, xsteps)
    yarr = _calc_pos(ymin, ymax, ysteps)
    harr = np.tile(xarr, ysteps)
    varr = np.sort(np.tile(yarr, xsteps))
    if not silent:
        print(harr)
        print(varr)
    return harr, varr


def spiral(x0=250, y0=250, d=30, rmax=150, initialDirection="W", clockwise=True, showSpiral=False):
    getlength = 1
    for nn in range(int(1.0 * rmax / d)):  # number of required rings
        r = (nn + 1) * d
        u = 2 * np.pi * r
        nu = int(u / d + 0.5)  # number of points on ring
        getlength += nu
    print("points on spiral: %d " % getlength)
    xarr = np.zeros(getlength)
    yarr = np.zeros(getlength)
    xarr[0] = x0
    yarr[0] = y0
    ctr = 1
    initDir = {"E": 0, "S": np.pi / 2, "W": np.pi, "N": 3 * np.pi / 2}
    alpha = initDir[initialDirection]
    if clockwise:
        sign = 1
    else:
        sign = -1
    for nn in range(int(1.0 * rmax / d)):  # number of required rings
        r = (nn + 1) * d
        u = 2 * np.pi * r
        nu = int(u / d + 0.5)  # number of points on ring
        for ii in range(nu):
            xarr[ctr] = x0 + r * np.sin(alpha + 2 * np.pi / nu * ii * sign)
            yarr[ctr] = y0 + r * np.cos(alpha + 2 * np.pi / nu * ii * sign)
            ctr += 1
        alpha = alpha + 2 * np.pi / nu * (nu - 1) * sign
    if showSpiral:
        pylab.plot(xarr, yarr, "--ro")
        pylab.show()
    return xarr, yarr


# ======================================================================================================================
# Lens(es) functions
# ======================================================================================================================

def setLensOut(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 0:
        lensOutPos = [(y2.name, y2.position)]  # tuple of motor and motor position
    else:
        lensOutPos = []
        for motor in args:
            lensOutPos.append((motor.name, motor.position))
    pwGlob.add('lensOutPos', lensOutPos)
    showLensOut()


def showLensOut():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensOutPos
    except AttributeError:
        print("glob.lensOutPos not known yet.")
        print("Use setLensOut() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    print("Lens Out at:")
    for myMotor in pwGlob.lensOutPos:
        print("  Motor %s at %.3f" % (myMotor[0], myMotor[1]))


def lensOut():
    pwGlob = ParametersWardrobe('xsvt-globals')
    showLensOut()
    print("Moving lens out of the beam.")
    for motor in pwGlob.lensOutPos:
        umv(config.get(motor[0]), motor[1])


# ------------------------------------------------------------
# ------------------------------------------------------------ Single lens
def setLensIn(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 0:
        lensInPos = [(y2.name, y2.position)]  # tuple of motor and motor position
    else:
        lensInPos = []
        for motor in args:
            lensInPos.append((motor.name, motor.position))
    pwGlob.add('lensInPos', lensInPos)
    showLensIn()


def showLensIn():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensInPos
    except AttributeError:
        print("xsvtglob.lensInPos not known yet.")
        print("Use setLensIn() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    print("Lens In at:")
    for myMotor in pwGlob.lensInPos:
        print("  Motor %s at %.3f" % (myMotor[0], myMotor[1]))


def lensIn():
    pwGlob = ParametersWardrobe('xsvt-globals')
    showLensIn()
    print("Moving lens into the beam.")
    for motor in pwGlob.lensInPos:
        umv(config.get(motor[0]), motor[1])


# ------------------------------------------------------------
# ------------------------------------------------------------ Lens plate
def setLensSet(newDir, materialStr, newTagStr, newPosStr="0 1 2 3 4 5 6 7 8 9", plateType="2D", radiusStr=None):

    pwGlob = ParametersWardrobe('xsvt-globals')
    if materialStr is None:
        materialStr = ""
    if len(materialStr.split()) == 1:
        # only 1 material given
        singleElement = materialStr
        materialStr = ""
        for ii in range(len(newTagStr.split())):
            materialStr = materialStr + " " + singleElement
            materialStr = materialStr.lstrip(" ")
    else:
        # many materials given
        if len(materialStr.split()) != len(newTagStr.split()):
            print("Not the same number of lens tags as there are in list of materials")
            print("Please redo setLensSet() !")
            return

    print(radiusStr)    
    if radiusStr is None:
        radiusStr = ""
    elif len(radiusStr.split()) == 1:
        # only 1 material given
        singleRadius = radiusStr
        radiusStr = ""
        for ii in range(len(newTagStr.split())):
            radiusStr = radiusStr + " " + singleRadius
            radiusStr = radiusStr.lstrip(" ")
        print(radiusStr)
    else:
        # many radii given
        if len(radiusStr.split()) != len(newTagStr.split()):
            print("Not the same number of lens tags as there are in list of radii")
            print("Please redo setLensSet() !")
            return
        
    if ("-" in newPosStr) or (":" in newPosStr) or ("," in newPosStr):
        newPosStr = generatePosStr(newPosStr)
    if newPosStr == "":
        print("empty newPosStr is not allowed.")
        print("Please redo setLensSet() !")
        return
    if plateType not in ["2D", "1D", "pinholes", "perpendicular"]:
        print("Known plate types only 1D, 2D, pinholes, and perpendicular")
        print("Please redo setLensSet() !")
        return
    if len(newPosStr.split()) != len(newTagStr.split()):
        print("Not the same number of lens tags as there are lens positions")
        print("Please redo setLensSet() !")

        
    pwGlob.add('lensSupport', plateType)
    pwGlob.add('lensSubfolder', newDir)
    pwGlob.add('lensPosStr', newPosStr)
    pwGlob.add('lensTagStr', newTagStr)
    pwGlob.add('materialStr', materialStr)
    pwGlob.add('radiusStr', radiusStr)

    showLensSet()


def showLensSet():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensTagStr
        pwGlob.lensPosStr
        pwGlob.lensSupport
        pwGlob.lensSubfolder
    except AttributeError:
        print("At least one of")
        print("pwGlob.lensTagStr")
        print("pwGlob.lensPosStr")
        print("pwGlob.lensSupport")
        print("pwGlob.lensSubfolder")
        print("not known yet.")
        print("Use setLensSet() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt

    posLen=len(pwGlob.lensPosStr.split())
    tagLen=len(pwGlob.lensTagStr.split())
    try:
       radLen=len(pwGlob.radiusStr.split())
    except:
       radLen=0
    try:
       matLen=len(pwGlob.materialStr.split())
    except:
       matLen=0
    maxLen=max(posLen,tagLen,matLen,radLen)
       
    print("Currently defined lens support plate:")
    print("   subfolder, i.e. global tag : %s" % pwGlob.lensSubfolder)
    print("   support type:                %s" % pwGlob.lensSupport)
    print("   position | lens tag          | material   | radius")
    print("   ---------+-------------------+------------+-------------")
    someError=False
    for ii in range(maxLen):
        try:
            print("   %8d |" % int(pwGlob.lensPosStr.split()[ii]), end="")
        except:
            print("            |",end="")
            someError=True
        try:
            print(" %17s |" %  pwGlob.lensTagStr.split()[ii], end="")
        except:
            print("                   |",end="")
            someError=True
        try:    
            print(" %10s |" %  pwGlob.materialStr.split()[ii], end="")
        except:
            print("            |", end="")
            if pwGlob.materialStr!="":
               someError=True
        try:
            print(" %8s " % str(pwGlob.radiusStr.split()[ii]), end="")
        except:
            print("      ", end="")
            if pwGlob.radiusStr!="":
               someError=True
        print(" ")

    if someError:
        print("ATTENTION: different number of defined positions, tags, radii and materials !!!!!")
        print("Please redo setLensSet() !")


def lensNin(nr, move=True, offset=0):
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensPosMot
        pwGlob.lensParasiticMot
        pwGlob.parasiticSlope
        pwGlob.parasiticY0
    except AttributeError:
        print("At least one of")
        print("pwGlob.lensPosMot")
        print("pwGlob.lensParasiticMot")
        print("pwGlob.parasiticSlope")
        print("pwGlob.parasiticY0")
        print("not known yet.")
        print("Use setLensMotors() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    try:
        pwGlob.lensTagStr
        pwGlob.lensPosStr
        pwGlob.lensSupport
        pwGlob.lensSubfolder
    except AttributeError:
        print("At least one of")
        print("pwGlob.lensTagStr")
        print("pwGlob.lensPosStr")
        print("pwGlob.lensSupport")
        print("pwGlob.lensSubfolder")
        print("not known yet.")
        print("Use setLensSet() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt

    if pwGlob.lensSupport == "2D":
        spacing = 15
        pos0 = -4.5 * spacing
    elif pwGlob.lensSupport == "1D":
        spacing = 24
        pos0 = -4.5 * spacing
    elif pwGlob.lensSupport == "pinholes":
        spacing = -1
        pos0 = -4.5 * 24
    elif pwGlob.lensSupport == "perpendicular":
        spacing = -1.2
        pos0 = 11
    else:
        print("Unknown support plate of type %s" % pwGlob.lensSupport)
        print("Known are only 1D, 2D and pinholes")
        raise KeyboardInterrupt
    if pwGlob.lensSupport == "pinholes":
        positions = np.arange(10) * 15 - 4.5 * 15 + offset
        positions = np.append(positions, np.arange(10) * 24 - 4.5 * 24 + offset)
        positions = np.sort(positions)
    if pwGlob.lensSupport == "perpendicular":
        positions = np.arange(24) * spacing + pos0 + offset
    else:
        positions = np.arange(10) * spacing + pos0 + offset
    if move:
        umv(config.get(pwGlob.lensPosMot), positions[nr])
    else:
        print("umv(%s,%.4f)" % (pwGlob.lensPosMot, positions[nr]))
    if pwGlob.lensParasiticMot != None:
        if move:
            umv(config.get(pwGlob.lensParasiticMot), pwGlob.parasiticSlope * positions[nr] + pwGlob.parasiticY0)
        else:
            print("umv(%s,%.4f)" % (pwGlob.lensParasiticMot, pwGlob.parasiticSlope * positions[nr] + pwGlob.parasiticY0))
    if move:
        print("Moved in lens on position %d" % nr)
        arrayElement = getIndex(pwGlob.lensPosStr, nr)
        print("called %s" % pwGlob.lensTagStr.split()[arrayElement])


def showLensNin(nr):
    lensNin(nr, move=False)


# ------------------------------------------------------------
# ------------------------------------------------------------ Aux lens functions
def getIndex(posStr, nr):
    for ii in range(len(posStr.split())):
        if nr == int(posStr.split()[ii]):
            return ii


def generatePosStr(inStr):
    try:
        commaparts = inStr.split(",")
        outArr = []
        ictr = 0
        for idx in range(len(commaparts)):
            colonparts = commaparts[idx].split("-");
            if len(colonparts) == 1:
                colonparts = commaparts[idx].split(":");
            if len(colonparts) == 1:
                imstart = int(commaparts[idx]);
                imend = imstart;
            else:
                imstart = int(colonparts[0])
                imend = int(colonparts[1])

            for ii in range(imend - imstart + 1):
                outArr.append(imstart + ii)
                ictr = ictr + 1
        outStr = ' '.join(str(p) for p in outArr)
    except:
        print("Incomprehensible position list. Try 0:3,5,7:8 or similar")
        outStr = ""
    return outStr


def lensPosPrint(lp=False):
    outstr = "motors |   y2       z2      phi2   psi2   th2       x2\n"
    outstr += "-------+------------------------------------------------\n"
    outstr += "user   | %8.3f %8.3f %6.3f %6.3f %7.3f %8.2f \n" % (
    y2.position, z2.position, phi2.position, psi2.position, th2.position, x2.position)
    outstr += "dial   | %8.3f %8.3f %6.3f %6.3f %7.3f %8.2f \n" % (
    y2.dial, z2.dial, phi2.dial, psi2.dial, th2.dial, x2.dial)
    print(outstr)
    if lp:
        import os
        cmdStr = "echo -e %s | lp -d lj1bm05"
        os.system(cmdStr)

# RC220617 - deprecated?
def setLensMotors(newLensPosMot=y2, newLensParasiticMot=None, p1=-1, h1=0, p2=1, h2=0):
    pwGlob = ParametersWardrobe('xsvt-globals')
    pwGlob.add('lensPosMot', newLensPosMot.name)
    if newLensParasiticMot != None:
        pwGlob.add('lensParasiticMot', newLensParasiticMot.name)
        pwGlob.add('parasiticSlope', (h2 - h1) / (p2 - p1))
        pwGlob.add('parasiticY0', h1 + parasiticSlope * (0 - p1))
    else:
        pwGlob.add('lensParasiticMot', None)
        pwGlob.add('parasiticSlope', 0)
        pwGlob.add('parasiticY0', 0)
    showLensMotors()


# RC220617 - deprecated?
def showLensMotors():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensPosMot
        pwGlob.lensParasiticMot
        pwGlob.parasiticSlope
        pwGlob.parasiticY0
    except AttributeError:
        print("At least one of")
        print("pwGlob.lensPosMot")
        print("pwGlob.lensParasiticMot")
        print("pwGlob.parasiticSlope")
        print("pwGlob.parasiticY0")
        print("not known yet.")
        print("Use setLensMotors() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt

    print("Translation to change lenses: %s" % pwGlob.lensPosMot)
    if pwGlob.lensParasiticMot != None:
        print("Translation perpendicular to it: %s" % pwGlob.lensParasiticMot)
        if pwGlob.parasiticSlope == 0:
            print("   Moves to fixed height parasiticY0 = %.3f" % pwGlob.parasiticY0)
        else:
            print("   Follows linear curve given by y= %.3f *x + %.3f " % (pwGlob.parasiticSlope, pwGlob.parasiticY0))
            print("   parasiticSlope is %.4f" % pwGlob.parasiticSlope)
            print("   parasiticY0 is %.4f" % pwGlob.parasiticY0)


# ======================================================================================================================
# Membrane(es) functions
# ======================================================================================================================

def setMembraneOut(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 0:
        membraneOutPos = [(y1.name, y1.position)]  # tuple of motor and motor position
    else:
        membraneOutPos = []
        for motor in args:
            membraneOutPos.append((motor.name, motor.position))
    pwGlob.add('membraneOutPos', membraneOutPos)
    showMembraneOut()


def showMembraneOut():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.membraneOutPos
    except AttributeError:
        print("pwGlob.membraneOutPos not known yet.")
        print("Use setMembraneOut() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    print("Membrane Out at:")
    for myMotor in pwGlob.membraneOutPos:
        print("  Motor %s at %.3f" % (myMotor[0], myMotor[1]))


def membraneOut():
    pwGlob = ParametersWardrobe('xsvt-globals')
    showMembraneOut()
    print("Moving membrane out of the beam.")
    for motor in pwGlob.membraneOutPos:
        umv(config.get(motor[0]), motor[1])


# ---------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------
def setMembraneIn(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 0:
        membraneInPos = [(y1.name, y1.position)]  # tuple of motor and motor position
    else:
        membraneInPos = []
        for motor in args:
            membraneInPos.append((motor.name, motor.position))
    pwGlob.add('membraneInPos', membraneInPos)
    showMembraneIn()


def showMembraneIn():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.membraneInPos
    except AttributeError:
        print("pwGlob.membraneInPos not known yet.")
        print("Use setMembraneIn() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    print("Membrane In at:")
    for myMotor in pwGlob.membraneInPos:
        print("  Motor %s at %.3f" % (myMotor[0], myMotor[1]))


def membraneIn():
    pwGlob = ParametersWardrobe('xsvt-globals')
    showMembraneIn()
    print("Moving membrane into the beam.")
    for motor in pwGlob.membraneInPos:
        umv(config.get(motor[0]), motor[1])


# ======================================================================================================================
# detector
# ======================================================================================================================
def detDistort(expTime=3, settleTime=3, mode="both", objective="10x", horizMot=y3, vertMot=z3, suffix=None):
    # mode="detector" varies detector position on a mesh-grid
    # mode="piezo" varies piezo position on a mesh-grid
    # mode="both" does both of the two meshs
    membraneIn()
    lensOut()
    if suffix is None:
        myDir("det_dist_" + objective)
    else:
        myDir("det_dist_" + objective + "_" + suffix)

    myPointerFile("det_dist")

    if mode == "both" or mode == "detector":
        horizPos0 = horizMot.position
        vertPos0 = vertMot.position
        SCAN_SAVING.images_path_template = "det_dist_nfc"
        SCAN_SAVING.images_prefix = "det_dist_nfc_"
        if objective == "20x":
            dmesh(horizMot, -0.025, 0.025, 5, vertMot, -0.025, 0.025, 5, expTime, sleep_time=settleTime)
            stepsmm=10e-3
        else:
            dmesh(horizMot, -0.05, 0.05, 5, vertMot, -0.05, 0.05, 5, expTime, sleep_time=settleTime)
            stepsmm=20e-3
        moveMode="det"
        umv(horizMot, horizPos0, vertMot, vertPos0)
    if mode == "both" or mode == "piezo":
        prepare_piezos()
        SCAN_SAVING.images_path_template = "det_dist_piezo"
        SCAN_SAVING.images_prefix = "det_dist_piezo_"
        if objective == "20x":
            amesh(pzh, 50, 100, 5, pzv, 50, 100, 5, expTime, sleep_time=.1)
            stepsmm=10e-3
        else:
            amesh(pzh, 50, 150, 5, pzv, 50, 150, 5, expTime, sleep_time=.1)
            stepsmm=20e-3
        moveMode="piezo"
    # taking flats
    membraneOut()
    SCAN_SAVING.images_path_template = "flats"
    SCAN_SAVING.images_prefix = "flats_"
    loopscan(10, expTime)
    endSaving()
    beep(10)
    create_detDistortIni(objective,stepsmm,moveMode)

def create_detDistortIni(objective,stepsmm,mode):
    pwGlob = ParametersWardrobe('xsvt-globals')
    outdir="/data/optics/refractive_optics/fromBM5"
    yymmdd=SCAN_SAVING.date[2:8]
    try:
       magnification=str(int(objective))
    except:
       magnification=str(int(objective.rstrip("xX")))
    outFileName="/data/optics/refractive_optics/fromBM5/det_dist_%s_%sx_piezo.ini" % (yymmdd,magnification)

    templateFile=open("/users/blissadm/local/bm05.git/bm05/XSVT/det_dist_YYMMDD_MMx_piezo.ini")
    templateLines=templateFile.readlines()

    outf=open(outFileName,"w")
    for line in templateLines:
        line=line.replace("___ROOTFOLDER___","%s/det_dist_%sx" % (SCAN_SAVING.base_path,magnification))
        line=line.replace("___YYMMDD___","%s" % yymmdd)
        line=line.replace("___MAGNIFICATION___","%s" % magnification)
        line=line.replace("___MOTORSTEPSMM___","%f" % stepsmm)
        line=line.replace("___MODE___","%s" % mode)
        outf.write(line)
    outf.close()
    print("Ini file %s generated" % outFileName)
    pWGlob.add("detDistortName","%s_%sx_%dum_disto" % (mode,magnification,int(stepsmm*1e3+0.5)))

    
def focusScan(magnification, expTime=0.1, pts=51, range_scan=None):

    pwGlob = ParametersWardrobe('xsvt-globals')
    if type(magnification)==int:
        if magnification not in [4,10,20]:
            print("Magnification can only be 4, 10, or 20!")
            print("Please specify")
            return
        else:
            magStr="%dx" % magnification
            magInt=magnification
    elif type(magnification)==str:
        if magnification not in ["4x","10x","20x","4X","10X","20X"]:
            print("Magnification can only be 4x, 10x, or 20x!")
            print("Please specify")
            return
        else:
            magStr="%sx" % magnification.rstrip("xX")
            magInt=int(magnification.rstrip("xX"))
    else:
       print("Magnification can only be 4x, 10x, or 20x!")
       print("Please specify")
       return 
       
    pwGlob.add("magnification", magStr)

    if range_scan is None:
        range_scan = [-10, 10]
    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    plotselect(camera.counters.total_std)
    membraneIn()
    dscan(focus01, range_scan[0], range_scan[1], pts, expTime, camera)
    goto_peak()
    # fine scan
    dscan(focus01, range_scan[0]*0.1, range_scan[1]*0.1, int(pts/2), expTime, camera)
    goto_cen()
    endSaving()
    ct(expTime)
    beep(1)


def changeDetector(detector):

    pwGlob = ParametersWardrobe('xsvt-globals')
    pwGlob.camera = detector

# ======================================================================================================================
# auxiliary functions
# ======================================================================================================================

def pinholeAlign(posstr="-67.5 -60 -52.5 -22.5 -7.5 7.5 22.5 52.5 60 67.5", exptime=.1, repeats=2, angleMotor=phi2,
                  y2opt=True, resetPos=True):
    # angleMotor=psi2
    # angleMotor=phi2
    pwGlob = ParametersWardrobe('xsvt-globals')
    myDir("pinhole_alignment")
    myPointerFile("pinhole_alignment")
    SCAN_SAVING.images_path_template = "pinhole_alignment"
    SCAN_SAVING.images_prefix = "pinhole_alignment_"

    # align phi2 and th2
    plotselect(pwGlob.camera.counters.total_sum)
    positions = posstr.split()
    ctr = 0
    if posstr == "all":
        positions = np.arange(10) * 15 - 4.5 * 15
        positions = np.append(positions, np.arange(10) * 24 - 4.5 * 24)
        positions = np.sort(positions)
    yi = np.zeros(len(positions))
    psii = np.copy(yi)
    thi = np.copy(yi)
    for pos in positions:
        yi[ctr] = float(pos)
        umv(y2, yi[ctr])
        if y2opt:
            targetPos = y2.position
            dscan(y2, -1.5, 1.5, 20, exptime)
            print(fwhm())
            if inside(fwhm(), 1.2, 1.4):
                goto_cen()
                y2.position = targetPos
            else:
                goto_peak()

        if ctr == 0:  # first position has one more repeat
            ct(exptime)
            dscan(angleMotor, -.2, .2, 20, exptime)
            goto_peak()
            ct(exptime)
            dscan(th2, -.2, .2, 20, exptime)
            goto_peak()
        for ii in range(repeats):
            ct(exptime)
            dscan(angleMotor, -.2, .2, 20, exptime)
            try:
                goto_cen()
            except:
                goto_peak()
            ct(exptime)
            dscan(th2, -.2, .2, 20, exptime)
            try:
                goto_cen()
            except:
                goto_peak()
        yi[ctr] = y2.position
        thi[ctr] = th2.position
        psii[ctr] = angleMotor.position
        ctr += 1
    print("Results:")
    print("y2 positions")
    print(yi)
    print("th2 positions")
    print(thi)
    print("%s positions" % angleMotor.name)
    print(psii)
    print("")
    print("Average:")
    print("th2 mean:")
    print(np.mean(thi))
    print("%s mean" % angleMotor.name)
    print(np.mean(psii))

    umv(th2, np.mean(thi))
    print("!!! Moved to average best theta angle %.3f !!!" % np.mean(thi))
    if np.std(thi) < 0.05 and resetPos:
        th2.position = 0
        print("!!! SET THETA ANGLE BACK TO zero/ZERO/0 !!!")

    umv(angleMotor, np.mean(psii))
    print("!!! Moved to average best %s angle %.3f !!!" % (angleMotor.name, np.mean(psii)))
    if np.std(psii) < 0.05 and resetPos:
        angleMotor.position = 0
        print("!!! SET %s ANGLE BACK TO zero/ZERO/0 !!!" % angleMotor.name)
    beep(10)

    fig, ax = pylab.subplots()
    ax.plot(yi, psii, "--ro")
    ax.set_xlabel("y2-position [mm]")
    ax.set_ylabel(("best %s [deg]" % angleMotor.name), color="red")
    ax2 = ax.twinx()
    ax2.plot(yi, thi, "--bo")
    ax2.set_ylabel("best th2 [deg]", color="blue")
    timestamp = "%sh%smin" % (time.localtime().tm_hour, time.localtime().tm_min)
    rootfolder = pwGlob.rootDir
    pylab.savefig("%s/pinhole_alignment/pinhole_alignment_%s.png" % (rootfolder, timestamp))
    pylab.show()

    endSaving()


def dist(*args):
    if len(args) == 1:
        target = args[0]
        print("Lens to detector distance was %.1f mm" % (x3.position - x2.position))
        print("Moving detector...")
        umv(x3, x2.position + target)

    elif len(args) == 2:
        print("Membrane to lens distance was %.1f mm" % (x2.position))
        print("Lens to detector distance was %.1f mm" % (x3.position - x2.position))
        print("Moving membrane and detector...")
        umv(x2, args[0], x3, args[0] + args[1])

    print(" Membrane to lens distance is %6.1f mm" % (x2.position))
    print(" Lens to detector distance is %6.1f mm" % (x3.position - x2.position))
    print("(Membrane to detector dist is %6.1f mm)" % (x3.position))


def bm5Distances():
    print("Still a syntax error to get removed...")
    x1tom = 20
    cx1 = 43000
    myx2 = x2.position
    myx2min = x2.low_limit
    myx3 = x3.position
    myx3min = x3.low_limit
    myx3max = x3.high_limit
    cx1ToLens = (cx1 + x1tom + myx2)

    print("Useful distances at BM5:")
    print("                distance-to-source    distance-to-lens     ")
    print("source                     0                %6.2f m " % ((0 - (cx1 + cx1ToLens)) * 1e-3))
    print("primary slits vert.       26.22 m           %6.2f m " % ((26220 - (cx1 + cx1ToLens)) * 1e-3))
    print("primary slits horiz.      26.32 m           %6.2f m " % ((26320 - (cx1 + cx1ToLens)) * 1e-3))
    print("DCM                       29.70 m           %6.2f m " % ((29700 - (cx1 + cx1ToLens)) * 1e-3))
    print("secondary slits vert.     35.85 m           %6.2f m " % ((35850 - (cx1 + cx1ToLens)) * 1e-3))
    print("secondary slits horiz.    36.09 m           %6.2f m " % ((36090 - (cx1 + cx1ToLens)) * 1e-3))
    print("end of UHV beampipe EH1    -.-  m           %6.2f m " % (-99.99))
    print("triple axis granite start  -.-  m           %6.2f m " % (-99.99))
    print("center x1 tower           %5.2f m           %6.2f m " % (cx1 * 1e-3, -1e-3 * cx1ToLens))
    print("membrane                  %5.2f m           %6.2f m " % ((cx1 + x1tom) * 1e-3, myx2 * 1e-3))
    print("sample @ x2_min           %5.2f m               0 m " % ((cx1 + x1tom + myx2min) * 1e-3))
    print("sample                    %5.2f m               0 m " % ((cx1 + x1tom + myx2) * 1e-3))
    print("detector @ x3_min         %5.2f m               ---" % ((cx1 + x1tom + myx3min) * 1e-3))
    print("detector                  %5.2f m           %6.2f m " % ((cx1 + x1tom + myx3) * 1e-3, (myx3 - myx2) * 1e-3))
    print("detector @ x3_max         %5.2f m           %6.2f m " % (
    (cx1 + x1tom + myx3max) * 1e-3, (myx3max - myx2min) * 1e-3))
    print("triple axis granite end   45.18 m           %6.2f m " % ((45180 - (cx1 + x1tom + myx3)) * 1e-3))

def startUpHelp():
    print("Going to monochromatic beam:")
    print("Close the FE.")
    print("open OH bliss session and type:  move_EH1(\"mono\")   ")
    print("open OH bliss session and type:  move_mono(\"mono\")  ")
    print("connect pco edge ")
    print("pco edge trigger: on electrical racks, connect caple 1760 of patch panel to OPIOM2 output O4")
    print("via Lemo cable")
    print("in XSVT bliss session:    activate_pco() ")


# ======================================================================================================================
# other useful functions
# ======================================================================================================================

def beep(repeats=1):
    for ii in range(repeats * 6):
        print("\a", end="\r")
        sleep(.05)

        
def deccelerate_y2Axis():
    """ helps controlling loss of steps in y2 - BM05 known issue RC220506 """
    y2.velocity = 4
    y2.acctime = 0.5
    y2.backlash = 1

    
def attenuatorsOut():
    if outside(att1.position, -1., 1.) or outside(att2.position, -1., 1.) or outside(att3.position, -1., 1.):
        print("Removing attenuators!!!")
        reopen = False
        if fe.is_open:
            fe.close()
            reopen = True
        umv(att1, 0)
        umv(att2, 0)
        umv(att3, 0)
        if reopen:
            fe.open()

            
def laserIn():
    try:
        ibt.laser()
    except:
        umv(yinsert, 70)
        print("Laser is in the beam!")


def laserOut():
    try:
        ibt.free()
    except:
        umv(yinsert, -10)
        print("Laser is not in the beam!")


def diodeIn():
    umv(diode1,-30)
    """
    try:
        ibt.diode()
    except:
        umv(yinsert, 95)
    """   
    print("Diode is in the beam!")
   

def diodeOut():
    umv(diode1,0)
    """
    try:
        ibt.free()
    except:
        umv(yinsert, -10)
    """    
    print("Diode is not in the beam!")


def beam():
    if fe.is_closed:
        fe.open()
        print("Front End status is ", fe.state)
    elif fe.is_open:
        print("Front End is open")
    else:
        print("CHECK FRONT END STATUS")
    laserOut()
    diodeOut()
    # DISABLE / CLOSED / OPEN
    shstate = str(eh1.state).split(".")[-1]
    print(shstate)
    if shstate == "OPEN":
        print("Shutter for eh1 is already open")
        return
    if shstate in ["DISABLED", "DISABLE"]:
        print("Currently disabled. Waiting for end of search ...")
        while shstate == "DISABLED":
            sleep(.1)
            shstate = str(eh1.state).split(".")[-1]

    print("Trying to open the shutter...")
    ctr = 0
    while shstate != "OPEN":
        try:
            eh1.open(quiet=True)
        except:
            eh1.open()
            print("edit /users/blissadmin/local/bliss.git/bliss/controllers/tango_shutter.py !!")
        if (ctr > 2):
            sleep(.25)
            shstate = str(eh1.state).split(".")[-1]
        else:
            shstate = str(eh1.state).split(".")[-1]
        ctr += 1

    print("Shutter open.")
    ct(.1)
    

def wss():
    print("secondary slits gap: %.2f x %.2f at offset %.1f / %.1f" % (
    sshg.position, ssvg.position, ssho.position, ssvo.position))

def wps():
    print("primary slits gap: %.2f x %.2f at offset %.1f / %.1f" % (
    pshg.position, psvg.position, psho.position, psvo.position))


def ss(*args):
    if len(args) == 0:
        wss()
    elif len(args) == 1:
        gapsize = args[0]
        if (gapsize > -.1) and (gapsize < 10):
            umv(sshg, gapsize, ssvg, gapsize)
        else:
            print("Resonable gapsize between -.1 and 10")
    elif len(args) == 2:
        hgap = args[0]
        vgap = args[1]
        if (hgap > -.1) and (hgap < 10) and (vgap > -.1) and (vgap < 10):
            umv(sshg, hgap, ssvg, vgap)
        else:
            print("Resonable gapsize between -.1 and 10")
    else:
        print("Usage is:  ss(<gapSize>) or ss(<horizGap>,<vertGap>) ")
        print("           in order to get a square slit or horiz/vert slit")
        print("     e.g.  ss(2) or ss(2,1) in order to get a square slit or 2x1 horiz/vert slit")


def pls(*args):
    if len(args)>1:
        plotselect(*args)
    if len(args)==1:
        if args[0]=="pico1":
           plotselect(pico1)
        elif args[0]=="pico3":
           plotselect(pico3)
        elif args[0]=="total":
           plotselect(pcogold.counters.total_avg)
        elif args[0]=="std":
           plotselect(pcogold.counters.total_std)
        elif args[0]=="center":
           plotselect(pcogold.counters.center_avg)
        else:
           plotselect(args[0])
    if len(args)==0:
       plotselect(pcogold.counters.total_avg)

def plotpico1():
    plotselect(pico1)
def plot1():
    plotselect(pico1)
def plotpico():
    plotselect(pico1)
def plotpico3():
    plotselect(pico3)
def plot3():
    plotselect(pico3)
def plotpco():
    plotselect(pcogold.counters.total_avg)
def plotavg():
    plotselect(pcogold.counters.total_avg)
def plottot():
    plotselect(pcogold.counters.total_avg)
def plotstd():
    plotselect(pcogold.counters.total_std)
    

        
# ======================================================================================================================
# potpourri functions
# ======================================================================================================================

def inside(myvalue, lowlim, highlim):
    if (myvalue >= lowlim) and (myvalue <= highlim):
        return True
    else:
        return False


def outside(myvalue, lowlim, highlim):
    if (myvalue < lowlim) or (myvalue > highlim):
        return True
    else:
        return False


# ======================================================================================================================
# piezos
# ======================================================================================================================

def piInfo():
    print("PI E-727 mac address:   01:d8:80:39:d8:48:04")
    print("IP address on BM5:      160.103.125.175")
    print("alias:                  e727pitroth")


def prepare_piezos():
    print('Waking up piezos...', end='')
    sync()
    pzv.enable()
    sleep(0.2)
    pzh.enable()
    sleep(0.2)

    pzv.sync_hard()
    while 'READY' not in pzv.state:
        pzv.sync_hard()
    sleep(0.5)
    pzh.sync_hard()
    while 'READY' not in pzh.state:
        pzh.sync_hard()

    sleep(0.5)
    umv(pzv, 20)
    umv(pzh, 20)
    sleep(0.5)

    print('They are awake!')


# ======================================================================================================================
# directories, file names, saving and co.
# ======================================================================================================================

def check_readiness():

    hasSubFolder = False
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensSubfolder
        hasSubFolder = True
    except AttributeError:
        print("No subfolder nor prefix defined yet.")
        print("Please define them by using xsvt.setLensSet()")

    return hasSubFolder


def getRootDir(trDir="/data/bm05/inhouse/xog-refropt", nameExt="lenses"):
    now = time.localtime()
    year, month, day = map(int, time.strftime("%Y %m %d").split())
    yesterday = float(time.time()) - 60 * 60 * 24
    yesterdayMonth = time.localtime(yesterday)[1]
    yesterdayDay = time.localtime(yesterday)[2]
    todayDir = "%02d%02d%02d%s" % (year - 2000, month, day, nameExt)
    yesterdayDir = "%02d%02d%02d%s" % (year - 2000, yesterdayMonth, yesterdayDay, nameExt)
    rootDir = trDir + "/" + todayDir
    if os.path.isdir(rootDir) == 1:
        print("%s directory exists. We use it." % rootDir)
    else:
        alternativeDir = trDir + "/" + yesterdayDir
        if os.path.isdir(alternativeDir) == 1:
            print("%s directory (yesterday) exists. We use it." % alternativeDir)
            rootDir = alternativeDir
        else:
            print("Neither %s\n    nor %s exist. Create folder for today." % (rootDir, alternativeDir))
            try:
                os.makedirs(rootDir)
            except:
                rootDir = "/data/bm05/inhouse/xog-refropt/"
                print("Problem creating directory %s" % rootDir)
                print("Check permissions.")
                print("###################################")
                print("###################################")
                print("###  USE %s !!!" % rootDir)
                print("###################################")
                print("###################################")
    return rootDir


def myDir(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 1:
        SCAN_SAVING.template = args[0]
    if Nargs > 1:
        print("No or one arguments only allowed!")
    print("currently saving to folder %s " % SCAN_SAVING.template)
    try:
        print("(current lens set path is  %s )" % pwGlob.lensSubfolder)
    except:
        pass


def myPointerFile(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 1:
        SCAN_SAVING.data_filename = args[0]
    if Nargs > 1:
        print("No or one arguments only allowed!")
    print("currently h5 pointer file %s.h5 " % SCAN_SAVING.data_filename)


# TODO: check if used; if not, suppress RC220616
def myScan(*args):
    pwGlob = ParametersWardrobe('xsvt-globals')
    Nargs = len(args)
    if Nargs == 1:
        SCAN_SAVING.images_path_template = ("scans")
        SCAN_SAVING.images_prefix = ("scan_")
    if Nargs > 1:
        print("No or one arguments only allowed!")
    print("currently saving scans to folder %s " % SCAN_SAVING.images_path_template)
    print("currently saving scans to folder %s " % SCAN_SAVING.images_prefix)


def startSaving(h5dir, imagesubdir, imageprefix):
    myDir(h5dir)  # name of folder where *.h5 file goes
    myPointerFile(h5dir)  # name of *.h5 file
    SCAN_SAVING.images_path_template = imagesubdir
    SCAN_SAVING.images_prefix = imageprefix + "_"




def endSaving():
    myDir("align")
    myPointerFile("align")
    SCAN_SAVING.images_path_template = ("align_scans")
    SCAN_SAVING.images_prefix = ("align_scan_")

# ======================================================================================================================
# .ini_file for speckle tracking
# ======================================================================================================================

def write_ini_file(method, outFolder, lensTag, material, radius="NN", step=1):
    pwGlob = ParametersWardrobe('xsvt-globals')
    rootDir=pwGlob.rootDir

    
    #sample ini file:
    inFile=open("/users/blissadm/local/bm05.git/bm05/XSVT/experiment.ini","r")
    inLines=inFile.readlines()
    inFile.close()

    #new ini file to be created
    dateStr=rootDir.rstrip("/").split("/")[-1].split("lenses")[-1]
    iniFileOutDir=rootDir+"/"+pwGlob.lensSubfolder+"/inis"
    os.makedirs(iniFileOutDir,exist_ok=True)
    outFileName="%s_%s_%s.ini" % (material,lensTag,dateStr)
    outFile=os.path.join(iniFileOutDir,outFileName)

    #objectiveLens,pixelSize
    magStr=pwGlob.magnification
    if magStr=="4x":
        pixSize=1.625
    elif magStr=="10x":
        pixSize=0.65
    elif magStr=="20x":
        pixSize=0.325
    else:
        pixSize="___pixsize___"
    
    if pwGlob.lensSupport=="2D": lensShape="c"
    else: lensShape="r"

    hor_dist_map="/data/optics/refractive_optics/wft_mtrl/_det_dist/det_dist_"+dateStr+"/data/nfc_"+magStr+"_20um_disto_h.tif"
    ver_dist_map=hor_dist_map.replace("_h.tif","_v.tif")

    #membrane type guess
    E= bm05mono.bragg2energy(mono.position)
    if E<25 and magStr=="10x":
        membrane="acetate cellulose?"
    elif magStr=="20x":
        membrane="acetate cellulose?"
    elif magStr=="4x":
        membrane=="3x grit 1200?"
    else:
        membrane="___membrane___"
        
    lastSpeckle=len(glob.glob(rootDir+"/"+pwGlob.lensSubfolder+"/"+lensTag+"/"+lensTag+"_00*.h5"))-1
    prfx_sample=lensTag+"_speckles_00%02d.h5" % lastSpeckle

    lastRadio=len(glob.glob(rootDir+"/"+pwGlob.lensSubfolder+"/radios/"+lensTag+"_radios_00*.h5"))-1
    prfx_radio=lensTag+"_radios_00%02d.h5" % lastRadio
    
    lastRefStart=len(glob.glob(rootDir+"/"+pwGlob.lensSubfolder+"/refs/refStart_00*.h5"))-1
    prfx_ref="refStart_00%02d.h5" % lastRefStart
    
    lastFlat=len(glob.glob(rootDir+"/"+pwGlob.lensSubfolder+"/flats/flatStart_00*.h5"))-1
    prfx_flat="flatStart_00%02d.h5" % lastFlat
    
    lastDark=len(glob.glob(rootDir+"/"+pwGlob.lensSubfolder+"/darks/darkStart_00*.h5"))-1
    prfx_dark="darkStart_00%02d.h5" % lastDark

    dirOut="/data/optics/refractive_optics/wft_mtrl/_x-ray_lenses/%s_%s_%sum/%s_%s/" % (pwGlob.lensSupport,material,radius,lensTag,dateStr)
    prfx_out=lensTag
    
    #new ini file to be created
    outf=open(outFile,"w")
    for line in inLines:
        print(line)
        #REPLACE SOME STUFF
        if "___method___"               in line: line=line.replace("___method___",method)
        if "___dist___"                 in line: line=line.replace("___dist___","%.2f" % (x3.position-x2.position))
        if "___pixsize___"              in line: line=line.replace("___pixsize___","%.3f" % pixSize)
        if "___step___"                 in line: line=line.replace("___step___","%d" % step)
        if "___root_fldr___"            in line: line=line.replace("___root_fldr___",rootDir)
        if "___dir_sample___"           in line: line=line.replace("___dir_sample___",lensTag)
        if "___dir_ref___"              in line: line=line.replace("___dir_ref___","/refs/")
        #if "___dir_sample_bis___"       in line: line=line.replace("___dir_sample_bis___",)
        #if "___dir_ref_bis___"          in line: line=line.replace("___dir_ref_bis___",)
        if "___dir_radio___"            in line: line=line.replace("___dir_radio___","/radios/")
        if "___dir_flat___"             in line: line=line.replace("___dir_flat___","/flats/")
        if "___dir_dark___"             in line: line=line.replace("___dir_dark___","/darks/")
        if "___prfx_sample___"          in line: line=line.replace("___prfx_sample___",prfx_sample)
        if "___prfx_ref___"             in line: line=line.replace("___prfx_ref___",prfx_ref)
        #if "___prfx_sample_bis___"      in line: line=line.replace("___prfx_sample_bis___",)
        #if "___prfx_ref_bis___"         in line: line=line.replace("___prfx_ref_bis___",)
        if "___prfx_radio___"           in line: line=line.replace("___prfx_radio___",prfx_radio)
        if "___prfx_flat___"            in line: line=line.replace("___prfx_flat___",prfx_flat)
        if "___prfx_dark___"            in line: line=line.replace("___prfx_dark___",prfx_dark)
        if "___roi___"                  in line: line=line.replace("___roi___","50 1998 50 1998")
        if "___roidd___"                in line: line=line.replace("___roidd___","50 1998 50 1998")
        if "___hor_dist_map___"         in line: line=line.replace("___hor_dist_map___",hor_dist_map)
        if "___ver_dist_map___"         in line: line=line.replace("___ver_dist_map___",ver_dist_map)
        if "___energy___"               in line: line=line.replace("___energy___","%.3f" % bm05mono.bragg2energy(mono.position))
        if "___material___"             in line: line=line.replace("___material___",material)
        if "___lens_focusing___"        in line: line=line.replace("___lens_focusing___",pwGlob.lensSupport.rstrip("D"))
        if "___lens_ap_dim___"          in line: line=line.replace("___lens_ap_dim___",lensShape)
        if "___root_fldr2___"           in line: line=line.replace("___root_fldr2___",dirOut+"/data/")
        if "___dir_out___"              in line: line=line.replace("___dir_out___",dirOut)
        if "___prfx_out___"             in line: line=line.replace("___prfx_out___",prfx_out)
        if "___radiotif___"             in line: line=line.replace("___radiotif___",lensTag+"_radio.tif")
        if "___correlationtif___"       in line: line=line.replace("___correlationtif___",lensTag+"_corr.tif")
        if "___hor_gradtif___"          in line: line=line.replace("___hor_gradtif___",lensTag+"_grad_h.tif")
        if "___ver_gradtif___"          in line: line=line.replace("___ver_gradtif___",lensTag+"_grad_v.tif")
        if "___membrane___"             in line: line=line.replace("___membrane___",membrane)
        if "___det___"                  in line: line=line.replace("___det___","%s" % pwGlob.camera.name)
        if "___mag___"                  in line: line=line.replace("___mag___",magStr)
        if "___dist_source2membrane___" in line: line=line.replace("___dist_source2membrane___","%.1f" % 43000)
        if "___dist_membrane2sample___" in line: line=line.replace("___dist_membrane2sample___","%.1f" % x2.position)
        if "___dist_sample2detector___" in line: line=line.replace("___dist_sample2detector___","%.1f" % (x3.position-x2.position))
        if "___exp_date___" in line: line=line.replace("___exp_date___",time.strftime("%d %b %Y",time.localtime(time.time())))
        
        outf.write(line)
    outf.close()
        
        
# ======================================================================================================================
# global variables and wardrobe auxiliary functions
# ======================================================================================================================

def initGlobals():
    pwGlob = ParametersWardrobe('xsvt-globals')
    # This gets from Redis a set of variables called xsvt-globals.
    # There might be many defined, there might be none.
    rootDir = getRootDir("/data/bm05/inhouse/xog-refropt", "lenses")
    pwGlob.add('rootDir', rootDir)
    try:
        print("Camera I hope to use is: ", pwGlob.camera.name)
    except:
        pwGlob.add('camera', pcogold)
        # pwGlob.add('camera',edgehs)
        print("Camera I will try to use is: ", pwGlob.camera.name)


def deleteGlobals(keepDet=True):
    pwGlob = ParametersWardrobe('xsvt-globals')
    if keepDet:
        oldDet = pwGlob.camera
    pwGlob.purge()
    if keepDet:
        pwGlob.add('camera', oldDet)


def show_exp_info():
    showGlobals()

def showGlobals():
    missing = 0
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        print("detector:   ", pwGlob.camera.name)
    except:
        print("WHAT CAMERA DO WE USE?")
        missing += 1
    try:
        print("Membrane out:   ", pwGlob.membraneOutPos)
    except:
        missing += 1
    try:
        print("Membrane in:    ", pwGlob.membraneInPos)
    except:
        missing += 1
    try:
        print("Lens out:       ", pwGlob.lensOutPos)
    except:
        missing += 1
    try:
        print("Lens in:        ", pwGlob.lensInPos)
    except:
        missing += 1
    try:
        print("Type of lens support:", pwGlob.lensSupport)
    except:
        missing += 1
    try:
        print("Lens tags:    ", pwGlob.lensTagStr)
    except:
        missing += 1
    try:
        print("Lens positions:    ", pwGlob.lensPosStr)
    except:
        missing += 1
    try:
        print("Lens material:    ", pwGlob.materialStr)
    except:
        missing += 1
    try:
        print("Subfolder:         ", pwGlob.lensSubfolder)
    except:
        missing += 1
    try:
        print("Energy: ", pwGlob.energy)
    except:
        missing += 1


def required():
    missing = 0
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.membraneOutPos
    except AttributeError:
        missing += 1
        print("pwGlob.membraneOutPos not known yet.")
        print("   Move membrane out of the beam and type 'setMembraneOut()' !")
    try:
        pwGlob.membraneInPos
    except AttributeError:
        missing += 1
        print("pwGlob.membraneInPos not known yet.")
        print("   Move membrane into the beam and type 'setMembraneIn()' !")
    try:
        pwGlob.lensOutPos
    except AttributeError:
        missing += 1
        print("pwGlob.lensOutPos not known yet.")
        print("   Move lens out of the beam and type 'setLensOut()' !")
    try:
        pwGlob.lensInPos
    except AttributeError:
        missing += 1
        print("pwGlob.lensInPos not known yet.")
        print("   Move lens into the beam and type 'setLensIn()' !")

    definition = True
    try:
        pwGlob.lenstagstr
    except AttributeError:
        missing += 1
        definition = False
        print("pwGlob.lenstagstr not known yet.")
    try:
        pwGlob.lensposstr
    except AttributeError:
        missing += 1
        definition = False
        print("pwGlob.lensposstr not known yet.")
    try:
        pwGlob.support
    except AttributeError:
        missing += 1
        definition = False
        print("pwGlob.support plate type not known yet.")
    try:
        pwGlob.subfolder
    except AttributeError:
        missing += 1
        definition = False
        print("pwGlob.subfolder plate type not known yet.")
    if definition == False:
        print("   Define  setLensSet(newDir,newTagStr,newPosStr='0 1 2 3 4 5 6 7 8 9',plateType='2D')' !")
    try:
        pwGlob.membraneLensDist
    except AttributeError:
        missing += 1
        print("pwGlob.membraneLensDist not known yet.")
        print("   type 'setMembraneLensDist(<mm>)' !")
    try:
        pwGlob.lensDetectorDist
    except AttributeError:
        missing += 1
        print("pwGlob.lensDetectorDist not known yet.")
        print("   type 'setLensDetectorDist(<mm>)' !")
    try:
        pwGlob.energy
    except AttributeError:
        missing += 1
        print("pwGlob.energy not known yet.")
        print("   type 'setEnergy(<keV>)' !")
    try:
        pwGlob.material
    except AttributeError:
        missing += 1
        print("pwGlob.material not known yet.")
        print("   type 'setMaterial(Al/C/diamond/Be/SU8/other)' !")

    return missing


# ======================================================================================================================
# help
# ======================================================================================================================

def help():
    print("Available commands")
    os.system("cat /data/bm05/inhouse/xog-refropt/macros/ST_BM5.py | grep '^def '")
    print("Global variables")
    show_exp_info()
    print("required global variables")
    required()


def helpPco():
    print("open a terminal on loka or lbm05loka.")
    print("type there \'rd_pcousb\' to connect remotely to the rackable PC running the device server.")
    print("double click on the \'cond1.9pcousb3\' icon on the Desktop.")
    print("close the remote desktop window with a mouse click")
    print("")
    print("inside the xsvt bliss session, type the following:")
    print("  activate_pcoEdge()")
    print("  umvactivate_hard_triggering()")

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# Messy codes - add stuff here!!!

def specklesSeries(exptime=3, subset="all", startRefs=True, endRefs=True, scanType="vectscan", n_rep=10):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    lensposstr = pwGlob.lensPosStr
    lenstagstr = pwGlob.lensTagStr
    materialstr= pwGlob.materialStr
    radiusStr = pwGlob.radiusStr
    radiusarr = radiusStr.split()
    lenstagarr = lenstagstr.split()
    materialarr= materialstr.split()
    membraneOut()

    if startRefs:
        print(scanType)
        xsvtRefs(exptime, "refs", "refStart", scanType=scanType)

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder

    darks(exptime, "darks", "darks", n_rep)
    flats(exptime, "flats", "flats", n_rep)
    radioSeries(exptime=exptime, repeats=n_rep,  subset=subset, makeDarks=False, makeFlats=False)

    SCAN_SAVING.template = pwGlob.lensSubfolder
    SCAN_SAVING.data_filename = pwGlob.lensSubfolder
            
    if endRefs:
        xsvtRefs(exptime, "refs", "refEnd", scanType=scanType)

    endSaving()
    beep(10)


'''
def do_multi_distance(distList, lensfolder, lenstag, keepLens=False):

    xsvtMultiDist(distList, 'XSVT_multidist', lenstag=None, exptime=2, abs_dist=True, postAlign=True, scanType="both",
                  n_rep=10, settleTime=3, keepLens)

    xss1DMultiDist(distList, 'XSS_2x1D_multidist', lenstag=None, Nsteps=150, dx=0.5, exptime=2, abs_dist=True,
                   postAlign=True, scanType="both", n_rep=10, settleTime=3, keepLens)

    xss2DMultiDist(distList, 'XSS_2D_multidist', lenstag=None, Nsteps=35, dx=1., exptime=2, abs_dist=True,
                   postAlign=True, n_rep=10, settleTime=3,keepLens)


def caustics(lensfolder, lenstag, x0, xf, z0, zf, exptime=3, n_rep=10, beeping=True):

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder


    if startRefs:
        membraneOut()
        lensOut()
        darks(exptime, "darks", "darkStart", n_rep)
        flats(exptime, "flats", "flatStart", n_rep)
        xsvtRefs(exptime, "refs", "refStart", scanType=scanType)

    membraneIn()
    lensIn()

    SCAN_SAVING.images_path_template = ("%s" % lenstag)
    SCAN_SAVING.images_prefix = ("%s_speckles_" % lenstag)

    scan_membrane_2D(exptime=exptime, scanType=scanType)

    if endRefs:
        xsvtRefs(exptime, "refs", "refEnd", scanType=scanType)
        flats(exptime, "flats", "flatEnd", n_rep)
        darks(exptime, "darks", "darkEnd", n_rep)

    if makeRadios:
        lensIn()
        radios(exptime, "radios", "radios", n_rep)

    endSaving()
    if beeping:
        beep(10)
'''

def gaussianFlat(x, area, center, sigma, bkg):
    _SQRT_2_PI = np.sqrt(2 * np.pi)
    """Returns (a / (sqrt(2 * pi) * s)) * exp(- 0.5 * ((x - c) / s)^2)
    :param np.ndarray x: values for which the gaussian must be computed
    :param float area: area under curve ( amplitude * s * sqrt(2 * pi) )
    :param float center:
    :param float sigma:
    :rtype: np.ndarray
    """
    return (area / (_SQRT_2_PI * sigma)) * \
            np.exp(-0.5 * ((x - center) / sigma) ** 2) + bkg 


# ------------------------------------------------------------ Energy edge plate
def setEdgeSet(materialStr, tagStr, energyStr, thicknessStr=None, newPosStr="0 1 2 3 4 5 6 7 8 9", plateType="2D"):

    pwGlob = ParametersWardrobe('xsvt-globals')
    if materialStr is None:
        print("specify elements!!!")
        return
    if energyStr is None:
        print("specify positions of energy in keV!")        
        return    
    if tagStr is None:
        print("specify K, L3 or other edge type for each material!")
        return

    if len(materialStr.split()) != len(tagStr.split()) or len(materialStr.split()) != len(energyStr.split()):
            print("Not the same number of element, edge, and energy tags!")
            print("Please redo setEdgeSet() !")
            return
    if "-" in newPosStr or ":" in newPosStr or "," in newPosStr:
        newPosStr = generatePosStr(newPosStr)
    if newPosStr == "":
        print("empty newPosStr is not allowed.")
        print("Please redo setEdgeSet() !")
        return
    if plateType not in ["2D", "1D", "pinholes"]:
        print("Known plate types only 1D, 2D, and pinholes")
        print("Please redo setEdgeSet() !")
        return
    if len(newPosStr.split()) != len(materialStr.split()):
        print("Not the same number of lens tags as there are lens positions")
        print("Please redo setedgeSet() !")
        return
    if thicknessStr!=None:
        if len(thicknessStr.split()) != len(materialStr.split()):
           print("if you define different thickness, do it for all %d foils." % len(materialStr.split()) )
           return
            
    pwGlob.add('edgeSupport', plateType)
    pwGlob.add('edgeSubfolder', "energySeries")
    pwGlob.add('edgePosStr', newPosStr)
    pwGlob.add('edgeTagStr', tagStr)
    pwGlob.add('edgeElementStr', materialStr)
    pwGlob.add('edgeEnergyStr', energyStr)
    if thicknessStr!=None:
        pwGlob.add('edgeThicknessStr',thicknessStr)
    showEdgeSet()


def showEdgeSet():
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.edgeTagStr
        pwGlob.edgePosStr
        pwGlob.edgeSupport
        pwGlob.edgeSubfolder
        pwGlob.edgeElementStr
        pwGlob.edgeEnergyStr
    except AttributeError:
        print("At least one of")
        print("pwGlob.edgeEnergyStr")
        print("pwGlob.edgeElementStr")
        print("pwGlob.edgeTagStr")
        print("pwGlob.edgePosStr")
        print("pwGlob.edgeSupport")
        print("pwGlob.edgeSubfolder")
        print("not known yet.")
        print("Use setEdgeSet() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt

    posLen=len(pwGlob.edgePosStr.split())
    tagLen=len(pwGlob.edgeTagStr.split())
    elementLen=len(pwGlob.edgeElementStr.split())
    energyLen=len(pwGlob.edgeEnergyStr.split())
    maxLen=max(posLen,tagLen,elementLen,energyLen)
    try:
        thicknessStr=pwGlob.edgeThicknessStr
    except:
        thicknessStr=""
        for ii in range(elementLen):
           #thicknessStr=" -".append()
           thicknessStr+=" -"
           
    print("Currently defined absorber plate:")
    print("   subfolder, i.e. global tag : %s" % pwGlob.edgeSubfolder)
    print("   support type:                %s" % pwGlob.edgeSupport)
    print("   position | edgeEnergy | material   | edge-type   | thickness ")
    print("   ---------+------------+------------+-------------+-----------")
    someError=False
    for ii in range(maxLen):
        try:
            print("   %8d |" % int(pwGlob.edgePosStr.split()[ii]), end="")
        except:
            print("            |",end="")
            someError=True
        try:
            print(" %17s |" %  pwGlob.edgeEnergyStr.split()[ii], end="")
        except:
            print("                   |",end="")
            someError=True
        try:    
            print(" %10s |" %  pwGlob.edgeElementStr.split()[ii], end="")
        except:
            print("            |", end="")
            someError=True
        try:
            print(" %8s " % str(pwGlob.edgeTagStr.split()[ii]), end="")
        except:
            print("      ", end="")
            if pwGlob.edgeTagStr!="":
               someError=True
        try:
            print(" %8s " % str(pwGlob.edgeThicknessStr.split()[ii]), end="")
        except:
            print("      ", end="")
            try:
                pwGlob.edgeThicknessStr
                someError=True
            except:
                pass
        print(" ")
    if someError:
        print("ATTENTION: different number of defined positions, tags, elements and energies !!!!!")
        print("Please redo setEdgeSet() !")

        
def edgeNin(nr, move=True, offset=0):
    pwGlob = ParametersWardrobe('xsvt-globals')
    try:
        pwGlob.lensPosMot
        pwGlob.lensParasiticMot
        pwGlob.parasiticSlope
        pwGlob.parasiticY0
    except AttributeError:
        print("At least one of")
        print("pwGlob.lensPosMot")
        print("pwGlob.lensParasiticMot")
        print("pwGlob.parasiticSlope")
        print("pwGlob.parasiticY0")
        print("not known yet.")
        print("Use setEdgeMotors() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt
    try:
        pwGlob.edgeElementStr
        pwGlob.edgeEnergyStr
        pwGlob.edgeTagStr
        pwGlob.edgePosStr
        pwGlob.edgeSupport
        pwGlob.edgeSubfolder
    except AttributeError:
        print("At least one of")
        print("pwHGlob.edgeEnergyStr")
        print("pwHGlob.edgeElementStr")
        print("pwGlob.edgeTagStr")
        print("pwGlob.edgePosStr")
        print("pwGlob.edgeSupport")
        print("pwGlob.edgeSubfolder")
        print("not known yet.")
        print("Use setEdgeSet() first!")
        stopCode = 1
    else:
        stopCode = 0
    if stopCode:
        raise KeyboardInterrupt

    if pwGlob.edgeSupport == "2D":
        spacing = 15
        pos0 = -4.5 * spacing
    elif pwGlob.edgeSupport == "1D":
        spacing = 24
        pos0 = -4.5 * spacing
    elif pwGlob.edgeSupport == "pinholes":
        spacing = -1
        pos0 = -4.5 * 24
    else:
        print("Unknown support plate of type %s" % pwGlob.edgeSupport)
        print("Known are only 1D, 2D and pinholes")
        raise KeyboardInterrupt
    if pwGlob.edgeSupport == "pinholes":
        positions = np.arange(10) * 15 - 4.5 * 15 + offset
        positions = np.append(positions, np.arange(10) * 24 - 4.5 * 24 + offset)
        positions = np.sort(positions)
    else:
        positions = np.arange(10) * spacing + pos0 + offset
    if move:
        umv(config.get(pwGlob.lensPosMot), positions[nr])
    else:
        print("umv(%s,%.4f)" % (pwGlob.lensPosMot, positions[nr]))
    if pwGlob.lensParasiticMot != None:
        if move:
            umv(config.get(pwGlob.lensParasiticMot), pwGlob.parasiticSlope * positions[nr] + pwGlob.parasiticY0)
        else:
            print("umv(%s,%.4f)" % (pwGlob.lensParasiticMot, pwGlob.parasiticSlope * positions[nr] + pwGlob.parasiticY0))
    if move:
        print("Moved in absorption edge foil on position %d" % nr)
        arrayElement = getIndex(pwGlob.edgePosStr, nr)
        print("called %s-%s edge at %s keV" % (pwGlob.edgeElementStr.split()[arrayElement], pwGlob.edgeTagStr.split()[arrayElement],pwGlob.edgeEnergyStr.split()[arrayElement] ))

def showEdgeNin(nr):
    edgeNin(nr, move=False)

def energyGotoEdge(target="Mo",E=None,EScanrange=0.8,detector=pcogold.counters.total_avg):
    if target in ["Ni", "NiK", "Ni-K"]:
        if E==None:
            E=8.333
    if target in ["Zn","ZnK","Zn-K"]:
        if E==None:
            E=9.659
    if target in ["Au", "Au-L3", "AuL3"]:
        if E==None:
            E=11.919
    if target in ["Pb", "Pb-L3", "PbL3"]:
        if E==None:
            E=13.035
    if target in ["Y","Y-K","YK"]:
        if E==None:
            E=17.035
    if target in ["Mo","MoK","Mo-K"]:
        if E==None:
            E=20.000
    if target in ["Ag","AgK","Ag-K"]:
        if E==None:
            E=25.251
    if target in ["Sb", "SbK", "Sb-K"]:
        if E==None:
            E=30.491
    if E==None:
       print("Please specify expected E of edge. (not needed for Ni, Zn, Au, Pb, Y, Mo, Ag, Sb)")        
       return
    
    SCAN_SAVING.template = "%s_edge"
    SCAN_SAVING.data_filename = "%s_edge"
    if detector==pico3:
       counteraxis="pico3"
    if detector==pcogold.counters.total_avg:
       counteraxis="total_avg"
    
    #first coarse pass
    plotselect(pico1)
    user_script_load("/data/bm05/inhouse/xog-refropt/macros/xsvt-mono.py","kev")
    kev.moveE(E, do_dm=True, mode='scratch', counter=pico1, detune=20, diodeIn=True, diodeOut=True)
    plotselect(detector)
    myscan = dscan(monoE, -0.5*EScanRange, 0.5*EscanRange, 40, 0.1, detector)
        
    xarr = myscan.get_data()['monoE']
    yarr = myscan.get_data()[counteraxis]
    dydx = np.abs(np.diff(yarr)/np.diff(xarr))
    xn=xarr[0:len(xarr)-1]+0.5*(xarr[1]-xarr[0])
    newE = xarr[dydx.argmax()]   
  
    # finer pass
    plotselect(pico1)
    kev.moveE(newE, do_dm=True, mode='hopeful', counter=pico1, detune=20, diodeIn=True, diodeOut=True)
    plotselect()
    myscan = dscan(monoE, -0.1, 0.1, 50, 0.1, detector)

    xarr = myscan.get_data()['monoE']
    yarr = myscan.get_data()[counteraxis]
    dydx = np.abs(np.diff(yarr)/np.diff(xarr))
    xn=xarr[0:len(xarr)-1]+0.5*(xarr[1]-xarr[0])
        
    # compute guess
    center = xarr[dydx.argmax()]
    bkg=0.5*(dydx[0]+dydx[-1])
    peak=np.max(dydx)
    sigma =(xn[-1]-xn[0])*0.1
    area = abs((dydx.sum()-len(xn)*0.5*(dydx[0]+dydx[-1])) * (xn[-1] - xn[0]) / len(xn))
    #fit
    fit=curve_fit(gaussianFlat,xn,dydx,(area,center,sigma,bkg))

    if plotIt:
           pylab.figure("gaussFit")
           pylab.plot(xn,dydx,"bo")
           pylab.plot(np.linspace(xn[0],xn[-1],1000),gaussianFlat(np.linspace(xn[0],xn[-1],1000),fit[0][0],fit[0][1],fit[0][2],fit[0][3]),"r-")
           pylab.savefig(pwGlob.rootDir +'/' + pwGlob.lensSubfolder + '/'+lenstagarr[ii]+'.png')
           pylab.close()
           
           osCMD = "display %s &" %(pwGlob.rootDir + '/' + pwGlob.lensSubfolder + '/' +lenstagarr[ii]+'.png')
           os.system(osCMD)
          
    print(fit[0])
    print("Edge FWHM is %.3f um" % (fit[0][2]*2.355*1e6))
    E_found=fit[0][1]
    th_b = bm05mono.energy2bragg(fit[0][1])
    print("monochromator-energy of %s edge is %.3f  keV  (should be %.3f keV)" % (target,E_found,E))    
    print("monochromator-angle  of %s edge is %.4f deg   (could be %.3f deg - depends on d-spacing)" % (target,th_b,bm05mono.energy2bragg(E)))

    print("Moving to edge...")
    kev.moveE(E_found, do_dm, mode="tweak", counter=pico1, detune=20, diodeIn=True, diodeOut=True)
    
    endSaving()


def energySeries(exptime=0.1, subset="all", postAlign=True, plotIt=False, detector=pcogold.counters.total_avg):
    user_script_load("/data/bm05/inhouse/xog-refropt/macros/xsvt-mono.py","kev")


    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera
    edgeposstr = pwGlob.edgePosStr
    edgetagstr = pwGlob.edgeTagStr
    materialstr= pwGlob.edgeElementStr
    energyStr = pwGlob.edgeEnergyStr
    
    energyarr = energyStr.split()
    edgetagarr = edgetagstr.split()
    materialarr= materialstr.split()

    SCAN_SAVING.template = "energySeries"
    SCAN_SAVING.data_filename = "energySeries"
    
    if detector==pico3:
       counteraxis="pico3"
    if detector==pcogold.counters.total_avg:
       counteraxis="total_avg"
    
    outFileName = pwGlob.rootDir + '/' + pwGlob.edgeSubfolder + '/edgeResults.txt'
    headerWritten=False
    headerLine="#pos E_target E_found th_B_found lambda_found d_used scanNr  timeStr"

    for ii in range(len(edgeposstr.split())):
        wantedPos = int(edgeposstr.split()[ii])
        print("Do we need position %d ?" % wantedPos)
        if subset != "all":
            subsetarr = generatePosStr(subset).split()
            print(subsetarr)
            if str(wantedPos) not in subsetarr:
                print("%d  is not wanted " % wantedPos)
                continue
        edgeNin(wantedPos)
        if postAlign:
            SCAN_SAVING.images_path_template = ("%s_align" % edgetagarr[ii])
            SCAN_SAVING.images_prefix = ("%s_align_" % edgetagarr[ii])
            plotselect(camera.counters.total_avg)
            umvr(y2,-3);ct(exptime, detector)
            dscan(y2, -3, 3, 30, exptime, detector)
            goto_com()
            
        SCAN_SAVING.images_path_template = ("%s" % edgetagarr[ii])
        SCAN_SAVING.images_prefix = ("%s_eScan_" % edgetagarr[ii])
        
        
        # first pass
        plotselect(pico1)
        kev.moveE(float(energyarr[ii]), do_dm=True, mode='scratch', counter=pico1, detune=20, diodeIn=True,diodeOut=True)
        plotselect(detector)
        myscan = dscan(monoE, -1,1, 40, exptime, detector)
        
        xarr = myscan.get_data()['monoE']
        yarr = myscan.get_data()[counteraxis]
        
        dydx = np.abs(np.diff(yarr)/np.diff(xarr))
        xn=xarr[0:len(xarr)-1]+0.5*(xarr[1]-xarr[0])
        
        # finer pass
        newE = xarr[dydx.argmax()]
       
        plotselect(pico1)
        kev.moveE(newE, do_dm=True, mode='hopeful', counter=pico1, detune=20, diodeIn=True,diodeOut=True)
        plotselect(detector)
        myscan = dscan(monoE, -0.1, 0.1, 50, exptime, detector)
        
        xarr = myscan.get_data()['monoE']
        yarr = myscan.get_data()[counteraxis]
        
        dydx = np.abs(np.diff(yarr)/np.diff(xarr))
        xn=xarr[0:len(xarr)-1]+0.5*(xarr[1]-xarr[0])
        
        # compute guess
        center = xarr[dydx.argmax()]
        bkg=0.5*(dydx[0]+dydx[-1])
        peak=np.max(dydx)
        sigma =(xn[-1]-xn[0])*0.1
        area = abs((dydx.sum()-len(xn)*0.5*(dydx[0]+dydx[-1])) * (xn[-1] - xn[0]) / len(xn))
        print(area)
        print(center)
        print(sigma)
        print(bkg)
        #fit
        fit=curve_fit(gaussianFlat,xn,dydx,(area,center,sigma,bkg))

        if plotIt:
           pylab.figure("gaussFit")
           pylab.plot(xn,dydx,"bo")
           pylab.plot(np.linspace(xn[0],xn[-1],1000),gaussianFlat(np.linspace(xn[0],xn[-1],1000),fit[0][0],fit[0][1],fit[0][2],fit[0][3]),"r-")
           pylab.savefig(pwGlob.rootDir +'/' + pwGlob.edgeSubfolder + '/'+edgetagarr[ii]+'.png')
           pylab.close()
           
           osCMD = "display %s &" %(pwGlob.rootDir + '/' + pwGlob.edgeSubfolder + '/' +edgetagarr[ii]+'.png')
           os.system(osCMD)
          
        print(fit[0])
        print("Sigma: %f " % fit[0][2])
        #if fit[0][2]<0:
        #    fit[0][0]=abs(fit[0][0])
        #    fit[0][2]=abs(fit[0][2])
        #    print("Sigma: %f " % fit[0][2])
        print("Beam FWHM is %.3f um" % (fit[0][2]*2.355*1e6))
        E_found=fit[0][1]
        
        th_b = bm05mono.energy2bragg(fit[0][1])
        dspacing = bm05mono.info_xtals().split(':')[-1].split()[0]
       
       
        outf=open(outFileName,"a")
        if not headerWritten:
           outf.write(headerLine+"\n")
           headerWritten=True
        timeStr=time.strftime("%H:%M:%S")
        scanNr=int(SCANS[-1].scan_number)
        resultLine="%d %s %.4f %.4f %.5f %s %3d %s" % (ii, energyarr[ii], E_found, th_b, 12.3982/E_found, dspacing, scanNr, timeStr )
        outf.write(resultLine+"\n")   
        outf.close()
        print('>>> file was updated')
    endSaving()
    beep(10)

def fitEnergySeries(file_path):

    def _E_from_th(x, d, thE):
        return  12.3982/(2*d*np.sin(x + thE))
            
    def _th_from_E(x, d, thE):
        return  np.arcsin(12.3982/(2*d*x)) - thE
        
    aux_file = open(file_path, 'r')
    lines = aux_file.readlines()
    
    n_fdata_set = len(lines)
    n_data_set = 0
    for line in lines:
         if line[0]=='#':
             pass
         else:
             n_data_set+=1
    
    E_target = np.zeros(n_data_set)
    E_found = np.zeros(n_data_set)
    th_B_found = np.zeros(n_data_set)
    lambda_found = np.zeros(n_data_set)
    d_used = np.zeros(n_data_set)
    
    k = 0
    for line in lines:
         if line[0]=='#':
             pass
         else:
             aux_line = line.split()
             E_target[k] = float(aux_line[1])
             E_found[k] = float(aux_line[2])
             th_B_found[k] = float(aux_line[3])
             d_used[k] = float(aux_line[5])
             k+=1
     
    guess_vals = [np.mean(d_used), 0] # 12.3982/(2*d*np.sin(x + thE))
     
    # curve_fit(f, xdata, ydata, p0) 
    best_vals, covar = curve_fit(_E_from_th, np.asarray(th_B_found)*np.pi/180, np.asarray(E_target), p0=guess_vals)
    print('fit coefficients for: E = 12.3982/(2*d*np.sin(thB + thE))')
    print('>>> E = 12.3982/(2*%.4f*np.sin(thB + %.4f))'%(best_vals[0], best_vals[1]*180/np.pi))
    print('>>>> err:')
    perr = np.sqrt(np.diag(covar))
    print(perr)
        
    print('\n')
        
    guess_vals = [np.mean(d_used), 0] # np.asin(12.3982/(2*d*x)) - thE
    # curve_fit(f, xdata, ydata, p0) 
    best_vals, covar = curve_fit(_th_from_E, np.asarray(E_target), np.asarray(th_B_found)*np.pi/180, p0=guess_vals)
    print('fit coefficients for: thB = np.asin(12.3982/(2*d*E)) - thE ')
    print('>>> thB = np.asin(12.3982/(2*%.8f*E)) - %.8f '%(best_vals[0], best_vals[1]*180/np.pi))
    print('>>>> err:')
    perr = np.sqrt(np.diag(covar))
    print(perr)


def test_alignment(distList, lensfolder, lenstag=None, exptime=3, abs_dist=True, postAlign=True, n_rep=10, settleTime=3):

    pwGlob = ParametersWardrobe('xsvt-globals')
    camera = pwGlob.camera

    SCAN_SAVING.template = lensfolder
    SCAN_SAVING.data_filename = lensfolder

    membraneOut()
    lensOut()

    for distance in distList:
        if abs_dist is True:
            umv(x3, distance)
        else:
            umvr(x3, distance)
        sleep(settleTime)

        # RC220618: this only works for a SS of about the FOV on the CCD
        if postAlign:
            plotselect(camera.counters.total_avg)
            dscan(y3, -1.5, 1.5, 50, .1)
            goto_peak()
            # umv(y3, (cen()+peak())/2)
            sleep(settleTime)

            plotselect(camera.counters.total_avg)
            dscan(z3, -1.5, 1.5, 50, .1)
            goto_peak()
            # umv(z3, (cen()+peak())/2)
            sleep(settleTime)

        if lenstag is None:
            file_tag = 'd_%04dmm' % int(distance)
        else:
            file_tag = lenstag + '_d_%04dmm' % int(distance)
        flats(exptime, "flats", "flat_" + file_tag, n_rep)

    endSaving()
    beep(10)
    pass

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================

# ======================================================================================================================
# initialisation
# ======================================================================================================================

# def main():


initGlobals()
pwGlob = ParametersWardrobe('xsvt-globals')
sleep(0.2)

print("------------------------------------")
print("--------- ST_BM5.py loaded ---------")
print("------------------------------------\n")

print("Root directory is: \n>>%s" % pwGlob.rootDir)
print("Believe we use camera %s" % pwGlob.camera.name)
print(">> change with with xsvt.changeDetector(pcogold) or similar")

attenuatorsOut()
current_session.disable_esrf_data_policy()
SCAN_SAVING.base_path = pwGlob.rootDir
deccelerate_y2Axis()
endSaving()

print("------------------------------------")
print("------------------------------------")
print("------------------------------------\n")

# if __name__ == '__main__':
#     main()


print("Really trying hard to disable esrf data policy...")
current_session.disable_esrf_data_policy()
print("Finished with ST_BM5.py")
