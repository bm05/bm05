import numpy
from scipy.optimize import leastsq

class GaussianFit:
    """
    GaussianFit class is using a vector p as parameters of the fit.
    This vector p contains:
    - the amplitude of the gaussian, A
    - the width of the gaussian, mu
    - the centre of the gaussian, x0
    - the offset of the gaussian, D
    The formula for the gaussian is:
      G(x) = D + A / (mu.sqrt(2pi)) * exp(-[(x-x0)/mu]**2/2)
    """
    def __init__(self, x, y):
      self.x = x
      self.y = y

    def init(self, mu0 = None):
      """
      This function tries to setup an initial set of parameters, so
      the fit should work well.
      The amplitude is taken as the peak to peak value of the data,
      the width is taken as one tenth of the X span, the centre is
      the position of the highest value in Y and the offset is the
      min value in Y.
      """
      x = self.x
      data = self.y

      if mu0 is None:
          mu0 = x.ptp() * 0.1
      return (mu0 * numpy.sqrt(2. * numpy.pi)) * data.ptp(), mu0, x[data.argmax()], data.min()

    def evaluate(self, p):
      x = self.x
      return p[3] + p[0] / (p[1] * numpy.sqrt(2. * numpy.pi)) * numpy.exp((x - p[2]) ** 2 / (-2. * (p[1] ** 2)))

    def fwhm(self, p):
      return p[1] * 2.35482

    def peakpos(self, p):
      return p[2]

def calc_residuals(p, fit):
  return fit.y - fit.evaluate(p)


