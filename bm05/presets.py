from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.controllers.lima.lima_base import Lima
from bliss.common.logtools import log_info
from bliss import current_session
from bliss.shell.standard import bench
import time
from blissdata import settings

class DirectoriesMappingPreset(ChainPreset):

    def prepare(self,acq_chain):

        saving_space = current_session.scan_saving.proposal_type
        lima_nodes = [node for node in acq_chain.nodes_list if type(node) == LimaAcquisitionMaster]
        if len(lima_nodes) > 0:
            node = lima_nodes[0]
            if node.acq_params['saving_mode'] != 'NOSAVING' and saving_space in node.device.directories_mapping_names:
                node.device.select_directories_mapping(saving_space)
                directory = node._unmapped_path
                prefix = node.acq_params['saving_prefix']
                force_no_saving = False if node.save_flag else True
                node.set_image_saving(directory, prefix, force_no_saving)

    def start(self,acq_chain):
        pass
    def stop(self,acq_chain):
        pass



class FrontEndCheckPreset(ChainPreset):

    def __init__(self, frontend, waittime=None, polling_time=1.):
        self.frontend = frontend
        self.waittime = waittime
        self.polling_time = polling_time


    class Iterator(ChainIterationPreset):
        def __init__(self, frontend, waittime, polling_time):
            self.frontend = frontend
            self.waittime = waittime
            self.polling_time = polling_time

        def prepare(self):
            if self.frontend.is_closed or self.frontend.state.value == 'Fault state':
                while self.frontend.is_closed or self.frontend.state.value == 'Fault state':
                    print("Waiting for beam")
                    time.sleep(self.polling_time)
                print("Waiting for stabilization")
                time.sleep(self.waittime)


    def get_iterator(self, acq_chain):
        while True:
            yield FrontEndCheckPreset.Iterator(self.frontend,self.waittime,self.polling_time)


class ZapScanTopoPreset(ChainPreset):

    def __init__(self, multiplexer, musst):
        self._multiplexer = multiplexer
        self._musst = musst

    def prepare(self,acq_chain):

        self._prev_source = self._multiplexer.getOutputStat('SOURCE')
        self._multiplexer.switch("SOURCE", "MUSST2B")
        self._multiplexer.switch("CAMERA", "EH1_SPARE2_START")

    def stop(self,acq_chain):
        self._multiplexer.switch("SOURCE", self._prev_source)



class ZapScanFluoDetPreset(ChainPreset):

    def __init__(self, multiplexer, musst):
        self._multiplexer = multiplexer
        self._musst = musst

    def prepare(self,acq_chain):

        self._prev_source = self._multiplexer.getOutputStat('SOURCE')
        self._multiplexer.switch("SOURCE", "MUSST1B")
        self._multiplexer.switch("CAMERA", "EH1_SPARE1_START")

    def stop(self,acq_chain):
        self._multiplexer.switch("SOURCE", self._prev_source)

class FShutPreset(ChainPreset):

    def __init__(self, fshutz, fshut):
        self._fshutz = fshutz
        self._fshut = fshut
        self._setting = settings.SimpleSetting('DisableShutter', default_value='False')

    class Iterator(ChainIterationPreset):
        def __init__(self, fshutz, fshut, setting):
            self._fshutz = fshutz
            self._fshut = fshut
            self._setting = setting
        def start(self):
            if -30 < self._fshutz.position < 30 and self._setting.get() == False:
                self._fshut.open()
                time.sleep(0.03)

        def stop(self):
            if -30 < self._fshutz.position < 30 and self._setting.get() == False:
                self._fshut.close()

    def get_iterator(self, acq_chain):
        while True:
            yield FShutPreset.Iterator(self._fshutz, self._fshut, self._setting)


class FShutTomoPreset(ScanPreset):

    def __init__(self, fshutz, fshut):
        self._fshutz = fshutz
        self._fshut = fshut
        self._setting = settings.SimpleSetting('DisableShutter', default_value='False')

    def start(self, scan):
        self._setting.set('True')
        if -30 < self._fshutz.position < 30 and not 'dark' in scan.name:
            self._fshut.open()
            time.sleep(0.03)

    def stop(self, scan):
        self._setting.set('False')
        if -30 < self._fshutz.position < 30 and not 'dark' in scan.name:
            self._fshut.close()
