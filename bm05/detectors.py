from bliss import current_session
from bliss.common.plot import plotselect
from bliss.common.utils import add_autocomplete_property, autocomplete_property
from bliss.config.settings import SimpleSetting
from bliss.controllers.lima.lima_base import Lima
from bliss.common.scans import DEFAULT_CHAIN
from bliss.common.measurementgroup import ACTIVE_MG
import time 

class Detectors:

    def __init__(self,name, config):
        self.__opiom = config.get('opiom')
        self.__detectors = config.get('detectors')
        self.__detectors_names = [det['device'].name for det in self.__detectors]
        self.__setting = SimpleSetting("active_detector", default_value=self.__detectors[0]['device'].name)
        [add_autocomplete_property(self, det, self.__getattr__(det,False)) for det in self.__detectors_names]
        
    def __activate_hard_triggering(self):
        default_acq_chain = current_session.config.get("default_acq_chain")
        DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])
        ACTIVE_MG.disable('*p201*')
        
    def __activate_soft_triggering(self):
        DEFAULT_CHAIN.set_settings([])
        ACTIVE_MG.enable('*p201*')

    def __activate(self, detector):
        detector_pars, = [det for det in self.__detectors if det['device'] == detector]
        opiom_output = detector_pars['opiom_output']
        self.__opiom.switch('CAMERA',opiom_output)
        opiom_shutter = detector_pars['opiom_shutter']
        self.__opiom.switch('SHUTTER',opiom_shutter)
        meas_group = detector_pars['measurement_group']
        meas_group.set_active()
        meas_group.set_active()
        hard_trigger = detector_pars['hard_triggering']
        if hard_trigger:
            self.__activate_hard_triggering()
            if detector.camera_type.lower() == 'maxipix' and detector_pars['accumulation']:
                self.__opiom.switch('SOURCE','MUSST')
                default_acq_chain_acc = current_session.config.get("default_acq_chain_acc")
                DEFAULT_CHAIN.set_settings(default_acq_chain_acc['chain_config'])
            else:
                self.__opiom.switch('SOURCE','P201')
        else:
            self.__activate_soft_triggering()
        detector.roi_counters['total'] = detector.proxy.image_roi
        plotselect(detector.counters.total_std)
    
    def __getattr__(self, name, activate=True):
        if name in self.__detectors_names and activate:
            self.__setting.set(name)
            detector = current_session.config.get(self.active_detector)
            self.__activate(detector)
    
    @property
    def active_detector(self):
        return self.__setting.get()
            
    def __info__(self):
        info_str = f"\nActive detector:\t{self.active_detector}\n"
        info_str += "Detectors handled by this object:\n"
        for det_name in self.__detectors_names:
            info_str += f"{det_name}\n"
        return info_str
    

