# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bm05.MONO.monochromator import MonochromatorBase
    
class BM05Mono(MonochromatorBase):
    
    def initialize(self):
        
        self.change_motor = self.config.get("change_motor")
                        
        self.low_pos = self.xtals.get_xtals_config("low_pos")
        self.high_pos = self.xtals.get_xtals_config("high_pos")

    def _xtal_is_in(self, xtal):
        current_pos = self.change_motor.position
        if current_pos > self.low_pos[xtal] and current_pos < self.high_pos[xtal]:
            return True
        return False
        
    def _xtal_change(self, xtal):
        target_pos = self.low_pos[xtal] + (self.high_pos[xtal] - self.low_pos[xtal]) / 2.0
        self.change_motor.move(target_pos)
