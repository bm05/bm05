# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import math
import numpy as np
import copy
import xcalibu

from bliss.config import settings
from bliss.physics.units import ur, units
from bliss.physics.diffraction import CrystalPlane, _get_all_crystals, MultiPlane, hc
from bliss.controllers.motor import CalcController

"""
    This file contain classes which represent a set of common functionnalities
    for monochromator control.
    We assume that a monochromator is composed of:
        - Rotation motor (bragg angle - real motor)
        - Energy motor (Calc Motor)
        - Crystal(s)
        
    This is represented by 3 classes MonochromatorBase, XtalManager and EnergyCalcMotor
    
    MonochromatorBase:
        * Minimum YAML file
            - plugin: bliss
              package: bm23.MONO.BM23monochromator
              class: MonochromatorBase
              name: bm23mono
              xtals: $bm23_mono_xtals       <-- see XtalManager section
              energy_motor: $bm23energy     <-- see EnergyCalcMotor section
              bragg_motor: $bm23bragg       <-- Bragg angle (real motor)
        
        * a Monochromator object as a name that you may use to get its status
            SESSION_MONO [1]: bm23mono
                     Out [1]: Monochromator: bm23mono
                              Crystal: Si111 [Si111 / Si333 / Si311 / Si511]
                              Energy: 11.387 KeV
                              Angle:  10.0 deg
        * For DCM monochromator, a fix exit offset can given and pass
          to the XtalManager object (see XTalManager section)
            - YAML file
                - package: bm23.MONO.BM23mono
                  class: BM23Mono
                  name: bm23mono
                  xtals: $bm23_mono_xtals
                  energy_motor: $bm23energy
                  bragg_motor: $bm23bragg
                  fix_exit_offset: 25.0     <--
        * If the monochromator holds a set of crystals, method to move
          from one crystal to the other(s) is foreseen. 
          In this case, a new class should be written which inheriting
          from MonochromatorBase Class.
            - 3 methods should be re-written:
                + intitilize(self)
                  Allow to get the necessary parameters in the mono yaml file
                + _xtal_change(self, xtal)
                  Move "xtal" in the beam
                + _xtal_is_in(self, xtal)
                  return True if "xtal" is in the beam, False otherwise
            - Additionnal parameters should also be added in the XtalManager
              YAML file to get, for example the position of the crystals in the beam
              
            - Example:
                + Mono YAML file example
                    - plugin: bliss
                      package: bm23.MONO.BM23mono
                      class: BM23Mono
                      name: bm23mono
                      xtals: $bm23_mono_xtals
                      energy_motor: $bm23energy
                      bragg_motor: $bm23bragg
                      change_motor: $smty
                      fix_exit_offset: 25.0
                
                + XtalManager file example
                
                + New Monochromator class
                
                    from bm23.MONO.BM23monochromator import MonochromatorBase
                    
                    class BM23Mono(MonochromatorBase):
    
                        def initialize(self):
                            # motor to move crystals in the beam
                            self.change_motor = self.config.get("change_motor")
                        
                            # position of each crystals in the beam
                            self.position = {}
                            xtals = self.xtals.config.get("xtals")
                            for xtal_index in range(len(xtals)):
                                xtal_name = xtals[xtal_index]["xtal"]
                                self.position[xtal_name] = float(xtals[xtal_index]["position"])

                        def _xtal_is_in(self, xtal):
                            if self.change_motor.position == self.position[xtal]:
                                return True
                            return False
        
                        def _xtal_change(self, xtal):
                            self.change_motor.move(self.position[xtal])
        

    
    XtalManager
        * Description of the crystal(s) mounted on the monochromator.
        * At least one crystal need to be defined ...
            - YAML file:
                plugin: bliss
                package: bm23.MONO.BM23monochromator
                class: XtalManager
                name: bm23_mono_xtals
                xtals:
                    - xtal: Si111
            - Available method:
                + bragg2energy
                + energy2bragg
        * ... but more may be used:
            - YAML file:
                xtals:
                    - xtal: Si111
                    - xtal: Si311  <--
                    - xtal: Ge511  <--
        * More method are availble for DCM monochromator, 
          if the "fix_exit_offset" attribute is set:
                + bragg2dxtal
                + energy2dxtal
                + dxtal2bragg
            - YAML file:            
                plugin: bliss
                package: bm23.MONO.BM23monochromator
                class: XtalManager
                name: bm23_mono_xtals
                fix_exit_offset: -10    <--
                xtals:
                    - xtal: Si111
                    - xtal: Si311
                    - xtal: Ge511
            
        * If you want to use crystal harmonics, add a crystal with its
          corresponding HKL values. You will be able to use it by changing 
          the selected crystal with the Monochromator object (mono.xtal_change(new_xtal)).
            - YAML file:
                xtals:
                    - xtal: Si111
                    - xtal: Si333  -> Use third harmonic of Si111
                    - xtal: Si311
                    - xtal: Ge511  
        * Calculations are using the module bliss.physics.diffraction based
          on the public module mendeleev which define a number of crystals parameters.
          You may specify your own dspacing (in Angstrom) in the yml file.
          In this case, this value MUST be specify for each crystals
            - YAML file:
                xtals:
                    - xtal: Si111
                      dspacing: 3.1356  --> new dspacing for Si111
                    - xtal: Si333
                      dspacing: 3.1356  --> need to be set to be coherent
                    - xtal: Ge511  --> using default constants
        * Multilayers: 
            + Principle
              - the Bragg to Energy calculation for a multilayer is given by:
                n*lambda = 2*d-spacing*sqrt(1-delta_bar/pow(sin(theta),2))*sin(theta)
              - n = order of reflexion
              - d-spacing of such a multilayer is the sum of the thickness
                of each materials (in Angstrom in the calculation)
              - delta_bar:
                . Parameter which is energy dependant.
                . Given by the Optic Group as a file Energy/Delta_bar
                . this formula is not bijective. In consequence a loukup table
                  is build at the creation of the object to get energy
                  from angle or angle from energy
            
            + Configuration
                + to define a multilayer, use the tag "multilayer" instead of "xtal"
                + dpspacing:
                  . Use tag "thickness1" and "thickness2" if the materials and their
                    thickness are known. Only 2 materials are take into account
                    Thickness must be given in nm
                  . Use tag "dspacing" if the materials are unknown but you know
                    an approximative dspacing
                    dspacing must be given in nm
                + delta_bar:
                  . Use tag "delta_bar" to specify the file given by the optic group
                  . if delta_bar is not specified, delta_bar=0
                + lookup table:
                  . if none of the parameters are known you can directly specify a
                    lookup table enegy(eV) vs bragg angle (radian)
                  . Use the tag "lookup_table" to specify the file containing the lut
                  
            + YAML File:
                xtals:
                  - xtal: Si111       <-- Normal crystal definition
      
                  - multilayer: ML_1  <-- Multilayer: All parameters are known
                    dspacing1: 10        
                    dspacing1: 11    
                    delta_bar: /users/blissadm/local/beamline_configuration/mono/multilayer/OpticalConstantsW_B4C.txt
      
                  - multilayer: ML_2  <-- Multilayer: Only an approximative dspacing is known
                    dspacing: 21                      delta_bar = 0
      
                  - multilayer: ML_2  <-- Multilayer: Only a Energy<-> Bragg lookup table is given
                    lookup_table: /users/blissadm/local/beamline_configuration/mono/multilayer/Bm29_multilayer.txt
        
            
    EnergyCalcMotor
        * EnergyCalcMotor need to know the onochromator it is refering to.
          At Initialization of the mono, the method EnergyCalcMotor.set_mono(self)
          is call.
          The Value of the EnergyCalcMotor is Nan before this call or if no crystals
          is selected in the XtalManager object. The Monochrmator object 
          is in charge to do this selection
        * YAML file
            - plugin: emotion
              package: bm23.MONO.BM23monochromator
              class: EnergyCalcMotor
              axes:
                - name: $id10bragg
                  tags: real bragg
                - name: id10energy
                  tags: energy
                  unit: KeV


"""
"""
    Monochromator Base
"""
class MonochromatorBase(object):

    def __init__(self, name, config):
    
        self.__name = name
        self.__config = config
        self.__setting = None
        
        self.energy_motor = self.config.get("energy_motor", None)
        self.energy_motor.controller.set_mono(self)
        self.bragg_motor = self.config.get("bragg_motor", None)
        
        # xtals
        self.xtals = self.config.get("xtals", None)
        if self.xtals is None:
            raise RuntimeError(f"No XtalManager configured")
        else:
            if len(self.xtals.xtal_names) == 0:
                raise RuntimeError(f"No Crystals Defined in th XtalManager")
        
        # Fix exit Geometry
        self.fix_exit_offset = self.config.get("fix_exit_offset", None)
        
        self._initialize()

    def _initialize(self):
        
        self.initialize()
                
        xtal = self.xtals.xtal_sel
        if xtal is not None:
            if not self.xtal_is_in(xtal):
                xtal_in = self.xtal_in()
                if len(xtal_in) == 0:
                    self.xtals.xtal_sel = None
                else:
                    self.xtals.xtal_sel = xtal_in[0]
        
    """
    In case of inheritance, reimplement this method for software initialization
    """
    def initialize(self):
        pass
        
    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    @property
    def settings(self):
        return self.__settings
        
    def __info__(self):
        # name
        info_str = f"Monochromator: {self.name}\n\n"
        
        # crystals
        if self.xtals is not None:
            info_str += self.xtals.__info__()
        info_str += f"\n"
            
        # Energy/Bragg Motor
        if self.energy_motor is not None:
            info_str += f"Current Energy: {self.energy_motor.position:.3f} {self.energy_motor.unit}\n"
        info_str += f"Current Angle:  {self.bragg_motor.position:.3f} {self.bragg_motor.unit}\n"
        
        return info_str

    """
    Double Crystals monochromator may need this value
    It will be taken into account if present in the yml file
    """
    @property
    def fix_exit_offset(self):
        return self.__fix_exit_offset

    @fix_exit_offset.setter
    def fix_exit_offset(self, val):
        self.__fix_exit_offset = val
        self.xtals.fix_exit_offset = val

    """
    In case of monochromator with multi crystals a changer may be implemented
    By default, each crystals is consider un place. Changing the crystal only
    change the energy calculation
    """
        
    def xtal_is_in(self, xtal):
        if xtal in  self.xtals.xtal_names:
            return self._xtal_is_in(xtal)
        else:
            raise RuntimeError(f"Crystal {xtal} not configured")
    
    """
    This method needs to be overwrite to reflect the monochromator behaviour
    """
    def _xtal_is_in(self, xtal):
        return True        
        
    def xtal_change(self, xtal):
        if xtal in self.xtals.xtal_names:
            self._xtal_change(xtal)
            self.xtals.xtal_sel = xtal
            self.energy_motor.sync_hard()
        else:
            raise RuntimeError(f"Crystal {xtal} not configured")
        
    
    """
    This method needs to be overwrite to reflect the monochromator behaviour
    """
    def _xtal_change(self, xtal):
        pass
        
    """
    return the list of crystals in place
    """
    def xtal_in(self):
        xtal_in_place = []
        for xtal in self.xtals.xtal_names:
            if self.xtal_is_in(xtal):
                xtal_in_place.append(xtal)
        return xtal_in_place
        
"""
    Crystals management + Energy Calculation
    
    YML file exemple
        - plugin: bliss
          package: bm23.MONO.BM23monochromator
          class: XtalManager
          name: mono_xtals
          xtals:
            - xtal: Si111
            - xtal: Si333
            - xtal: Ge311

"""
class XtalManager(object):
    
    
    def __init__(self, name, config):
        
        self.__name = name
        self.__config = config
                
        # Crystal(s) management
        self.all_xtals = self.get_all_xtals()
        xtals = self.config.get("xtals")
        
        self.xtal_names = []
        self.xtal = {}
        for elem in xtals:
            if "xtal" in elem.keys():
                xtal_name = elem.get("xtal")
                dspacing = elem.get("dspacing", None)
                symbol = self.xtalplane2symbol(xtal_name)
                if symbol not in self.all_xtals:
                    if dspacing is not None:
                        self.xtal[xtal_name] = MultiPlane(distance=dspacing*1e-10)
                    else:
                        raise RuntimeError(f"dspacing of Unknown crystals must be given")
                else:
                    self.xtal[xtal_name] = copy.copy(CrystalPlane.fromstring(xtal_name))
                if dspacing is not None:
                    self.xtal[xtal_name].d = dspacing*1e-10
                self.xtal_names.append(xtal_name)
            elif "multilayer" in elem.keys():
                ml_name = elem.get("multilayer")
                self.xtal[ml_name] = Multilayer(ml_name, elem)
                self.xtal_names.append(ml_name)
                
        def_val = {"xtal_sel": None}
        self.__settings_name = f"XtalManager_{self.name}"
        self.__settings = settings.HashSetting(self.__settings_name, default_values=def_val)

        if self.settings["xtal_sel"] not in self.xtal_names:
            self.settings["xtal_sel"] = None

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    @property
    def settings(self):
        return self.__settings

    @property
    def fix_exit_offset(self):
        return self.__fix_exit_offset

    @fix_exit_offset.setter
    def fix_exit_offset(self, val):
        self.__fix_exit_offset = val
    
    @property
    def xtal_sel(self):
        return self.settings["xtal_sel"]
        
    @xtal_sel.setter
    def xtal_sel(self, xtal):
        if xtal is None or xtal in self.xtal_names:
            self.settings["xtal_sel"] = xtal
        else:
            raise RuntimeError(f"Crystal ({crystal}) not configured")
        
    def __info__(self):
        
        if self.xtal_sel is not None:
            xtal_sel = self.xtal[self.xtal_sel]
            if isinstance(xtal_sel, Multilayer):
                info_str = f"Multilayer:"
            else:
                info_str = f"Crystal:"
        else:
            info_str = f"Crystal:"
        info_str += f" {self.xtal_sel} ["
        for xtal in self.xtal_names:
            info_str += f"{xtal} / "
        info_str = info_str[:-3] + f"]"
        info_str += f"\n"
        
        if self.xtal_sel is not None:
            if isinstance(xtal_sel, Multilayer):
                ml_str = xtal_sel.__info__()
                info_str += ml_str                        
            else:
                dspacing = (self.xtal[self.xtal_sel].d*ur.m).to("angstrom")
                info_str += f"dspacing: {dspacing}\n"
        
        if self.fix_exit_offset is not None:
            info_str += f"Fix Exit Offset: {self.fix_exit_offset*ur.mm}\n"

        return info_str
        
    """
    Utils
    """
    def get_all_xtals(self):
        xtals = _get_all_crystals()
        all_xtals = []
        for xtal in xtals:
            all_xtals.append(xtal.name)
        return all_xtals
            
    def xtalplane2symbol(self, xtalplane):
        symbol, plane = "", ""
        for c in xtalplane:
            if c.isdigit() or c.isspace():
                plane += c
            elif c.isalpha():
                symbol += c
        return symbol
    
    def get_xtals_config(self, key):
        res = {}
        xtals = self.config.get("xtals")
        for elem in xtals:
            if "xtal" in elem.keys():
                elem_name = elem.get("xtal")
            elif "multilayer" in elem.keys():
                elem_name = elem.get("multilayer")
            else:
                raise RuntimeError("No keyword \"xtal\" nor \"multilayer\" in xtal")
            res[elem_name] = float(elem.get(key))
                
        return res
        

    """
    Calculation methods
    """
    def energy2bragg(self, ene):
        xtal = self.xtal[self.xtal_sel]
        bragg = xtal.bragg_angle(ene*ur.keV)
        if np.isnan(bragg):
            return np.nan
        else:
            bragg = bragg.to(ur.deg).magnitude
            return(bragg)
        
    def bragg2energy(self, bragg):
        xtal = self.xtal[self.xtal_sel]
        energy = xtal.bragg_energy(bragg*ur.deg)
        if np.isnan(energy):
            return np.nan
        else:
            energy = energy.to(ur.keV).magnitude
            return energy
        
    def bragg2dxtal(self, bragg):
        if self.fix_exit_offset is not None:
            dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
            return dxtal
        raise RuntimeError(f"No Fix Ext Offset defined")
        
    def dxtal2bragg(self, dxtal):
        if self.fix_exit_offset is not None:
            bragg = math.degrees(math.acos(self.fix_exit_offset / (2.0 * dxtal)))
            return bragg
        raise RuntimeError(f"No Fix Ext Offset defined")            
        
    def energy2dxtal(self, ene):
        if self.fix_exit_offset is not None:
            xtal = self.xtal[self.xtal_sel]
            bragg = xtal.bragg_angle(ene*ur.keV).to('deg').magnitude
            dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
            return dxtal
        raise RuntimeError(f"No Fix Ext Offset defined")
     
    
class Multilayer(object):
    
    def __init__(self, name,  config):
        
        self.__name = name
        self.__config = config
        
        self.thickness1 = self.config.get("thickness1", None)
        self.thickness2 = self.config.get("thickness2", None)
        self.ml_file = self.config.get("delta_bar", None)
        self.lut_file = None
        if  self.thickness1 is not None and self.thickness2 is not None:
            self.d = ((self.thickness1 + self.thickness2) * 1e-9)*ur.m
            if self.ml_file is not None:
                self.create_lut_from_ml_file()
        else:
            dspacing = self.config.get("dspacing", None)
            if dspacing is not None:
                self.d = (dspacing * 1e-9)*ur.m
            else:
                self.d = None
                self.lut_file = self.config.get("lookup_table", None)
                if self.lut_file is not None:
                    self.create_lut_from_lut_file()
                else:
                    raise RuntimeError(f"Multilayer {name}: Wrong yml configuration")
    
    @property
    def name(self):
        return self.__name
        
    @property
    def config(self):
        return self.__config
        
    def __info__(self):
        info_str = ""
        if self.thickness1 is not None and self.thickness2 is not None:
            info_str += f"Thickness Material #1: {xtal_sel.thickness1*ur.nm}\n"
            info_str += f"Thickness Material #2: {xtal_sel.thickness2*ur.nm}\n"
            dspacing = xtal_sel.d.to("nm")
            info_str += f"d-spacing: {dspacing}\n"
            if self.ml_file is not None:
                min_en = (self.en2bragg.min_x()*ur.J).to("keV").magnitude
                max_en = (self.en2bragg.max_x()*ur.J).to("keV").magnitude
                min_th = np.degrees(self.en2bragg.min_y())
                max_th = np.degress(self.en2bragg.max_y())
                        
                info_str += f"delta_bar file: {self.ml_file}\n"
                info_str += f"Energy (keV): [{min_en:.3f} : {max_en:.3f}]\n"
                info_str += f"Bragg (deg) : [{max_th:.3f} : {min_th:.3f}]\n"
        else:
            if self.d is not None:
                dspacing = self.d.to("nm")
                info_str += f"d-spacing: {dspacing}\n"
            else:
                if self.lut_file is not None:
                    min_en = (self.en2bragg.min_x()*ur.J).to("keV").magnitude
                    max_en = (self.en2bragg.max_x()*ur.J).to("keV").magnitude
                    min_th = np.degrees(self.en2bragg.min_y())
                    max_th = np.degrees(self.en2bragg.max_y())
                        
                    info_str += f"Lookup table file: {self.lut_file}\n"
                    info_str += f"Energy (keV): [{min_en:.3f} : {max_en:.3f}]\n"
                    info_str += f"Bragg (deg) : [{max_th:.3f} : {min_th:.3f}]\n"
                else:
                    raise RuntimeError("THIS ERROR SHOULD NEVER HAPPENED !!!\n")
                    
        return info_str

    def create_lut_from_ml_file(self):
        if self.ml_file is not None:
            arr = np.loadtxt(self.ml_file, comments="#").transpose()            
            arr_energy = np.copy((arr[0]*ur.keV).to(ur.J))
            arr_theta = np.arcsin(np.sqrt(arr[5] + np.power(hc/(2.0*self.d*arr_energy),2)))
            
            self.en2bragg = xcalibu.Xcalibu()
            self.en2bragg.set_calib_name(f"{self.name}_bragg")
            self.en2bragg.set_calib_time(0)
            self.en2bragg.set_calib_type("TABLE")
            self.en2bragg.set_reconstruction_method("INTERPOLATION")
            self.en2bragg. set_raw_x(arr_energy.magnitude)
            self.en2bragg. set_raw_y(arr_theta.magnitude)
            
            arr_flip_theta = np.flip(arr_theta)
            arr_flip_energy = np.flip(arr_energy)
            self.bragg2en = xcalibu.Xcalibu()
            self.bragg2en.set_calib_name(f"{self.name}_bragg")
            self.bragg2en.set_calib_time(0)
            self.bragg2en.set_calib_type("TABLE")
            self.bragg2en.set_reconstruction_method("INTERPOLATION")
            self.bragg2en. set_raw_x(arr_flip_theta.magnitude)
            self.bragg2en. set_raw_y(arr_flip_energy.magnitude)

    def create_lut_from_lut_file(self):
        if self.lut_file is not None:
            arr = np.loadtxt(self.lut_file, comments="#").transpose()            
            arr_energy = np.copy(((arr[0]/1000.0)*ur.keV).to(ur.J))
            arr_theta = np.copy(arr[1]*ur.radians)
            
            self.en2bragg = xcalibu.Xcalibu()
            self.en2bragg.set_calib_name(f"{self.name}_bragg")
            self.en2bragg.set_calib_time(0)
            self.en2bragg.set_calib_type("TABLE")
            self.en2bragg.set_reconstruction_method("INTERPOLATION")
            self.en2bragg. set_raw_x(arr_energy.magnitude)
            self.en2bragg. set_raw_y(arr_theta.magnitude)
            
            arr_flip_theta = np.flip(np.copy(arr_theta))
            arr_flip_energy = np.flip(np.copy(arr_energy))
            self.bragg2en = xcalibu.Xcalibu()
            self.bragg2en.set_calib_name(f"{self.name}_bragg")
            self.bragg2en.set_calib_time(0)
            self.bragg2en.set_calib_type("TABLE")
            self.bragg2en.set_reconstruction_method("INTERPOLATION")
            self.bragg2en. set_raw_x(arr_flip_theta.magnitude)
            self.bragg2en. set_raw_y(arr_flip_energy.magnitude)
    
    @units(wavelength="m", result="J")
    def wavelength_to_energy(self, wavelength):
        """
        Returns photon energy (J) for the given wavelength (m)

        Args:
            wavelength (float): photon wavelength (m)
        Returns:
            float: photon energy (J)
        """
        return hc / wavelength


    @units(energy="J", result="m")
    def energy_to_wavelength(self, energy):
        """
        Returns photon wavelength (m) for the given energy (J)

        Args:
            energy (float): photon energy (J)
        Returns:
            float: photon wavelength (m)
        """
        return hc / energy

    @units(theta="rad", d="m", result="m")
    def bragg_wavelength(self, theta, d, n=1):
        """
        Return a bragg wavelength (m) for the given theta and distance between
        lattice planes.

        Args:
            theta (float): scattering angle (rad)
            d (float): interplanar distance between lattice planes (m)
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            float: bragg wavelength (m) for the given theta and lattice distance
        """
        return 2.0 * self.d * math.sin(theta)

    @units(theta="rad", result="J")
    def bragg_energy(self, theta):
        """
        Return a bragg energy for the given theta and distance between lattice
        planes.

        Args:
            theta (float): scattering angle (rad)
            d (float): interplanar distance between lattice planes (m)
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            float: bragg energy (J) for the given theta and lattice distance
        """
        if self.ml_file is None and self.lut_file is None:
            return self.wavelength_to_energy(self.bragg_wavelength(theta, self.d, n=1))
        else:
            th = theta.magnitude
            if th >= self.bragg2en.min_x() and th <= self.bragg2en.max_x():
                return self.bragg2en.get_y(theta.magnitude)*ur.J
            else:
                return (np.nan)*ur.J

    @units(energy="J", result="rad")
    def bragg_angle(self, energy):
        """
        Return a bragg angle (rad) for the given theta and distance between
        lattice planes.

        Args:
            energy (float): energy (J)
            d (float): interplanar distance between lattice planes (m)
            n (int): order of reflection. Non zero positive integer (default: 1)
        Returns:
            float: bragg angle (rad) for the given theta and lattice distance
        """
        if self.ml_file is None and self.lut_file is None:
            return np.arcsin(hc / (2.0 * self.d * energy))
        else:
            en = energy.magnitude
            if en >= self.en2bragg.min_x() and en <= self.en2bragg.max_x():
                return self.en2bragg.get_y(energy.magnitude)*ur.rad
            else:
                return (np.nan)*ur.rad
        
"""
    Energy Calc Motor
"""        
class EnergyCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
                
        self.mono = None
    
    def set_mono(self, mono):
        self.mono = mono
        
    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
        
        if self.mono is not None and self.mono.xtals.xtal_sel is not None:
            ene = self.mono.xtals.bragg2energy(reals_dict["bragg"])
            unit = self.pseudos[0].unit
            if not np.isnan(ene):
                ene = (ene*ur.keV).to(unit).magnitude
            pseudos_dict["energy"] = ene
        else:
            pseudos_dict["energy"] = np.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        if self.mono is not None and self.mono.xtals.xtal_sel is not None and not np.isnan(pseudos_dict["energy"]):
            unit = self.pseudos[0].unit
            ene = (pseudos_dict["energy"]*ur.parse_units(unit)).to("keV").magnitude
            reals_dict["bragg"] = self.mono.xtals.energy2bragg(ene)
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
                
        return reals_dict
