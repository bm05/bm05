
import numpy

from bm05.multilayer.monochromator.monochromator_calcmotor import MonochromatorCalcMotorBase

class BM05BraggCalcMotor(MonochromatorCalcMotorBase):
    """Bragg Calculation Motor"""

    def calc_from_real(self, real_positions):

        pseudos_dict = {}

        bragg_xtal1 = real_positions["xtal1"]
        bragg_xtal2 = real_positions["xtal2"]

        if (
            numpy.isclose(bragg_xtal1, bragg_xtal2, atol=self.approx)
        ) or self._pseudos_are_moving():
            pseudos_dict["bragg"] = bragg_xtal1
        else:
            pseudos_dict["bragg"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, positions_dict):

        reals_dict = {}

        if (
            not numpy.isnan(positions_dict["bragg"]).any()
        ):
            reals_dict["xtal1"] = positions_dict["bragg"]
            reals_dict["xtal2"] = positions_dict["bragg"]
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict

class BM05BraggFixExitCalcMotor(MonochromatorCalcMotorBase):
    """
    Bragg Fix Exit Calculation Motor
    """

    def calc_from_real(self, real_positions):

        pseudos_dict = {}

        if self._mono is not None:
            bragg = self._mono.xtal2bragg(real_positions["xtal"])
            bragg_xtal1 = real_positions["bragg_xtal1"]
            bragg_xtal2 = real_positions["bragg_xtal2"]
            
            xtal_in_pos = numpy.isclose(bragg, bragg_xtal1, atol=self.approx)
            bragg_in_pos = numpy.isclose(bragg_xtal1, bragg_xtal2, atol=self.approx)

            if (xtal_in_pos and bragg_in_pos) or self._pseudos_are_moving():
                pseudos_dict["bragg_fix_exit"] = bragg_xtal1
            else:
                pseudos_dict["bragg_fix_exit"] = numpy.nan
        else:
            pseudos_dict["bragg_fix_exit"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, positions_dict):

        reals_dict = {}

        if (
            self._mono is not None
            and not numpy.isnan(positions_dict["bragg_fix_exit"]).any()
        ):
            reals_dict["bragg_xtal1"] = positions_dict["bragg_fix_exit"]
            reals_dict["bragg_xtal2"] = positions_dict["bragg_fix_exit"]
            reals_dict["xtal"] = self._mono.bragg2xtal(positions_dict["bragg_fix_exit"])
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict
