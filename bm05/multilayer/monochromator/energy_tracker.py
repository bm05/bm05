import numpy as np
import math
import xcalibu
import scipy.constants as codata
import tabulate
from datetime import datetime

from bliss.config.conductor import client
from bliss.config import settings
from bliss.common.utils import (
    add_property,
    add_object_method,
    add_autocomplete_property,
    autocomplete_property,
)

