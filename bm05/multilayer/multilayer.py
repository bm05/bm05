from bm05.multilayer.monochromator.monochromator import MonochromatorFixExit
import numpy as np
import tabulate

class BM05MultilayerMonochromator(MonochromatorFixExit):
    
    def __init__(self, config):
        super().__init__(config)
        
    """
    Load Configuration
    """
    def _load_config(self):
        super()._load_config()
        """
        Motors
        """
        self._motors["bragg_fix_exit"] = None
        motors_conf = self.config.get("motors", None)        
        for motor_conf in motors_conf:            
            # Bragg Fe
            if "bragg_fix_exit" in motor_conf.keys():
                self._motors["bragg_fix_exit"] = motor_conf.get("bragg_fix_exit")
                self._motors["bragg_fix_exit"].controller._set_mono(self)
        
        """
        Distance between crystals is manage by a motor (xtal) moving 
        second crystals perpendicular to the crystal plane.
        dxtals_at_xtal0 is the distance between first and second crystal when
        the motor (xtal) is at zero position
        Positive motion is intended to get both crystals closer.
        It will be taken into account for all xtals if "dxtal_at_xtal0"
        parameter is present in the monochromator yml file or
        xtal by xtal if present in the xtals yml object
        """
        dxtal_at_xtal0 = self.config.get("dxtal_at_xtal0", None)
        if dxtal_at_xtal0 is not None:
            self._dxtal_at_xtal0 = {}
            self._dxtal_at_xtal0["all"] = float(dxtal_at_xtal0)
        xtal_xtal0 = self._xtals.get_xtals_config("dxtal_at_xtal0")
        if xtal_xtal0 is not None:
            if hasattr(self, "_dxtal_at_xtal0"):
                self._dxtal_at_xtal0.update(self._xtals.get_xtals_config("dxtal_at_xtal0"))        
            else:
                self._dxtal_at_xtal0 = xtal_xtal0

    def __info__(self):
        info_str = "\n"
        info_str += self._info_mono()
        info_str += self._info_xtals()
        info_str += self._info_motor_energy()
        info_str += self._info_motor_fix_exit()
        info_str += "\n"
        return info_str
        
    def _info_motor_fix_exit(self):
        if self._motors["bragg_fix_exit"] is not None:
            controller = self._motors["bragg_fix_exit"].controller
            # TITLE
            title = [""]
            title.append(self._motors["bragg_fix_exit"].name)
            for axis in controller.reals:
                title.append(axis.name)
            # CALCULATED POSITION ROW
            calculated = ["Calculated"]
            # bragg_fe
            # bragg
            rbragg = controller._tagged["bragg_xtal1"][0]
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            # xtal
            rxtal = controller._tagged["xtal"][0]
            calc_xtal = self.bragg2xtal(rbragg.position)
            calculated.append(f"{calc_xtal:.3f} {rxtal.unit}")
            #
            # CURRENT POSITION ROW
            #
            current = ["Current"]
            # bragg fix exit
            current.append(f"{controller.pseudos[0].position:.3f} {controller.pseudos[0].unit}")
            for axis in controller.reals:
                current.append(f"{axis.position:.3f} {axis.unit}")
                
            info_str = tabulate.tabulate(
                [calculated, current],
                headers=title,
                tablefmt="plain",
                stralign="right",
            )
            return f"{info_str}\n\n"
        else:
            return ""
       
    """
    Distance between xtals when xtal motor is at 0 position
    """
    @property
    def dxtal_at_xtal0(self):
        if hasattr(self, "_dxtal_at_xtal0"):
            if self._xtals.xtal_sel in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0[self._xtals.xtal_sel]
            if "all" in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0["all"]
        else:
            raise ValueError(f"Monochromator {self._name}: No dxtal_at_xtal0 defined")
                    
    """
    Energy related methods, specific to Fix Exit Mono
    """        

    def dxtal2xtal(self, dxtal):
        return self.dxtal_at_xtal0 - dxtal

    def xtal2dxtal(self, xtal):
        return self.dxtal_at_xtal0 - xtal

    def bragg2xtal(self, bragg):
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal

    def xtal2bragg(self, xtal):
        dxtal = self.xtal2dxtal(xtal)
        bragg = self.dxtal2bragg(dxtal)
        return bragg

    def energy2xtal(self, ene):
        bragg = self.energy2bragg(ene)
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal
                
