from bliss.shell.cli.user_dialog import UserChoice, UserMsg
from bliss.shell.dialog.helpers import dialog
from bliss.common.utils import flatten
from bliss.shell.cli.pt_widgets import BlissDialog


def menu_optic(optic):
	
	dialogs = []
	positions_list = optic.positions_list
	indexes = {pos["label"]: i for i, pos in enumerate(positions_list)}
	values = [(pos["label"], pos["description"]) for pos in positions_list]
	axis = positions_list[0]["target"][0]["axis"]
	defval_label = optic.position  # actual position in string form
	try:
		defval = indexes[defval_label]  # numeric index of position
	except KeyError:
		defval = 0
		dialogs.append([UserMsg(label="WARNING! Optic position is UNKNOWN")])

	dialogs.append(
		[
			UserChoice(
				label=f"", values=values, defval=defval
			)
		]
	)
	choices = BlissDialog(
		dialogs, title="Optic pixel size selection", paddings=(3, 0)
	).show()
	if choices:
		# exclude 'dynamically' added UserMsg returned values for 'Optic position is UNKNOWN'
		values = [retval for widget, retval in choices.items() if not isinstance(widget, UserMsg)][0]
		optic.move(values)
	return optic
