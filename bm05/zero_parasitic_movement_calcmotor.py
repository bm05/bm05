
# 01-03-2023 P Cook -- Making a CalcMotor to create a stable x translation by auto-correcting the yc and z0
# This assumes it will be run in an MRTOMO or HRTOMO session
from bliss.controllers.motor import CalcController
import numpy as np
from bliss.shell.cli.user_dialog import UserMsg, UserYesNo
from bliss.shell.cli.pt_widgets import display

class TomoDetTableController(CalcController):
    z0_xc_calib = None
    yc_xc_calib = None
    
    def calibrate_movement(self, pixelsize=pixel_size()):
        z0_xc_calibfile = self.config.get("z0_xc_calibfile", string)
        yc_xc_calibfile = self.config.get("yc_xc_calibfile", string)
        old_sample = SCAN_SAVING.collection_name
        old_dataset = SCAN_SAVING.dataset_name
        
        #Message to confirm movement
        dlg = UserYesNo(label="Caution: This will move the x translation to both hard limits. Are you sure this is safe?")
        if not display(dlg, title='Safe to move?'):
            print("=== Calibration aborted: x movement may be unsafe")
            return
        dlg = UserYesNo(label="The pixel size is apparently %0.3f um. Is this accurate?")
        if not display(dlg, title='Pixel size?'):
            print("=== Calibration aborted: Recheck pixel size")
            return
        dlg = UserYesNo(label="Are the slits at a good position to make a small spot on the detector?")
        if not display(dlg, title='Slit size?'):
            print("=== Calibration aborted: Adjust slit size")
            return

        print("====================")
        print("Previous sample name: " + old_sample)
        print("Previous dataset: " + old_dataset)
        print("====================")
        print("Changing sample name to \'xc_calibration\'")

        sleep(1)
        ACTIVE_MG.enable(active_detector.name + "*bpm*")
        print("Moving to negative hard limit on " + realx.name)
        realx.hw_limit(-1)
        s = sct(full_tomo.pars.exposure_time)
        
        dlg = UserYesNo(label="Is the beam still reasonably well centered?")
        if not display(dlg, title='Beam centered?'):
            newsample(old_sample)
            newdataset(old_dataset)
            print("=== Calibration aborted: Adjust y and z positions to center beam")
            return
        
        #Save previous sample and dataset
        #New sample and dataset
        #Check that beam is roughly centered
        #Move to +ve limit and sct along the way
        #Between moves, check that the signal is still OK
        #Write calibration data to file
        #Ask if you want to check calibration

    def init_calibs(self):
        if z0_xc_calib is None:
            z0_xc_calibfile = self.config.get("z0_xc_calibfile", string)
            
        if yc_xc_calibfile is None:
            yc_xc_calibfile = self.config.get("yc_xc_calibfile", string)
            
    pass

    def calc_from_real(self, positions_dict):
        xc_offset = self.config.get("xc_offset", float)
    pass
    
    def calc_to_real(self, positions_dict):
    pass
    
