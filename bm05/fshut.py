from gevent import sleep
# BaseShutter
from bliss.common.shutter import Shutter, BaseShutter, BaseShutterState

class FShutter(BaseShutter):

    def __init__(self, name, config):
        self.__name = name
        self.__config = config
        self.__multiplexer = config["multiplexer"]
        self.__closing_time = config["closing_time"]
        self.__opening_time = config["opening_time"]
    
    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config
    
    def open(self):
        self.__multiplexer.switch("SHUTTER","SOFT")
        self.__multiplexer.switch("SOFTSHUT","OPEN")
        
        sleep(self.opening_time / 1000.0)
    
    def close(self):
        self.__multiplexer.switch("SHUTTER","SOFT")
        self.__multiplexer.switch("SOFTSHUT","CLOSE")
        

        sleep(self.closing_time / 1000.0)
        
    def auto(self):
        self.__multiplexer.switch("SHUTTER","HARD")
    
    @property
    def state(self):
        """From BaseShutterState - To generate Verbose message of the shutter state"""
        """ It is either BaseShutterState.OPEN, BaseShutterState.CLOSED or BaseShutterState.UNKNOWN """

        fsstate = BaseShutterState.UNKNOWN
        if self.__multiplexer.getOutputStat("SOFTSHUT") == "CLOSE":
            fsstate = BaseShutterState.CLOSED
        else:
            if self.__multiplexer.getOutputStat("SOFTSHUT") == "OPEN":
                fsstate = BaseShutterState.OPEN
            else:
                fsstate = BaseShutterState.UNKNOWN

        return fsstate
        
    @property
    def closing_time(self):
        return self.__closing_time

    @property
    def opening_time(self):
        return self.__opening_time
    @property        
    def is_open(self):
        if self.state == BaseShutterState.OPEN:
            return True
        else:
            return False
    @property
    def is_closed(self):
        if self.state == BaseShutterState.CLOSED:
            return True
        else:
            return False
            
    @property
    def mode(self):
        if self.__multiplexer.getOutputStat("SHUTTER") == "HARD":
            return Shutter.EXTERNAL
        else:
            return Shutter.MANUAL

