============
BM05 project
============

[![build status](https://gitlab.esrf.fr/BM05/bm05/badges/master/build.svg)](http://BM05.gitlab-pages.esrf.fr/BM05)
[![coverage report](https://gitlab.esrf.fr/BM05/bm05/badges/master/coverage.svg)](http://BM05.gitlab-pages.esrf.fr/bm05/htmlcov)

BM05 software & configuration

Latest documentation from master can be found [here](http://BM05.gitlab-pages.esrf.fr/bm05)
